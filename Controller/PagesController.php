<?php

/**
 * Class PagesController
 */
class PagesController extends AppController
{

    public $components = array('Paginator', 'RequestHandler');

    /**
     * Admin
     */
    public function admin_index($id = null)
    {


        $res = array();

        $this->Page->Behaviors->unload('Translate');

        if ($id) {

            $res = $this->Page->findById($id);

            $res['Chemin'] = $this->Page->getPath($id);

        }

        $res['Childrens'] = $this->Page->find('all', array('order' => 'Page.position', 'conditions' => array('Page.parent_id' => $id)));

        echo json_encode($res);

        exit();
    }


    /**
     * Arbre du site
     */
    public function admin_tree()
    {

        $this->Page->Behaviors->unload('Translate');

        $pages = $this->Page->find(
            'threaded', array(
                'order' => 'Page.position'
            )
        );

        echo json_encode($pages);

        exit();
    }

    /**
     * Arbre en liste
     */
    public function admin_liste()
    {

        $this->Page->Behaviors->unload('Translate');

        $data = $this->Page->generateTreeList(null, '{n}.Page.id', '{n}.Page.name', ' - ');

        //Pour préserver l'ordre
        $object = array();

        foreach ($data as $key => $value)
        {

            $object[] = array(
                'id' => $key,
                'label' => $value
            );

        }

        echo json_encode($object);


        exit();
    }

    /**
     * Permet de lister les urls :
     */
    public function admin_redactor_links()
    {

        $this->loadModel('FileManager.File');

        $links = array();


        if (empty($this->request->params['named']['extention_id'])) {

            $links[] = array('name' => 'Choisir une page', 'url' => false);

            //Liste des pages :
            $Page = $this->Page->generateTreeList(null, '{n}.Page.url', '{n}.Page.name', ' - ');

            foreach($Page as $url => $v) {

                $links[] = array(
                    'name' => $v,
                    'url' => $url
                );

            }

            //Liste des documents :
            $Files = $this->File->find('list', array('fields' => array('File.url', 'File.name')));

            foreach($Files as $url => $v) {

                $links[] = array(
                    'name' => $v,
                    'url' => $url
                );


            }

        }else{

            $links[] = array('name' => 'Choisir un mp3', 'url' => false);

            //Liste des documents :
            $Files = $this->File->find('list', array('fields' => array('File.url', 'File.name'), 'conditions' => array('File.extention_id' => $this->request->params['named']['extention_id'])));

            foreach($Files as $url => $v) {

                $links[] = array(
                    'name' => $v,
                    'url' => $url
                );


            }

        }





        echo json_encode($links);


        exit();
    }



    /**
     * Admin edit
     *
     * @param null $id
     */
    public function admin_edit($id = null, $locale = 'fre')
    {
        $this->loadModel('ModuleManager.ContentModule');

        $data = $this->data;

        if (!empty($data)) {


            /**
             * Set locale
             */

            /**
             * Enregistrement des données de la page
             */
            $this->Page->locale = $data['Page']['locale'];

            $this->Page->create();

            if (empty($data['Page']['acces_membre'])) $data['Page']['acces_membre'] = 0;
            if (empty($data['Page']['rss_name'])) $data['Page']['rss_name'] = null;
            if (empty($data['Page']['rss_description'])) $data['Page']['rss_description'] = null;

            if ($this->Page->save($data)) {

                $id = $this->Page->id;

                $this->ContentModule->deleteAll(array('ContentModule.model' => 'Page', 'ContentModule.model_id' => $id));

                if (is_array($data['Contents'])) {

                    foreach($data['Contents'] as $k => $v) {

                        $this->ContentModule->save(array(
                           'id'             => null,
                            'model'         => 'Page',
                            'model_id'      => $id,
                            'position'      => $k,
                            'module'        => $v['ContentModule']['module'],
                            'content'       => $v['ContentModule']['content']
                        ));
                    }
                }

                $res['ok'] = true;
                $res['id'] = $id;

            } else {

                $res['ok'] = false;
            }

            exit(json_encode($res));
        }

        $res['Data']        = false;
        $res['Parents']     = $this->Page->generateTreeList(null, '{n}.Page.id', '{n}.Page.name', ' - ');

        if (!empty($id)) {

            $this->Page->locale = $locale;

            $res['Data'] = $this->Page->findById($id);

            $this->Page->locale = 'fre';

            $res['Data']['Chemin'] = $this->Page->getPath($id);

            $this->ContentModule->locale = $locale;

            $res['Data']['Contents'] = $this->ContentModule->find(
                'all',
                array(
                    'order' => 'ContentModule.position',
                    'conditions' => array(
                        'ContentModule.model'       => 'Page',
                        'ContentModule.model_id'    => $id
                    )
                )
            );
        }

        exit(json_encode($res));
    }


    public function admin_test($id) {


        $this->loadModel('Test');


        $this->Test->create();

        $this->Test->save(array('Test' => array('name' => 'essau')));


        $r = $this->Test->find('first', array('conditions' => array('Test.id' => $this->Test->id)));


        debug($r);


        /*
        $this->Page->locale = 'fre';

        $res['Data'] = $this->Page->findById($id);


        debug($res['Data']);
        */

        exit();


    }


    /**
     * Admin delete
     *
     * @param $id
     */
    public function admin_delete($id)
    {

        $res['ok'] = false;

        $Page = $this->Page->find('all', array('conditions' => array('Page.parent_id' => $id)));

        if (!count($Page)) {

            $this->Page->delete($id);

            $this->loadModel('ModuleManager.ContentModule');
            $this->ContentModule->deleteAll(array('ContentModule.model' => 'Page', 'ContentModule.model_id' => $id));


            $res['ok'] = true;

        }

        echo json_encode($res);

        exit();


    }

    /**
     * Ordre
     */
    public function admin_ordre()
    {

        $res['ok'] = false;

        if (!empty($this->request->data)) {

            $data = $this->request->data['data'];

            $data = explode(',', $data);

            foreach ($data as $position => $id) {

                $this->Page->id = $id;

                if ($this->Page->saveField('position', $position)) {


                }

            }

            //On réorganise l'arbre pour qu'il soit dans le bon ordre
            $id = $this->Page->field('parent_id', array('Page.id' => $id));

            if (empty($id)) $id = null;

            $this->Page->reorder(array('id' => $id, 'field' => 'Page.position', 'order' => 'ASC'));

            $res['ok'] = true;

        }

        exit(json_encode($res));

    }


    /**
     * Admin view
     *
     * @param $id
     */
    public function view($id)
    {

        $this->loadModel('ModuleManager.ContentModule');


        $page = $this->Page->findById($id);
        $this->set('page', $page);


        $_breadcrumb = array(
            'Model' => 'Page',
            'Chemin' => $this->Page->getPath($page['Page']['id'])
        );
        $this->set('_breadcrumb', $_breadcrumb);


        //Liste des contenus de la page
        $ContentModules = $this->ContentModule->find(
            'all',
            array(
                'conditions' => array(
                    'ContentModule.model'       =>  'page',
                    'ContentModule.model_id'    =>  $page['Page']['id'],
                ),
                'order' => array(
                    'ContentModule.position' => 'ASC'
                )
            )
        );

        $this->set('ContentModules', $ContentModules);


        //Menu page



        //
        if (!empty($page['Page']['template'])) {

            $template = $page['Page']['template'];

            if (!empty($template)) {

                if (method_exists($this, $page['Page']['template'])) {

                    $this->$template($page, $_breadcrumb);
                }

                if (file_exists(APP . '/View/Pages/' . $template . '.ctp')) {

                    $this->render($template);
                }

            }

        }

    }

    private function home_metier() {

        //Les métiers
        $metiers = $this->Page->find('all', array('conditions' => array('Page.template' => 'metier'), 'order' => 'Page.position'));


        $this->set('metiers', $metiers);

    }
    private function contact()
    {

        if(!empty($this->request->data)) {

            $this->loadModel('ContactManager.Contact');

            $data = $this->request->data;

            $this->Contact->set($data);

            if($this->Contact->validates($data)) {

                if($this->Contact->send($data)) {

                    $statut['statut'] = 1;
                    $statut['message']['titre']     = 'Votre message a été envoyé !';
                    $statut['message']['texte']     = 'Nous reprendrons contact avec vous dans les plus brefs délais. ';

                } else {

                    $statut['statut'] = 0;
                    $statut['message']['titre']     = 'Erreur lors de l’envoi du message !';
                    $statut['message']['texte']     = 'Merci d’essayer à nouveau. ';
                }

            } else {

                $statut['statut']               = 0;
                $statut['erreurs']              = $this->Contact->validationErrors;
                $statut['message']['titre']     = 'Le formulaire contient des erreurs !';
                $statut['message']['texte']     = 'Veuillez contrôler les champs marqués de rouge. ';

            }

            $this->set('statut', $statut);

        }

    }

    private function metier ($page) {

        $this->loadModel('Blog.Actualite');

        //Récupérer la liste des pages enfants :
        $childrens = $this->Page->children($page['Page']['id'], true, array('Page.id'));

        $ids = array();
        foreach ($childrens as $v) $ids[] = $v['Page']['id'];

        //Liste des références :
        $ids[] = $page['Page']['id'];


        $Refs = $this->Actualite->find('all', array('conditions' => array('Actualite.type' => 'reference', 'Actualite.etat_id' => 'publish', 'Actualite.id' => $ids)));


        $this->set('Refs', $Refs);

    }

    private function home_real ($page) {

        $this->loadModel('Blog.Actualite');

        //Liste de toutes les réalisations
        $Refs = $this->Actualite->find('all', array('conditions' => array('Actualite.type' => 'reference', 'Actualite.etat_id' => 'publish', 'Page.template' => 'realisation')));

        $this->set('Refs', $Refs);


        //Liste des type des réalisations
        $Reals = $this->Page->find('all', array('order' => 'position', 'conditions' => array('Page.template' => 'realisation')));

        $this->set('Reals', $Reals);


    }

    private function realisation ($page) {

        $this->loadModel('Blog.Actualite');

        //Liste de toutes les réalisations
        $Refs = $this->Actualite->find('all', array('conditions' => array('Actualite.type' => 'reference', 'Actualite.etat_id' => 'publish')));

        $this->set('Refs', $Refs);


    }

    private function blog ($page, $_breadcrumb) {

        $this->loadModel('Blog.Actualite');

        $this->Paginator->settings = array(
            'paramType' => 'querystring',
            'limit' => 5,
            'order' => array(
                'Actualite.created' => 'desc'
            )
        );


        //Liste des articles
        $Actus = $this->Paginator->paginate(
            'Actualite',
            array(
                'Actualite.etat_id' => 'publish',
            )
        );

        $this->set('Actus', $Actus);

        //Populaires :
        $pops = $this->Actualite->find('all', array('limit' => 3, 'order' => 'Actualite.created DESC', 'conditions' => array('Actualite.etat_id' => 'publish')));
        $this->set('pops', $pops);

        //Liste des sous pages (les mots clés)
        $sousPage = $this->Page->children($_breadcrumb['Chemin'][0]['Page']['id'], true);
        $this->set('sousPage', $sousPage);


    }

    public function admin_getRubriquesOptions() {

        $options = $this->Page->generateTreeList(array('Page.template' => 'rubrique'), '{n}.Page.id', '{n}.Page.name', ' - ');

        $return = array();

        foreach ($options as $k => $v) {

            $return[] = array('id' => $k, 'label' => $v);

        }


        exit(
        json_encode(
            array(
                'statut' => 1,
                'data' => $return
            )
        )
        );

    }


}