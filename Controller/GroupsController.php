<?php
/**
 * controllers/UsersController.php
 */
class GroupsController extends AppController
{
    /**
     * The controller name
     *
     * @var string $name
     */
    var $name = 'Groups';

    /**
     * Admin index
     *
     * @return void
     */
    function admin_index()
	{

		$groups = $this->Group->find('all');

		exit(
			json_encode(
				$groups
			)
		);

	}

    /**
     * Admin edit
     *
     * @param null $id
     */
    function admin_edit($id = null)
	{

		if (!empty($this->data)) {

			if ($this->Group->save($this->data)) {

	            $txt = (!empty($this->data['Group']['id'])) ? 'Groupe modifié' : 'Groupe créé'; 

	            $this->Session->setFlash('<strong>'.$txt.' !</strong>', 'success');

	            $this->redirect(array('action' => 'index'));

			} else {

				$this->Session->setFlash('<strong>Le formulaire contient des erreurs</strong>', 'error');
			}
		}

		if ($id) {

			$this->Group->id = $id;

			$this->data = $this->Group->read();
		}

		$this->set('Groups', $this->Group->generateTreeList(null, '{n}.Group.id', '{n}.Group.name', ' - '));

		$this->set('MenuAdminActives', array(45, 47));
	}

    /**
     * Admin delete
     *
     * @param $id
     */
    function admin_delete($id)
	{
 		$this->Group->delete($id);
		$
		$this->Session->setFlash('Le groupe a été supprimé.', 'success');

		$this->redirect('index');
	}

}
