<?php

/**
 * Class PartenairesController
 */
class PartenairesController extends AppController
{


    /**
     * Admin
     */
    function admin_index()
    {

        echo json_encode($this->Partenaire->find('all', array('order' => 'Partenaire.position' ,'conditions' => array('Partenaire.type' => 'partenaire'))));

        exit();
    }


    /**
     * Admin edit
     *
     * @param null $id
     */
    public function admin_edit($id = null)
    {
        $this->loadModel('ModuleManager.ContentModule');

        $res['ok'] = false;

        if (!empty($this->data)) {

            $data = $this->data;

            /**
             * Enregistrement des données de la Partenaire
             */
            if ($this->Partenaire->save($data)) {

                $id = $this->Partenaire->id;

                $this->ContentModule->deleteAll(array('ContentModule.model' => 'Partenaire', 'ContentModule.model_id' => $id));

                if (is_array($data['Contents'])) {

                    foreach($data['Contents'] as $k => $v) {

                        $this->ContentModule->save(array(
                           'id'             => null,
                            'model'         => 'Partenaire',
                            'model_id'      => $id,
                            'position'      => $k,
                            'module'        => $v['ContentModule']['module'],
                            'content'       => $v['ContentModule']['content']
                        ));
                    }
                }

                $res['ok'] = true;
                $res['id'] = $id;

            } else {

                $res['ok'] = false;
            }


        }

        exit(json_encode($res));

    }


    public function admin_view($id) {


        $this->loadModel('ModuleManager.ContentModule');

        //Valeurs par default :
        $res['Data']        = array(
            'Partenaire' => array(
                'etat_id' => 'publish',
                'type' => 'partenaire',
                'color' => '#f05050'
            ),
            'Contents' => array()
        );

        if (!empty($id)) {

            $res['Data'] = $this->Partenaire->findById($id);

            $res['Data']['Contents'] = $this->ContentModule->find(
                'all',
                array(
                    'order' => 'ContentModule.position',
                    'conditions' => array(
                        'ContentModule.model'       => 'Partenaire',
                        'ContentModule.model_id'    => $id
                    )
                )
            );

        }

        exit(json_encode($res));

    }


    /**
     * Admin delete
     *
     * @param $id
     */
    public function admin_delete($id)
    {

        $res['ok'] = false;

        $this->Partenaire->delete($id);

        $this->loadModel('ModuleManager.ContentModule');
        $this->ContentModule->deleteAll(array('ContentModule.model' => 'Partenaire', 'ContentModule.model_id' => $id));

        $res['ok'] = true;

        echo json_encode($res);

        exit();


    }

    /**
     * Ordre
     */
    function admin_ordre()
    {

        $res['ok'] = false;

        if(!empty($this->request->data)) {

            $data = $this->request->data['data'];

            $data = explode(',', $data);

            foreach ($data as $position => $id) {

                $this->Partenaire->id = $id;

                if($this->Partenaire->saveField('position', $position)) {


                }

            }

            $res['ok'] = true;
        }

        exit(json_encode($res));

    }


    /**
     * Admin view
     *
     * @param $id
     */
    public function view($id)
    {

    }

}