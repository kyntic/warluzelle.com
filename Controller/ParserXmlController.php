<?php

/**
 * Class ParserXmlController
 */
class ParserXmlController extends AppController
{
    /**
     * Admin index
     *
     * @return void
     */
    public function admin_index()
	{
		App::uses('Xml', 'Utility');

		$xmlString = file_get_contents('http://escapade-voyages.fr/gestour/cache/gestour_cache.php', false);
		
		if (!$xmlString) {
			
			debug('Erreur');
		}

		$xml = Xml::toArray(Xml::build($xmlString));

		debug($xml);

		exit();
	}
}