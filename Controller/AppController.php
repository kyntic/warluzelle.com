<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Class AppController
 */
class AppController extends Controller
{
    /**
     * The helpers
     *
     * @var array $helpers
     */
    public $helpers = array(
        'WhDate',
        'Session',
        'Html',
        'Form',
        'WhForm',
        'Js' => array(
            'Jquery'
        ),
        'Text',
        'FileManager.WhFile'
    );

    /**
     * The components
     *
     * @var array $components
     */
    public $components = array(
        'Auth',
        'Session',
        'Cookie',
        'RequestHandler'
    );


    /**
     * Breadcrumb
     *
     * @var string $_breadcrumb
     */
    public $_breadcrumb;

    /**
     * Before filter
     *
     * @return void
     */
    function beforeFilter()
    {

        if($this->params['controller'] != 'install') {


            /**
             * Ecritures des parametres
             */
            $this->loadModel('Parametre');

            $_params = $this->Parametre->find('first');

            if (!empty($_params)) {

                foreach ($_params['Parametre'] as $k => $v) {

                    Configure::write('Params.'.$k, $v);
                }
            }


        }


        /**
         * Gestion des prefixe
         */
        if ((isset($this->params['prefix']) && $this->params['prefix'] == 'admin') ||
            $this->action == 'admin_login' ||
            $this->action == 'admin_logout'
        ) {

            /**
             * Fonctions d'administration
             */
            $this->_administration();

        } elseif((isset($this->params['prefix']) && $this->params['prefix'] == 'membre')) {

            /**
             * Fonctions pour les membres
             */
            $this->_membreBeforeFilter();

        } else {

            /**
             * Fonction pour les visiteurs
             */
            $this->_publicBeforeFilter();
        }


    }

    /**
     * Before render
     *
     * @return void
     */
    function beforeRender()
    {
        if ((isset($this->params['prefix']) && $this->params['prefix'] == 'membre')) {

            $this->_membreBeforeRender();
        }

        $this->_PublicBeforeRender();
    }

    /**
     * Administration
     *
     * @return void
     */
    private function _administration()
    {
        $this->Auth->loginAction    = '/admin/users/login';
        //$this->Auth->ajaxLogin      = '/admin/users/error_403';
        $this->Auth->authorize      = 'Controller';
        $this->Auth->authError      = '<strong>Veuillez vous identifier</strong>';
        $this->Auth->logoutRedirect = '/admin/users/login';
        $this->Auth->authenticate   = array(
            'Form' => array(
                'fields' => array('username' => 'email'),
                'scope'  => array('User.disabled' => 0, 'User.verified' => 1, 'User.group_id' => array(1, 2)) //Groupe des administrateurs
            ));


        /**
         * Layout
         */
        $this->layout = $this->params['prefix'] . '_default';
    }

    /**
     * Membre Before filter
     *
     * @return void
     */
    private function _membreBeforeFilter()
    {
        $this->Auth->loginAction    = '/users/login';
        $this->Auth->authorize      = 'Controller';
        $this->Auth->authError      = '<strong>Cette section n’est pas accessible, veuillez vous identifier. </strong>';
        $this->Auth->logoutRedirect = '/';
        $this->Auth->authenticate   = array(
            'Form' => array(
                'userModel' => 'User',
                'fields' => array('username' => 'email', 'password' => 'password'),
                'scope'  => array('User.disabled' => 0, 'User.disabled' => 0)
            )
        );

        $this->layout = 'membre_default';
    }

    /**
     * Public Before Filter
     *
     * @return void
     */
    private function _publicBeforeFilter ()
    {
        $this->Auth->loginAction    = '/users/login';
        $this->Auth->authorize      = 'Controller';
        $this->Auth->authError      = '<strong>Cette section n’est pas accessible, veuillez vous identifier. </strong>';
        $this->Auth->logoutRedirect = '/';
        $this->Auth->authenticate   = array(
            'Form' => array(
                'userModel' => 'User',
                'fields' => array('username' => 'email', 'password' => 'password'),
                'scope'  => array('User.disabled' => 0, 'User.verified' => 1)
            ));

        if (empty($this->params['prefix'])) {

            $this->Auth->allow();
        }
    }

    /**
     * Public Before Render
     *
     * @return void
     */
    private function _PublicBeforeRender()
    {


        if($this->params['controller'] != 'install') {



            $this->loadModel('Page');

            if (!empty($this->Cont)) {

                $this->set('Cont', $this->Cont);
            }

            $this->helpers['WhTree'] = array();

            $_menuPages = (Configure::read('debug') == 0) ? Cache::read('_menuPages') : false;
            $_menuPages = false;

            if (!$_menuPages) {

                $_menuPages = $this->Page->find('threaded', array('conditions' => array('Page.etat_id' => 'publish', 'Page.menu_id'), 'order' => 'Page.position'));

                Cache::write('_menuPages', $_menuPages);

            }

            $_lienContact = $this->Page->find('first', array('conditions' => array('Page.template' => 'contact')));
            $_lienMentionsLegales = $this->Page->find('first', array('conditions' => array('Page.template' => 'mention_legale')));

            $this->set(compact('_menuPages', '_lienContact', '_lienMentionsLegales'));



            /*
            * Publication et dépublication des actualités programmée
            */
            $this->loadModel('Blog.Actualite');
            $this->Actualite->updateAll(
                array('Actualite.etat_id' => '"publish"'),
                array(
                    'Actualite.publication_date < ' => time(),
                    'Actualite.etat_id ' => 'publish_date',
                    'OR' => array(
                        'Actualite.publication_date_fin > ' => time(),
                        '!Actualite.publication_date_fin '
                    )
                )
            );

            $this->Actualite->updateAll(
                array('Actualite.etat_id' => '"draft"'),
                array(
                    'Actualite.etat_id ' => 'publish',
                    'Actualite.publication_date_fin < ' => time(),
                    'Actualite.publication_date_fin !=' => 0,

                )
            );

        }

        $this->theme = Configure::read('theme');


    }

    /**
     * Membre Before Render
     *
     * @return void
     */
    private function _membreBeforeRender()
    {

    }

    /**
     * Is Authorized user
     *
     * @param   null $user
     * @return  bool
     */
    public function isAuthorized($user = null)
    {
        if (!$user) {

            return false;
        }

        $this->loadModel('User');

        $Connecte = $this->User->findById($user['id']);

        if (!$Connecte) {

            return false;
        }

        if (!empty($this->request->params['prefix'])) {

            if ($this->request->params['prefix'] == 'admin' &&
                in_array($Connecte['User']['group_id'], array(1, 2)) &&
                $Connecte['User']['verified']
            ) {

                return true;
            }

            if ($this->request->params['prefix'] == 'membre') {

                return true;
            }

        } else {

            return true;
        }

        return false;
    }

    /**
     * Set Error Layout
     *
     * @return void
     */
    function _setErrorLayout()
    {
        if ($this->name == 'CakeError') {


        }
    }
}
