<?php

	$racine = getcwd() . '/';

	// GIT PULL
	exec('git pull', $output);
	foreach ($output as $v) {
		echo $v . "\n";
	}

	// CHMOD 755 des fichiers console
	chdir($racine . 'Console');
	exec('chmod 755 cake');
	exec('chmod 755 cake.bat');

	chdir($racine . 'cakephp/lib/Cake/Console');
	exec('chmod 755 cake');
	exec('chmod 755 cake.bat');

	chdir($racine . 'cakephp/vendors');
	exec('chmod 755 cakeshell');
