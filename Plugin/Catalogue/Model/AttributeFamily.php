<?php

class AttributeFamily extends CatalogueAppModel {

	public $actsAs = array(
		'Tree'
	);

	public function beforeSave($options = array())
	{

        if(!empty($this->data['AttributeFamily']['id'])) $attributFamille = $this->findById($this->data['AttributeFamily']['id']);

        if(!isset($this->data['AttributeFamily']['id']) || empty($this->data['AttributeFamily']['id']) || $attributFamille['AttributeFamily']['position'] == 0)
        {

			$positionMax = $this->find('first', array(
				'order' => array('AttributeFamily.position' => 'DESC')
			));
			$position = (!empty($positionMax)) ? $positionMax['AttributeFamily']['position'] + 1 : 1;

			$this->data['AttributeFamily']['position'] = $position;

		}

	}

}