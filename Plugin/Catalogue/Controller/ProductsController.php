<?php

class ProductsController extends CatalogueAppController {

    /**
     * Liste
     */
    public function admin_index($rubrique_id = null)
    {

        $conditions = array();

        if (!empty($rubrique_id)) {
            $conditions['Product.rubrique_id'] = $rubrique_id;
        }

    	$products = $this->Product->find(
    		'all',
            array(
                'conditions'    =>  $conditions,
                'order'         =>  array('Product.name' => 'ASC')
            )
    	);

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $products
                )
            )
        );

    }

    /**
     * Retourne
     */
    public function admin_view($id = null)
    {

        $this->loadModel('Catalogue.ProductAttribute');

        $product = $this->Product->findById($id);
        if (!$product) {

            exit(
                json_encode(
                    array(
                        'statut'    =>  0
                    )
                )
            );
        }

        $product['Product']['attributes'] = $this->ProductAttribute->getAttributesByFamily($id);

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $product
                )
            )
        );

    }

    /**
     * Enregistre
     */
    public function admin_edit($id = null)
    {

        $this->loadModel('Catalogue.ProductAttribute');

        if (!empty($this->request->data)) {

            $data = $this->request->data;
            $data['Product']['content_type_id'] = 'product';

            if ($this->Product->save($data)) {

                if (!$id) {

                    $id = $this->Product->getLastInsertID();
                }

                if (!isset($data['Product']['attributes']) || !is_array($data['Product']['attributes'])) {

                    $data['Product']['attributes'] = array();
                } else {

                    $productAttributes = array();
                    foreach ($data['Product']['attributes'] as $attributes) {
                        if (is_array($attributes)) {
                            $productAttributes = array_merge($productAttributes, $attributes);
                        }
                    }

                    $data['Product']['attributes'] = $productAttributes;

                }

                if ($this->ProductAttribute->saveAttributes($id, $data['Product']['attributes'])) {

                    exit(
                        json_encode(
                            array(
                                'statut'    =>  1,
                                'data'      => array(
                                    'Product' => array(
                                        'id' => $id
                                    )
                                )
                            )
                        )
                    );

                }

            }

        }

        exit(
            json_encode(
                array(
                    'statut'    =>  0
                )
            )
        );

    }

    /**
     * Supprime
     */
    public function admin_delete($id = null)
    {

        $this->loadModel('Catalogue.ProductAttribute');

        if($this->Product->delete($id) && $this->ProductAttribute->deleteAttributes($id)) {

            exit(
                json_encode(
                    array(
                        'statut'    =>  1
                    )
                )
            );

        }

        exit(
            json_encode(
                array(
                    'statut'    =>  0
                )
            )
        );

    }

    /**
     * Ordre
     */
    public function admin_ordre()
    {

        if (!empty($this->request->data)) {

            $data = $this->request->data['data'];
            $data = explode(',', $data);

            foreach ($data as $position => $id) {

                $this->Product->id = $id;

                if (!$this->Product->saveField('position', $position + 1)) {

                    exit(
                        json_encode(
                            array(
                                'statut'    =>  0
                            )
                        )
                    );

                }

            }

            exit(
                json_encode(
                    array(
                        'statut'    =>  1
                    )
                )
            );

        }

        exit(
            json_encode(
                array(
                    'statut'    =>  0
                )
            )
        );

    }

    public function admin_duplique($id)
    {

        $this->loadModel('Catalogue.ProductAttribute');

        $product = $this->Product->findById($id);
        if (!$product) {

            exit(
                json_encode(
                    array(
                        'statut'    =>  0
                    )
                )
            );

        }

        $product['Product']['id'] = null;
        $product['Product']['position'] = 0;

        if ($this->Product->save($product)) {

            $productId = $this->Product->getLastInsertID();

            $productAttributes = $this->ProductAttribute->find(
                'all',
                array(
                    'conditions' => array(
                        'ProductAttribute.product_id'   =>  $id
                    )
                )
            );

            foreach ($productAttributes as $productAttribute) {

                $productAttribute['ProductAttribute']['id'] = null;
                $productAttribute['ProductAttribute']['product_id'] = $productId;

                if (!$this->ProductAttribute->save($productAttribute)) {

                    exit(
                        json_encode(
                            array(
                                'statut'    =>  0
                            )
                        )
                    );

                }

            }

            exit(
                json_encode(
                    array(
                        'statut'    =>  1
                    )
                )
            );

        }

        exit(
            json_encode(
                array(
                    'statut'    =>  0
                )
            )
        );

    }

    /**
     * Affichage d'un produit
     *
     * @param null $id
     */
    public function view($id = null)
    {
        /**
         * The models we need
         */
        $this->loadModel('Page');
        $this->loadModel('Catalogue.ProductAttribute');
        $this->loadModel('Catalogue.Attribute');
        $this->loadModel('Blog.Actualite');

        /**
         * Get the product
         */
        $product = $this->Product->findById($id);
        $this->set('product', $product);

        $_breadcrumb = array(
            'Model' => 'Page',
            'Chemin' => $this->Page->getPath($product['Product']['rubrique_id'])
        );
        $this->set('_breadcrumb', $_breadcrumb);

        $attributeIds = $this->ProductAttribute->getAttributes($id);

        $attributes = $this->Attribute->find(
            'all',
            array(
                'conditions' => array(
                    'Attribute.id'                  =>  $attributeIds,
                    'Attribute.vignette !='         =>  '',
                    'AttributeFamily.speciale'      =>  1,
                    'AttributeFamily.id NOT'        =>  array(2, 4, 5)
                ),
                'order' => array(
                    'AttributeFamily.position'  =>  'ASC',
                    'Attribute.position'        =>  'ASC',
                )
            )
        );
        $this->set('attributes', $attributes);

        // Chargement des filtres
        $this->data = $this->Session->read('Filtres.' . $product['Product']['rubrique_id']);

        $produitIds = $this->Product->getProductIds($product['Product']['rubrique_id']);

        /**
         * Get filters
         */
        $filtres = $this->ProductAttribute->getFiltres(true, $produitIds);

        $this->set('filtres', $filtres);

        // Chargement des autres produits de la rubrique
        $autreProduits = $this->Product->find(
            'all',
            array(
                'conditions' => array(
                    'Product.id !='         =>  $product['Product']['id'],
                    'Product.etat_id'       =>  'publish',
                    'Product.rubrique_id'   =>  $product['Product']['rubrique_id']
                ),
                'order' => array(
                    'Product.position' => 'ASC'
                )
            )
        );
        $this->set('autreProduits', $autreProduits);

        // Chargement des réalisations
        $actualites = $this->Actualite->find(
            'all',
            array(
                'conditions' => array(
                    'Actualite.etat_id'     =>  'publish',
                    'Actualite.rubrique_id' =>  $product['Product']['rubrique_id']
                )
            )
        );
        $this->set('actualites', $actualites);

    }

    public function admin_import()
    {

        $this->loadModel('Catalogue.Product');
        $this->loadModel('OldProduit');
        $this->OldProduit->bindModel(
            array(
                'belongsTo' => array(
                    'OldContent' => array(
                        'foreignKey' => 'content_id'
                    )
                )
            )
        );

        $produits = $this->OldProduit->find('all');

        foreach ($produits as $produit) {

            $prix_ttc = (!empty($produit['OldProduit']['prix_net'])) ? $produit['OldProduit']['prix_net'] : $produit['OldProduit']['prix_mini'];
            if(!$prix_ttc) {
                $prix_ttc = 0;
            }

            $data = array(
                'Product'   => array(
                    'id'    =>  null,
                    'content_type_id'   =>  'product',
                    'etat_id'           =>  'publish',
                    'rubrique_id'       =>  null,
                    'name'              =>  $produit['OldContent']['h1'],
                    'h1'                =>  $produit['OldContent']['h1'],
                    'resume'            =>  $produit['OldContent']['resume'],
                    'reference'         =>  '',
                    'prix_ttc'          =>  $prix_ttc,
                    'prix_barre_ttc'    =>  0,
                    'dimensions'        =>  '',
                    'description'       =>  $produit['OldProduit']['description'],
                    'caracteristiques'  =>  $produit['OldContent']['body'],
                )
            );

            $product = $this->Product->findByName($produit['OldContent']['h1']);
            if (!empty($product)) {
                $data['Product']['id'] = $product['Product']['id'];
            }

            $this->Product->save($data);

        }

        exit();

    }

}