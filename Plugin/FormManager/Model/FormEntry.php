<?php

/**
 * Class FormEntry
 */
class FormEntry extends FormManagerAppModel
{
    /**
     * The belongs to
     *
     * @var array $belongsTo
     */
    public $belongsTo = array(
        'FormElement' => array(
            'foreignKey' => 'element_id'
        )
    );

    /**
     * After Find
     *
     * @param   mixed       $results
     * @param   bool        $primary
     * @return  mixed|void
     */
    public function afterFind($results, $primary = false)
    {
        if (empty($results)) {

            return $results;
        }

        $formattedResults = array();

        foreach ($results as &$result) {

            if (!isset($result['FormEntry'])) {

                continue;
            }

            if (!isset($formattedResults[$result['FormEntry']['collection_id']])) {

                $formattedResults[$result['FormEntry']['collection_id']] = array();
            }

            if ($this->_isJson($result['FormEntry']['data'])) {

                $decodedJson = json_decode($result['FormEntry']['data'], true);

                if (is_array($decodedJson) && isset($result['FormElement']['configuration']['options'])) {

                    $options = explode(',', $result['FormElement']['configuration']['options']);


                    foreach ($decodedJson as &$value) {

                        $value = $options[$value];
                    }

                    $result['FormEntry']['data'] = implode(':', $decodedJson);
                }
            }

            $formattedResults[$result['FormEntry']['collection_id']]['FormEntry'][] = $result['FormEntry'];
        }

        return $formattedResults;
    }

    /**
     * @param null $string
     *
     * @return bool
     */
    private function _isJson($string = null)
    {
        /**
         * Decode the string
         */
        json_decode($string, true);

        /**
         * If the decode is ok - Return true - It's a json
         */
        if (json_last_error() == 0) {

            return true;
        }

        return false;
    }
}