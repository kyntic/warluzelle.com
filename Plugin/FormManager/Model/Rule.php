<?php

/**
 * Class Rule
 */
class Rule extends FormManagerAppModel
{
    /**
     * This model does note use table
     *
     * @var bool $useTable
     */
    public $useTable = false;

    /**
     * The path for the rules
     *
     * @var string $_rulesFilePath
     */
    private $_rulesFilePath;

    /**
     * The rules from the XML
     *
     * @var $_rules
     */
    private $_rules;

    /**
     * Default constructor
     *
     * @param bool $id
     * @param null $table
     * @param null $ds
     */
    public function __construct($id = false, $table = null, $ds = null)
    {
        /**
         * Parent constructor
         */
        parent::__construct($id = false, $table = null, $ds = null);

        $this->_rulesFilePath = APP . 'Plugin/FormManager/rules/rules.xml';

        $this->_loadXml();
    }

    /**
     * Get the tags list from XML
     *
     * @return array
     */
    public function getTagsList()
    {
        $tags = array();

        foreach ($this->_rules['data']['tag'] as $tag) {

            $tags[$tag['@type']] = $tag['@name'];
        }

        return $tags;
    }

    /**
     * Get tag types from tag
     *
     * @param   null            $tagType
     * @return  false|array
     */
    public function getTagTypes($tagType = null)
    {
        /**
         * No tag type
         */
        if ($tagType == null) {

            return false;
        }

        $types = array();

        foreach ($this->_rules['data']['tag'] as $tag) {

            if ($tag['@type'] == $tagType) {

                if (isset($tag['type']['@disabled']) &&
                    $tag['type']['@disabled'] == 'false'
                ) {

                    $types[$tag['type']['@name']] = $tag['type']['@prettyname'];

                    return $types;
                }

                foreach ($tag['type'] as $type) {


                    /**
                     * Don't add disabled types
                     */
                    if ($type['@disabled'] == 'true') {

                        continue;
                    }

                    $types[$type['@name']] = $type['@prettyname'];
                }
            }
        }

        return $types;
    }

    /**
     * Get fields from tag type
     *
     * TODO : Rendre le XML propre ... c'est un peu degueulasse quand meme
     *
     * @param   null    $tagType
     * @param   null    $tagName
     * @return  bool
     */
    public function getFieldsFromTagType($tagType = null, $tagName = null)
    {
        /**
         * We don't have the data
         */
        if ($tagType == null ||
            $tagName == null
        ) {

            return false;
        }

        foreach ($this->_rules['data']['tag'] as $key => $tag) {

            if ($tag['@type'] != $tagName) {

                continue;
            }

            /**
             * Only one type
             */
            if (isset($tag['type']['field'])) {

                return $tag['type']['field'];
            }

            foreach ($tag['type'] as $type) {

                if (isset($type['@name']) &&
                    $type['@name'] != $tagType
                ) {
                    continue;
                }

                if (isset($type['field'])) {

                    return $type['field'];
                }

                return $type;
            }
        }

        return false;
    }

    /**
     * Validates the configuration
     *
     * @param array     $configuration
     * @param null      $tagName
     * @param null      $tagType
     *
     * @return array|bool
     */
    public function validateConfiguration(array $configuration = array(), $tagName = null, $tagType = null)
    {
        /**
         * Data is not valid
         */
        if (empty($configuration)   ||
            $tagName == null        ||
            $tagType == null
        ) {
            return false;
        }

        $errors = array();

        foreach ($this->_rules['data']['tag'] as $tag) {

            if ($tag['@type'] != $tagName) {

                continue;
            }

            foreach ($tag['type'] as $type) {

                if (isset($type['@name']) && $type['@name'] != $tagType) {

                    continue;
                }

                if (!isset($type['field'])) {

                    $type['field'] = $type;
                }

                foreach ($type['field'] as $field) {

                    if (isset($field['rule']) &&
                        isset($configuration[$field['@name']])
                    ) {
                        /**
                         * The field to validate
                         */
                        $fieldToValidate = $configuration[$field['@name']];

                        /**
                         * The validation
                         */
                        $validation = $this->_validateField($field['rule'], $fieldToValidate);

                        /**
                         * Single validation
                         */
                        if (!is_array($validation)) {

                            if (!$validation) {

                                $errors[$field['@name']] = $field['rule']['message'];
                            }
                        } else {

                            foreach ($validation as $key => $valid) {

                                if (!$valid) {

                                    $errors[$field['@name']] = $field['rule'][$key]['message'];
                                }
                            }
                        }
                    }
                }
            }
        }

        return $errors;
    }

    /**
     * Validates a field
     *
     * @param   array       $rule
     * @param   string      $fieldToValidate
     * @return  array
     */
    private function _validateField(array $rule = array(), $fieldToValidate)
    {
        /**
         * Single rule
         */
        if (isset($rule['type'])) {

            $validationType = $rule['type'];

            return Validation::$validationType($fieldToValidate);
        }

        $validations = array();

        foreach ($rule as $validation) {

            $validationType = $validation['type'];

            $validations[] = Validation::$validationType($fieldToValidate);
        }

        return $validations;
    }

    /**
     * Load the XML
     */
    private function _loadXml()
    {
        /**
         * XML Class
         */
        App::uses('Xml', 'Utility');

        /**
         * Get the rules from XML
         */
        $this->_rules = Xml::toArray(Xml::build($this->_rulesFilePath));
    }
}