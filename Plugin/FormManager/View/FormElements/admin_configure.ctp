<?php
$this->assign('titre', 'Configuration d\'un élément');
echo $this->Smart->create('FormElement', array('class' => 'smart-form'));
?>

<div class="modal-body">
    <fieldset>

        <?php if (count($tagTypes) == 1) : ?>

            <?php echo $this->Smart->hidden('FormElement.sub_type', array('value' => key($tagTypes)));?>
            <div class="fields">
                <?php echo $this->Smart->createFormManagerElements($fields); ?>
            </div>

        <?php elseif (!empty($this->data['FormElement']['sub_type'])) : ?>

            <?php echo $this->Smart->select('FormElement.sub_type', 'Sous-type d\'élément', $tagTypes, array('class' => 'select2'));?>

            <div class="fields">
                <?php echo $this->Smart->createFormManagerElements($fields); ?>
            </div>

        <?php else : ?>

            <?php echo $this->Smart->select('FormElement.sub_type', 'Sous-type d\'élément', $tagTypes, array('class' => 'select2'));?>

        <?php endif; ?>

        <?php echo $this->Smart->hidden('FormElement.tag_name', array('value' => $tagName));?>
        <?php echo $this->Smart->hidden('FormElement.id') ?>

        <div id="fields">

        </div>
    </fieldset>
</div>
<div class="modal-footer">
    <footer>

        <?php echo $this->Form->button(__('Enregistrer'), array('type' => 'submit', 'class' => 'btn btn-primary btn-xs'));?>

    </footer>
</div>

<?=$this->Smart->end();?>

<script type="text/javascript">

    /**
     * Loads the tag types
     */
    $('#FormElementSubType').on('change', function(e) {

        e.preventDefault();

        var tagType = $(this).val();
        var tagName = $('#FormElementTagName').val();

        $('.fields').remove();

        /**
         * No missing data
         */
        if (tagType != undefined &&
            tagName != undefined
        ) {

            $('#fields').load('/admin/form_manager/form_elements/get_fields/' + tagType + '/' + tagName);
        }
    });

    /**
     * Form submit action
     */
    $('#FormElementAdminConfigureForm').on('submit', function(e) {

        e.preventDefault();

        var form = $(this);

        $.ajax({
            type 		: 'POST',
            url 		: $(this).attr('action'),
            data 		: $(this).serialize(),
            cache 	    : false,
            complete	: function() {

                $(form).hideLoading()
            },
            success 	: function(response) {

                /**
                 * Get the response
                 */
                var response = jQuery.parseJSON(response);

                if (response.status == 0) {

                    if (response.errors != undefined) {

                        $('.input').removeClass('state-error');
                        $('.note-error').remove();

                        $.each(response.errors, function(index, value) {

                            var alert = '<div class="note note-error">' + value + '</div>';

                            $('input[name="data[FormElement][configuration][' + index + ']"]').parent().addClass('state-error');
                            $('input[name="data[FormElement][configuration][' + index + ']"]').after(alert);
                        });
                    }
                }

                if (response.status == 1) { //Ok

                    $('#myModal').modal('hide');
                }
            }
        });
    });

</script>