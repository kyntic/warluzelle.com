<?php
$this->assign('titre', 'Création d\'un formulaire');
$this->Html->addCrumb(__('Gestion des formulaires'), '/admin/form_manager/forms');
?>

<div class="row">
    <article class="col-sm-12 col-md-12 col-lg-6">

        <?php

        echo $this->Smart->openBox(
            array(
                'icone' => 'fa-edit',
                'titre' => 'Données du formulaire'
            )
        );
        ?>
        <div class="widget-body no-padding">


            <?php echo $this->Smart->create('Form', array('class' => 'smart-form'));?>

            <fieldset>

                <?php
                echo $this->Smart->input('Form.name', 'Nom : ');
                echo $this->Smart->hidden('Form.user_id', array('value' => $userId));
                ?>

            </fieldset>

            <footer>
                <?php echo $this->Smart->submit();?>
            </footer>

            <?php
            echo $this->Smart->end();
            ?>
        </div>
        <?php echo $this->Smart->closeBox();?>

    </article>
</div>
