<?php

/**
 * Class EvensController
 */
class EvensController extends BlogAppController
{
    /**
     * Admin index
     *
     * @return void
     */
    public function admin_index()
    {

        echo json_encode($this->Even->find('all', array('order' => 'Even.created DESC')));


        exit();
    }

    /**
     * Admin edit : Enregistrer
     *
     * @param null $id
     */
    public function admin_edit($id = null)
    {

        $this->loadModel('ModuleManager.ContentModule');
        $this->loadModel('ContentAssoc');


        if (!empty($this->data)) {

            $data = $this->data;

            /**
             * Enregistrement des données de la page
             */
            if(empty($data['Even']['all_day'])) $data['Even']['all_day'] = 0;
            if(empty($data['Even']['suscribe_nbr'])) $data['Even']['suscribe_nbr'] = 0;
            if(empty($data['Even']['acces_membre'])) $data['Even']['acces_membre'] = 0;

            if ($this->Even->save($data)) {

                $id = $this->Even->id;

                $this->ContentModule->deleteAll(array('ContentModule.model' => 'Even', 'ContentModule.model_id' => $id));

                if (is_array($data['Contents'])) {

                    foreach($data['Contents'] as $k => $v) {

                        $this->ContentModule->save(array(
                            'id'             => null,
                            'model'         => 'Even',
                            'model_id'      => $id,
                            'position'      => $k,
                            'module'        => $v['ContentModule']['module'],
                            'content'       => $v['ContentModule']['content']
                        ));
                    }
                }

                //ContentAss
                $ContentAss = array();
                if(!empty($data['Page']['ids'])) $ContentAss = $data['Page']['ids'];
                if(!empty($data['Even']['page_id'])) $ContentAss[] = $data['Even']['page_id'];
                $this->ContentAssoc->deleteAll(array('ContentAssoc.model' => 'Even', 'ContentAssoc.model_id' => $id));

                foreach($ContentAss as $i) {

                    $this->ContentAssoc->save(array('ContentAssoc' => array(
                        'id' => null,
                        'model'         => 'Even',
                        'model_id'      => $id,
                        'model_asso'    => 'Page',
                        'model_asso_id'    => $i
                    )));

                }


                $res['ok'] = true;
                $res['id'] = $id;

            } else {

                $res['ok'] = false;
                $res['error'] = $this->Even->invalidFields();
            }

            exit(json_encode($res));
        }

        exit();

    }


    /**
     * Voir un even
     * @param null $id
     * @param null $agenda_id
     */
    public function admin_view($id = null, $agenda_id = null)
    {

        $this->loadModel('ModuleManager.ContentModule');
        $this->loadModel('ContentAssoc');

        $this->loadModel('Agenda');

        //Valeurs par default :
        $res['Data']        = array(
            'Even' => array(
                'etat_id' => 'publish',
                'publication_date_fr' => date('d/m/Y'),
                'all_day' => 1,
                'type' => 'even',
                'color' => '#f05050'
            ),
            'Contents' => array(),
            'Agenda' => false

        );

        if (!empty($id)) {

            $res['Data'] = $this->Even->findById($id);


            $res['Data']['Contents'] = $this->ContentModule->find(
                'all',
                array(
                    'order' => 'ContentModule.position',
                    'conditions' => array(
                        'ContentModule.model'       => 'Even',
                        'ContentModule.model_id'    => $id
                    )
                )
            );

            $res['Data']['Page']['ids'] = $this->ContentAssoc->find('list', array('fields'  => array('model_asso_id'), 'group' => array('ContentAssoc.model_asso_id'), 'conditions' => array('ContentAssoc.model' => 'Even', 'ContentAssoc.model_id' => $id)));


        }elseif($agenda_id){

            $ag = $this->Agenda->findById($agenda_id);
            $res['Data']['Agenda'] = $ag['Agenda'];
            $res['Data']['Even']['agenda_id'] = $ag['Agenda']['id'];






        }

        exit(json_encode($res));
    }


    /**
     * Admin delete
     *
     * @param $id
     */
    public function admin_delete($id)
    {

        $res['ok'] = false;

        $this->Even->delete($id);

        $this->loadModel('ModuleManager.ContentModule');
        $this->ContentModule->deleteAll(array('ContentModule.model' => 'Even', 'ContentModule.model_id' => $id));

        $res['ok'] = true;

        echo json_encode($res);

        exit();


    }
    /**
     * Admin view
     *
     * @param int $id
     */
    public function view($id = null)
    {
        if ($id == null) {

            $this->redirect('/');
        }

        $this->loadModel('Page');
        $this->loadModel('Blog.ActualitePage');
        $this->loadModel('Blog.Actualite');
        $this->loadModel('Blog.EvenInscrit');

        $this->Cont = $this->Even->findById($id);

        $displayForm = false;

        if ($this->Cont['Even']['suscribe'] && !empty($this->Cont['Even']['suscribe_nbr'])) {

            /**
             * Number of subscribed people
             */
            $subscriptionsNumber = $this->EvenInscrit->find(
                'count',
                array(
                    'conditions' => array(
                        'EvenInscrit.even_id' => $id
                    )
                )
            );

            $subscriptionsLeft = $this->Cont['Even']['suscribe_nbr'] - $subscriptionsNumber;

            $this->set('subscriptionsLeft', $subscriptionsLeft);

            if ($subscriptionsLeft > 0) {

                $displayForm = true;
            }
        }

        /**
         * Liste des pages parentes
         */
        $pageIds = array();

        $_breadcrumb = array();

        if ($this->Cont['Even']['page_id']) {

            $Page   = $this->Page->findById($this->Cont['Even']['page_id']);

            $Path = $this->Page->getPath($Page['Page']['id']);

            foreach($Path as $v) $pageIds[] = $v['Page']['id'];

            $_breadcrumb['Model']   = 'Page';
            $_breadcrumb['Chemin']  = $Path;
        }

        /**
         * Listing des actualites associes
         * Tous les événements enfants de la page de niveau 0
         * Avec Paginate
         */
        $opts = array(
            'limit' => 4,
            'conditions' => array(
                'Actualite.etat_id'             => 'publish',
                'Actualite.id !='               => $id,
                'Actualite.page_id'             => $pageIds,
                'Actualite.type'                => 'actu',
            ),
            'order' => array(
                'Actualite.created' => 'ASC'
            )
        );

        $actualites = $this->Actualite->find('all', $opts);

        /**
         * Listing des evennements associes
         * Tous les événements enfants de la page de niveau 0
         * Avec Paginate
         */
        $opts = array(
            'limit' => 4,
            'conditions' => array(
                'Even.etat_id'             => 'publish',
                'Even.id !='               => $id,
                'Even.page_id'             => $pageIds,
                'Even.type'                => 'even',
                'Even.even_date_deb >='    => date('Y-m-d')
            ),
            'order' => array(
                'Even.even_date_deb' => 'ASC'
            )
        );

        $evenements = $this->Even->find('all', $opts);

        $this->loadModel('ModuleManager.ContentModule');
        $opts = array();
        $opts['conditions']['ContentModule.model'] = 'Even';
        $opts['conditions']['ContentModule.model_id'] = $this->Cont['Even']['id'];
        $opts['order'] = 'ContentModule.position ASC';

        $ContentModules = $this->ContentModule->find('all', $opts);

        $this->set(compact('actualites', 'Page', '_breadcrumb', 'evenements', 'ContentModules', 'displayForm'));
    }

    public function subscribe()
    {
        if (!empty($this->request->data)) {

            $this->Session->delete('validationErrors');

            $data = $this->request->data;

            $this->loadModel('Blog.EvenInscrit');

            if ($this->EvenInscrit->save($data)) {

                $this->Session->setFlash('Vous avez été inscrit à cet évènement avec succès', 'success');
                $this->redirect($this->referer());
            }

            $this->Session->setFlash('Une erreur est survenue', 'error');
            $this->redirect($this->referer());
        }
    }
}