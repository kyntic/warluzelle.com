<section class="liste_actualites">

	<?php foreach($ListeActualites as $v): ?><!--
	 --><div class="actualite">
			<div>
				<a href="<?php echo $v['Actualite']['url'] ;?>">
					<div class="image">
						<?php if(!empty($v['Vignette']['id'])) echo $this->WhFile->show($v['Vignette']); ?>
					</div>
					<h3><?php echo $v['Actualite']['name']; ?></h3>
					<p class="resume"><?php echo $v['Actualite']['resume']; ?></p>
					<div class="plus">En savoir +</div>
				</a>
			</div>
		</div><!--
 --><?php endforeach; ?>

</section>