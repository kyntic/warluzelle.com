<?php

class I18nManager extends AppModel {

    public function initI18n($plugin = '', $model = '', $champs = array(), $data)
    {

        if (empty($data)) {

            return false;
        }

        $I18nModel = ClassRegistry::init('I18nModel');

        $loadModel = $model;
        if (!empty($plugin)) {

            $loadModel = $plugin . '.' . $model;
        }
        $Model = ClassRegistry::init($loadModel);

        $i18nDatas = array();
        $tmpI18nDatas = $I18nModel->find(
            'all',
            array(
                'conditions' => array(
                    'I18nModel.model'       =>  $model,
                    'I18nModel.foreign_key' =>  $data[$model]['id'],
                    'I18nModel.field'       =>  $champs
                )
            )
        );

        if (!empty($tmpI18nDatas)) {

            foreach ($tmpI18nDatas as $k => $v) {
                $i18nDatas[$v['I18nModel']['locale']][$v['I18nModel']['field']] = $v['I18nModel']['id'];
            }

        }

        $langues = Configure::read('Config.languages');

        foreach ($langues as $langue) {

            foreach ($champs as $champ) {

                if (!isset($i18nDatas[$langue][$champ])) {

                    $i18n = array();

                    $i18n['I18nModel']['id'] = null;
                    $i18n['I18nModel']['locale'] = $langue;
                    $i18n['I18nModel']['model'] = $model;
                    $i18n['I18nModel']['foreign_key'] = $data[$model]['id'];
                    $i18n['I18nModel']['field'] = $champ;
                    if (!isset($data[$model][$champ])) {

                        $data[$model][$champ] = '';
                    }
                    if (is_object($data[$model][$champ])) {

                        $data[$model][$champ] = json_encode($data[$model][$champ]);
                    }
                    $i18n['I18nModel']['content'] = $data[$model][$champ];

                    if (!$I18nModel->save($i18n)) {

                        return false;
                    }

                }

            }

        }

        return true;

    }

}
