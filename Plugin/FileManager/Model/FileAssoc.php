<?php
class FileAssoc extends AppModel {

    public $belongsTo = 
    array(
        'FileManager.File'
    );



    public function beforeSave () {


        //S il ne peut y en avoir qu'un : 
    	if(!empty($this->data['FileAssoc']['nbr_max']) && !empty($this->data['FileAssoc']['model']) &&  !empty($this->data['FileAssoc']['model_id']) && isset($this->data['FileAssoc']['group'])) {


    		if($this->data['FileAssoc']['nbr_max'] == 1) {

    			$this->deleteAll(array(
    				'FileAssoc.model' 		=> $this->data['FileAssoc']['model'], 
    				'FileAssoc.model_id'	=> $this->data['FileAssoc']['model_id'], 
    				'FileAssoc.group' 		=> $this->data['FileAssoc']['group']
    			));

    			$this->data['FileAssoc']['default'] = 1;

    		}

    	}

    }

    public function afterSave() {

        //Si on designe une image par defautl, il faut changer l'état des autres : 
        if(!empty($this->data['FileAssoc']['default'])) {

            $inf = $this->findById($this->id);

            $this->updateAll(
                array('FileAssoc.default' => 0), 
                array(
                    'FileAssoc.model' => $inf['FileAssoc']['model'], 
                    'FileAssoc.model_id' => $inf['FileAssoc']['model_id'], 
                    'FileAssoc.group' => $inf['FileAssoc']['group'], 
                    'FileAssoc.id !=' => $inf['FileAssoc']['id'] 
                )

            );

        }


    }

   
}

?>
