<?php 
$this->assign('titre', 'Edition d’un fichier');
echo $this->Smart->create('File', array('url' => '/admin/file_manager/files/edit', 'class' => 'smart-form'));
?>

<div class="modal-body">
    <fieldset>

	        <?=$this->Smart->input('File.name', __('Nom'));?>
	        <?=$this->Smart->textarea('File.description', 'Description');?>
	        <?=$this->Smart->input('File.lien', 'Lien');?>

    	</div>

        <?=$this->Smart->hidden('File.id');?>
    </fieldset>
</div>
<div class="modal-footer">
    <footer>

        <?php echo $this->Smart->submit();?>

    </footer>
</div>

<?=$this->Smart->end();?>

<script type="text/javascript">

<?php if(!empty($this->request->params['named']['el'])) echo 'el = "#'.$this->request->params['named']['el'].'"'; ?>

$('#FileAdminEditForm').on('submit', function (e) {

	var form = $(this); 

	$(form).showLoading();

	$.ajax({
	  type 		: 'POST',
	  url 		: $(this).attr('action'),
	  data 		: $(this).serialize(),
	  cache 	: false, 
	  complete	: function () {$(form).hideLoading()}, 
	  success 	: function (reponse) {

	  	var reponse = jQuery.parseJSON(reponse);

	  	$('#myModalLarge').modal('hide');

	  	smartAlert(reponse.txt, 'success');

	  	if(el) window.parent.$(el).WhUploadFinished();

	  }

	});

	return false;

});


</script>
