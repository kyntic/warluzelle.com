<?php 
$this->assign('titre', 'Gestion des médias');
$this->Html->addCrumb(__('Gestion des'), $this->here);
$this->start('zone_titre_droite');
?>
<ul id="sparks" class="">
    <li class="sparks-info">
        <h5> Nombre d'items <span class="txt-color-blue"><?=$this->Paginator->counter('{:count}');?></span></h5>
    </li>
</ul>
<?php 
$this->end();
$btns[] = $this->Html->link('<i class="fa fa-plus-square"></i> Créer', '/admin/file_manager/files/edit/', array('data-toggle' => 'modal', 'data-target' => '#myModal', 'escape' => false, 'class' => 'btn btn-success btn-xs'));
$btns[] = $this->Html->link('<i class="fa fa-plus-square"></i> Déposer', '/admin/file_manager/files/edit/', array('data-toggle' => 'modal', 'data-target' => '#myModal', 'escape' => false, 'class' => 'btn btn-success btn-xs'));

echo $this->Smart->openBox(
    array(
        'icone' => 'fa-list', 
        'titre' => 'Liste des produits', 
        'btns'  => $btns
    )
);
?>
<div class="widget-body no-padding">

	<div class="alert alert-warning no-margin">
		<iframe src="/admin/file_manager/files/upload/<?php echo $param_url;?>" frameborder="0"></iframe>
	</div>

    <table class="table table-normal table-striped">
		<thead>
			<tr>
				<th>Admin</th>
				<th></th>
				<th>Nom</th>
				<th>Type</th>
				<th>Dossier</th>

			</tr>
		</thead>
		<tbody>
			<?php foreach($Files as $v) : ;?>
			<tr>
				<td style="width:150px;">
				<?php 
				echo $this->Html->link('<i class="fa fa-edit"></i>', '/admin/file_manager/files/edit_media/'.$v['File']['id'], array('escape' => false, 'class' => 'btn btn-primary btn-xs')).' ';
				echo $this->Html->link('<i class="fa fa-trash-o"></i>', '/admin/file_manager/files/delete/'.$v['File']['id'], array('escape' => false, 'class' => 'btn btn-danger btn-xs'), 'Etes vous sûre de vouloir supprimer ce fichier ? ').' ';
				?>
				</td>
				<td><?php echo $this->WhFile->show($v['File'], array('x' => 80, 'y' => 80));?></td>
				<td><?php echo $v['File']['name'];?></td>
				<td><?php echo $v['Extention']['type'];?></td>
				<td>Dossier</td>
			</tr>
			<?php endforeach;?>
		</tbody>
    </table>

    <div class="dt-toolbar-footer">
        <div class="col-xs-6">
            <div class="dataTables_info">
                <?=$this->Paginator->counter('Page <span class="txt-color-darken">{:page}</span> sur <span class="txt-color-darken">{:pages}</span> (total <span class="text-primary">{:count}</span>)');?>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="dataTables_paginate paging_simple_numbers">
                <?=$this->Paginator->numbers(array('separator' => '', 'class' => 'pagination pagination-sm', 'currentClass' => 'paginate_active', 'first' => 1, 'last' => 1 )); ?>
            </div>
        </div>
    </div>

</div>

<?php
echo $this->Smart->closeBox();
?>
