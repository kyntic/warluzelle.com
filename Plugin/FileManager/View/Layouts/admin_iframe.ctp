<!DOCTYPE html>
<html lang="fr" style="background:none;">
	<head>

		<?php 
		echo $this->Html->charset();
		//Title
		echo '<title>'.Configure::read('Projet.prefixe_title').$title_for_layout.'</title>'; 
		//Meta
		echo $this->Html->meta('keywords', $this->fetch('meta_keywords'));
		echo $this->Html->meta('description', $this->fetch('meta_description'));
		echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0'));
		echo $this->Html->meta(array('name' => 'author', 'content' => ''));
		echo $this->Html->meta(array('name' => 'robots', 'content' => $this->fetch('meta_robots')));
		//Css
		echo $this->Html->css('/smart/asset/css/bootstrap.min.css'); 
		echo $this->Html->css('/smart/asset/css/font-awesome.min.css'); 
		echo $this->Html->css('/smart/asset/css/smartadmin-production.css'); 
		echo $this->Html->css('/smart/asset/css/smartadmin-skins.css'); 
		echo $this->Html->css('/bootstrap/js/bootstrap-tagsinput/bootstrap-tagsinput.css');
		echo $this->Html->css('/smart/asset/css/your_style.css'); 
		echo $this->Html->css('/adminth/css/layout.css'); 
		echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700'); 
		echo $this->fetch('css');
		?>	
	</head>

	<body style="overflow:auto;background:none;">

<!-- Navbar
    ================================================== -->
    <div class="container-iframe">
    
	<?php 
	echo $this->Session->flash('Auth'); 
	echo $this->Session->flash(); 
	echo $this->fetch('content');
	?>

	</div>
    <?php 
    echo $this->Html->script('jquery'); 
    echo $this->Html->script('/bootstrap/js/bootstrap.min.js');
	echo $this->fetch('scriptBottom');
    echo $this->fetch('script'); //Ce sont les feuilles javascripts
    echo $this->Js->writeBuffer(); // Le code jascript en cache
    //if(Configure::read('debug')) echo '<div class="container">'.$this->element('sql_dump').'</div>';
    ?>

</body>

</html>