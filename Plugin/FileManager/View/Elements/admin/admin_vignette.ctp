<?php 
$param_url = array(); foreach($params as $k => $v) $param_url[] = $k.':'.$v; $param_url = implode('/', $param_url); 
$unique = uniqid();

$titre = (isset($params['titre'])) ? $params['titre'] : 'Vignette';

echo $this->Smart->openBox(array(
	'id' 	=> 'vignette_'.$unique, 
	'icone' => 'fa-image', 
	'titre' => $titre, 
	'btns'  => array(
		$this->Html->link('<i class="fa fa-plus-square"></i>', '#', array('class' => 'btn btn-success btn-xs btn-upload', 'escape' => false))
	)
));
echo '<div class="widget-body no-padding WhContent"></div>';
echo $this->Smart->closeBox();

$this->Html->script('/file_manager/js/admin.WhUpload.js', array('block' => 'scriptBottom'));
$this->Html->scriptStart(array('inline' => false));
?>
$('#vignette_<?php echo $unique;?>').WhUpload({
	btn_upload 		: $('#vignette_<?php echo $unique;?> .btn-upload'), 
	params_url		: '<?php echo $param_url;?>', 
	container		: $('#vignette_<?php echo $unique;?> .WhContent'), 
	container_url 	: '/admin/file_manager/files/vignette/<?php echo $param_url;?>'
});
<?php $this->Html->scriptEnd(); ?>