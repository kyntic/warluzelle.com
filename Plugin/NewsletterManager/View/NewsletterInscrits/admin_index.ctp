<?php
$this->assign('titre', '<i class="fa fa-envelope"></i> Gérer les inscrits');
$this->Html->addCrumb(__('Liste des inscrits'), '/admin/newsletter_manager/newsletter_inscrits');


echo $this->Smart->openBox(
    array(
        'icone' => 'fa-list',
        'btns'  => array(
            $this->Html->link('Exporter les emails (CSV)', '/admin/newsletter_manager/newsletter_inscrits/export', array('escape' => false, 'class' => 'btn btn-success btn-xs'))
        )
    )
);
?>
<div class="widget-body no-padding">
    <table class="table table-normal table-striped">
            <thead>
            <tr>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($inscrits as $inscrit) : ;?>
                <tr>
                    <td><?php echo $inscrit['NewsletterInscrit']['email']; ?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
    </table>

    <?=$this->element('admin_pagination');?>


</div>
<?php $this->Smart->closeBox();?>
