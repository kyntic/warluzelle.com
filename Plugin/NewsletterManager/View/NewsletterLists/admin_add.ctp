<?php $this->assign('titre', __('Création de liste')); ?>
<?=$this->WhForm->create('NewsletterList', array('url' => '/admin/newsletter_manager/newsletter_lists/add', 'class' => 'fill-up'));?>
    <div class="padded">
        <?=$this->WhForm->input('NewsletterList.name', 'Nom de la liste');?>
    </div>
    <div class="modal-footer">
        <button class="btn btn-blue" type="submit">Enregistrer</button>
    </div>
<?=$this->WhForm->end();?>