<?php
$this->assign('titre', '<i class="fa fa-list"></i> Listes');

$this->Html->addCrumb(__('Gestion des Listes'), $this->here);


echo $this->Smart->openBox(
    array(
        'icone' => 'fa-list',
        'btns'  => array(
            $this->Html->link('<i class="fa fa-plus-square""></i> Créer', array('action' => 'add'), array('escape' => false, 'class' => 'btn btn-success btn-xs'))
        )
    )
);
?>
<div class="widget-body no-padding">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Admin</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($lists as $list): ?>
            <tr>
                <td><?=$list['NewsletterList']['id'];?></td>
                <td><?=$list['NewsletterList']['name'];?></td>
                <td>
                    <?php
                    echo ' ' . $this->Html->link('<i class="fa fa-edit "></i>', '/admin/newsletter_manager/newsletter_lists/edit/' . $list['NewsletterList']['id'], array('escape' => false, 'class' => 'btn btn-primary btn-xs'));
                    echo ' ' . $this->Html->link('<i class="fa fa-trash-o "></i>', '/admin/newsletter_manager/newsletter_lists/delete/'. $list['NewsletterList']['id'], array('escape' => false, 'class' => 'btn btn-danger btn-xs'), 'Etes vous sûr de vouloir supprimer cette liste ?');
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php $this->Smart->closeBox();?>
