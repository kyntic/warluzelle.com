<?php
$this->assign('titre', '<i class="icon-list"></i> Édition d\'une campagne');
$this->Html->addCrumb(__('Gestion des campagnes'), '/admin/newsletter_manager/newsletter_campaigns/');
?>
<div class="row">
    <article class="col-sm-12 col-md-12 col-lg-6">

        <?php
        echo $this->Smart->openBox(
            array(
                'icone' => 'fa-edit',
                'titre' => 'Informations'
            )
        );
        ?>
        <div class="widget-body no-padding">

            <?php echo $this->Smart->create('NewsletterCampaign', array('class' => 'smart-form'));?>
            <fieldset>

                <?=$this->Smart->input('NewsletterCampaign.name', 'Nom de la campagne');?>
                <?=$this->Smart->select('NewsletterCampaign.list_id', 'Choix de la liste d\'envoi', $lists, array('type' => 'textarea', 'class' => 'chzn-select'));?>
                <?=$this->Smart->select('NewsletterCampaign.template_id', 'Nom du template', $templates, array('type' => 'textarea', 'class' => 'chzn-select'));?>
                <?=$this->Smart->input('NewsletterCampaign.default_from_name', 'Nom de l\'envoyeur');?>
                <?=$this->Smart->input('NewsletterCampaign.default_from_email', 'Courriel de l\'envoyeur');?>
                <?=$this->Smart->input('NewsletterCampaign.default_from_subject', 'Sujet par défaut');?>
                <?=$this->Smart->input('NewsletterCampaign.how_on_list', 'Rappel aux destinataires comment ils sont apparus dans la liste');?>
                <?=$this->Smart->input('NewsletterCampaign.company', 'Nom de l\'entreprise');?>

            </fieldset>
            <footer>

                <?php echo $this->Smart->submit();?>

            </footer>
            <?php
            echo $this->Smart->end();
            ?>
        </div>
        <?php echo $this->Smart->closeBox();?>

    </article>
</div>
