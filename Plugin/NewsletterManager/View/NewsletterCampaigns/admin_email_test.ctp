<?php $this->assign('titre', __('Mail de test')); ?>
<?=$this->WhForm->create('NewsletterCampaign', array('url' => '/admin/newsletter_manager/newsletter_campaigns/email_test', 'class' => 'fill-up'));?>
    <div class="padded">
        <?=$this->WhForm->input('NewsletterCampaign.email', __('Email'));?>
        <?=$this->Form->input('NewsletterCampaign.id', array('type' => 'hidden', 'value' => $campaignId));?>
        <?=$this->Form->input('NewsletterCampaign.template_id', array('type' => 'hidden', 'value' => $templateId));?>
    </div>
    <div class="modal-footer">
        <button class="btn btn-blue" type="submit">Envoyer</button>
    </div>
<?=$this->WhForm->end();?>

<script type="text/javascript">

    $('#NewsletterCampaignAdminEmailTestForm').on('submit', function (e) {

        e.preventDefault();

        var form = $(this);

        $(form).showLoading();
        $('.alert', this).remove();
        $('.error_text', this).remove();

        $.ajax({
            type 		: 'POST',
            url 		: $(this).attr('action'),
            data 		: $(this).serialize(),
            cache 	: false,
            complete	: function () {$(form).hideLoading()},
            success 	: function (reponse) {

                var reponse = jQuery.parseJSON(reponse);

                if(reponse.status == 0) { //Erreur

                    var alert = '<div class="padded"><div class="alert alert-error" style="margin:0;" >Email non envoye !</div></div>';
                    form.prepend(alert);
                }

                if(reponse.status == 1) { //Ok
                    $('#popup_ajx').modal('hide');
                }
            }
        });

        return false;
    });
</script>