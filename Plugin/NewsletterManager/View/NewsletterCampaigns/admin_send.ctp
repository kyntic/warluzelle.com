<?php
$this->assign('titre', '<i class="icon-magnet"></i> Lancer la campagne');
$this->Html->addCrumb(__('Gestion des campagnes'), '/admin/newsletter_manager/newsletter_campaigns');
?>
<?=$this->WhForm->create('NewsLetterCampaignSend', array('url' => '/admin/newsletter_manager/newsletter_campaigns/launch_campaign/' . $this->data['NewsletterCampaign']['id'] . '/true', 'class' => 'fill-up'));?>
<div class="box">
    <div class="box-header">
        <span class="title">Résumé de la campagne</span>
    </div>
    <div class="box-content">
        <div class="padded">
            <?=$this->WhForm->input('NewsletterCampaign.name', 'Nom de la liste', array('disabled'));?>
            <?=$this->WhForm->input('NewsletterTemplate.name', 'Template', array('disabled'));?>
            <?=$this->WhForm->input('NewsletterList.name', 'Liste', array('disabled'));?>
            <?=$this->WhForm->input('emailCount', 'Nombre d\'emails dans la liste', array('disabled', 'value' => $emailCount));?>
            <?=$this->WhForm->input('NewsletterCampaign.default_from_name', 'Email envoyé par', array('disabled'));?>
            <?=$this->WhForm->input('NewsletterCampaign.default_from_email', 'Email de l\'envoyeur', array('disabled'));?>
            <?=$this->WhForm->input('NewsletterCampaign.default_from_subject', 'Sujet', array('disabled'));?>
            <?=$this->WhForm->input('NewsletterCampaign.how_on_list', 'Rappel aux destinataires comment ils sont apparus dans la liste', array('disabled'));?>
            <?=$this->WhForm->input('NewsletterCampaign.company', 'Compagnie', array('disabled'));?>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-blue" type="submit">Lancer Campagne</button>
        <?=$this->Html->link('E-mail de test', '/admin/newsletter_manager/newsletter_campaigns/email_test/' . $this->data['NewsletterCampaign']['id'] . '/' . $this->data['NewsletterTemplate']['id'], array('escape' => false, 'class' => 'btn btn-green', 'data-toggle' => 'modal', 'data-target' => '#popup_ajx')).' ';?>
    </div>
</div>
<?=$this->WhForm->end();?>
<div class="box">
    <div class="box-header">
        <span class="title">Votre template</span>
    </div>
    <div class="box-content">
        <!-- Notre Iframe -->
        <iframe width="100%" height="500px" src="<?=$iframeUrl;?>" id="iframe"></iframe>
    </div>
</div>
