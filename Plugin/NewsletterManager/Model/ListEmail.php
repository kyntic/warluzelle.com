<?php

/**
 * Class NewsletterListEmail
 */
class ListEmail extends AppModel
{

    /**
     * Links between tables
     *
     * @var array
     */
    public $belongsTo = array(
        'NewsletterEmail' => array(
            'className'     => 'NewsletterManager.NewsletterEmail',
            'foreignKey'    => 'email_id'
        )
    );

    /**
     * Prefix for the table name
     *
     * @var string
     */
    public $tablePrefix = 'newsletter_';
}