<?=$this->Form->create('Recherche', array('id' => 'RechercheForm', 'url' => '/recherche/'));?>
<?=$this->Form->input('champ', array('div' => false, 'label' => false, 'placeholder' => 'Indiquez une référence ou un mot clé'));?>
<?=$this->WhForm->submit('<i class="fa fa-search"></i>');?>
<?=$this->Form->end();?>
