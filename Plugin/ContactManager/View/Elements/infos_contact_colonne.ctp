<section class="infos_contact">
	<h2>Coordonnées</h2>
	<p class="adresse" ><?php echo $ContactSite['Contact']['adresse_1'] ?></p>
	<p class="adresse" ><?php echo $ContactSite['Contact']['adresse_2'] ?></p>
	<p class="adresse" ><?php echo $ContactSite['Contact']['cp'].' '.$ContactSite['Contact']['ville']; ?></p>
	<p class="tel" >Tél : <?php echo $ContactSite['Contact']['telephone'] ?></p>
	<p class="email" ><?php echo $this->Html->link('Contactez-nous', $ContactSite['Contact']['url']); ?></p>
</section>