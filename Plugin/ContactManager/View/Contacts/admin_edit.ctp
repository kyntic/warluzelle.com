<?php 
$titre = (!empty($this->data)) ? $this->data['Contact']['name'] : 'Editer un contact';
$this->assign('titre', '<i class="fa fa-edit"></i> '.$titre);
$this->Html->addCrumb(__('Gestion du menu'), '/admin/menus');
$this->Html->addCrumb(__('Edition d’un contact'), $this->here);

echo $this->Smart->create('Form', array());
?>
<div class="row">
	<article class="col-sm-12 col-md-9 col-lg-9">

		<?php 
		$tab = '<ul class="nav nav-tabs pull-left in">';
		$tab .= '<li class="active"><a href="#tab-txt" data-toggle="tab"><i class="fa fa-font"></i> Textes</a></li>';
		if(!empty($this->data['Contact']['id'])) $tab .= '<li><a href="#tab-portfolio" data-toggle="tab"><i class="fa fa-picture-o"></i>  Portfolio</a></li>';
		$tab .= '<li><a href="#tab-referencement" data-toggle="tab"><i class="fa fa-thumbs-o-up"></i>  Référencement</a></li>';
		$tab .= '<li><a href="#tab-techniques" data-toggle="tab"><i class="fa fa-cog"></i>  Données techniques</a></li>';
		$tab .= '</ul>';
		
		$btns = (!empty($this->data['Contact']['url'])) ? $this->Html->link('Voir', $this->data['Contact']['url'], array('class' => 'btn btn-success btn-xs', 'target' => 'blank')) : '';

		echo $this->Smart->openBox(array('tab' => $tab, 'btns' => array($btns)));
		?>
		<div class="widget-body no-padding">

			<div class="tab-content">
				<div class="tab-pane active smart-form" id="tab-txt">


					<header>Titres</header>
					<fieldset>
						<div class="row">
						<?php
						echo $this->Smart->input('Contact.h1', 'Titre de la contact', array('class' => 'titre_contact', 'col' => 8, 'help' => 'Titre utilisé dans la contact'));
						echo $this->Smart->input('Contact.name', 'Titre dans le menu', array('class' => 'titre_menu', 'col' => 4, 'help' => 'Titre utilisé dans le menu'));
						?>
						</div>

						<?php echo $this->Smart->select('MenuItem.parent_id', 'Page parente : ' , $MenuItems, array('col' => 8, 'empty' => 'Racine', 'class' => 'select2'));?>

					</fieldset>


					<header>Informations sur le contact</header>
					<fieldset>
						<div class="row">
						<?php 
						echo $this->Smart->input('Contact.adresse_1', 'Adresse', array('col' => 6));
						echo $this->Smart->input('Contact.adresse_2', 'Complément d’adresse', array('col' => 6));
						?>
						</div>
						<div class="row">
						<?php 
						echo $this->Smart->input('Contact.cp', 'Code postal', array('col' => 4));
						echo $this->Smart->input('Contact.ville', 'Ville', array('col' => 8));
						?>
						</div>

						<div class="row">
						<?php 
						echo $this->Smart->input('Contact.telephone', 'Téléphone', array('col' => 6, 'append' => 'fa-phone'));
						echo $this->Smart->input('Contact.email', 'Email', array('col' => 6, 'append' => 'fa-envelope'));
						?>
						</div>
					</fieldset>




					<header>Textes & contenus</header>
					<fieldset>
					<?php
						echo $this->Smart->textarea('Contact.resume', 'Résumé : ');
						echo $this->Smart->textarea('Contact.conseil', 'Besoin de conseil : ');
						echo $this->Smart->textarea('Contact.body', 'Contenu : ', array('style' => 'min-height:450px;', 'class' => 'tinyMce'));
					?>
					</fieldset>


				</div>

				<div class="tab-pane smart-form" id="tab-portfolio">

					<?php
					if(!empty($this->data['Contact']['id']))  {
						echo $this->element('FileManager.admin/admin_galerie', array('params' => array(
							'content_type_id' 	=> strtolower('Contact'), //Obligatoire, sinon l'association ne se fera pas
							'id'				=> $this->data['Contact']['id'], 
							'group'				=> 'portfolio',
							'default'			=> 1,
							'titre'				=> 'Portflio'
						)));
					}
					?>

				</div>

				<div class="tab-pane smart-form" id="tab-referencement">
					<?php echo $this->element('/admin/cms/seo', array('Modele' => 'Contact'));?>
				</div>

				<div class="tab-pane smart-form" id="tab-techniques">
					<header>Données techniques</header>
					<fieldset>
					<?php 
					echo $this->Smart->input('Contact.url_r', 'Url de redirection');
					echo $this->Smart->input('Contact.template', 'Template');
					?>
					</fieldset>
				</div>



			</div>
		
			<div class="smart-form">
				<footer>

					<?php echo $this->Smart->submit('Enregistrer');?>

				</footer>
			</div>

		</div>

		<?php echo $this->Smart->closeBox();?>

	</article>
	<article class="col-sm-12 col-md-3 col-lg-3">
		
		<?php echo $this->Smart->openBox(array('icone' => 'fa-info', 'titre' => 'Enregistrer'));?>

		<div class="widget-body no-padding smart-form">
			<table class="table table-normal table-striped table-bordered th-right">
				<tr>
					<th>Affiche dans le menu</th>
					<td><?php echo $this->Smart->checkOn('Contact.menu_id');?></td>
				</tr>
				<tr>
					<th>Publier</th>
					<td><?php echo $this->Smart->checkOn('Contact.etat_id');?></td>
				</tr>
				<tr>
					<th>Type</th>
					<td>
					<?php 
					echo $this->Smart->input('Contact.type', false, array(
						'options' => array(
							'default' => 'Contact par default', 
							'individue' => 'Personne physique',  
							'bureau' => 'Bureau', 
							'partenaire' => 'Partenaire'
						), 
						'label' => false 
					));
					?></td>				
				</tr>
				<?php if(!empty($this->data)) {?>
				<tr>
					<th>Date modification</th>
					<td><?php echo $this->WhDate->show($this->data['Contact']['updated']);?></td>
				</tr>	
				<?php }?>			
			</table>
		    <footer>
		    	<?php echo $this->Form->button(__('Enregistrer'). ' <i class="fa fa-plus-square"></i>', array('type' => 'submit', 'class' => 'btn btn-success btn-xs', 'name' => 'data[Contact][nouveau]', 'value' => true));?>
		      	<?php echo $this->Form->button(__('Enregistrer'), array('type' => 'submit', 'class' => 'btn btn-primary btn-xs'));?>
		    </footer>

		</div>

		<?php echo $this->Smart->closeBox();?>

		<?php
		if(!empty($this->data['Contact']['id'])) {
		echo $this->element('FileManager.admin/admin_vignette', array('params' => array(
			'content_type_id' 	=> strtolower('Contact'), //Obligatoire, sinon l'association ne se fera pas
			'id'				=> $this->data['Contact']['id'], 
			'nbr_max' 			=> 1, 
			'group'				=> 'vignette',
			'default'			=> 1,
			'titre'				=> 'Vignette'
		)));
		}
		?>

	</article>
</div>
<?php 
echo $this->Smart->hidden('Contact.id');
echo $this->Smart->hidden('MenuItem.id');
echo $this->Smart->end();
echo $this->element('Tinymce.toolbarre', array('Textarea' => '.tinyMce', 'css_files' => FULL_BASE_URL.'/css/less/style.css,'.FULL_BASE_URL.'/bootstrap/css/bootstrap.css'));
$this->Html->scriptStart(array('inline' => false));
?>
$('.titre_contact').blur(function(){
	
	if($('.titre_menu').val() == '') $('.titre_menu').val($('.titre_contact').val());

});

<?php
$this->Html->scriptEnd();
?>