-- MySQL dump 10.13  Distrib 5.6.42, for Linux (x86_64)
--
-- Host: localhost    Database: warluzelle-com
-- ------------------------------------------------------
-- Server version	5.6.42-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acos`
--

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;
/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actualites`
--

DROP TABLE IF EXISTS `actualites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actualites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_type_id` varchar(128) NOT NULL,
  `etat_id` varchar(128) NOT NULL,
  `page_id` int(11) NOT NULL,
  `acces_membre` tinyint(1) DEFAULT '0',
  `type` varchar(128) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `h1` varchar(256) NOT NULL,
  `sous_titre` varchar(256) DEFAULT NULL,
  `vignette` text NOT NULL,
  `url` varchar(256) NOT NULL,
  `url_r` varchar(256) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `publication_date` int(11) DEFAULT NULL,
  `publication_date_fin` int(11) DEFAULT NULL,
  `agenda_id` int(11) DEFAULT NULL,
  `even_date_deb` datetime NOT NULL,
  `even_date_fin` datetime NOT NULL,
  `all_day` tinyint(1) NOT NULL DEFAULT '0',
  `suscribe` tinyint(1) NOT NULL DEFAULT '0',
  `suscribe_nbr` int(11) NOT NULL DEFAULT '0',
  `color` varchar(128) DEFAULT NULL,
  `adresse` text NOT NULL,
  `recurence` varchar(128) NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `push_accueil` tinyint(1) NOT NULL,
  `auteur_id` int(11) DEFAULT NULL,
  `from` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actualites`
--

LOCK TABLES `actualites` WRITE;
/*!40000 ALTER TABLE `actualites` DISABLE KEYS */;
/*!40000 ALTER TABLE `actualites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adresses`
--

DROP TABLE IF EXISTS `adresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `civilite_id` int(11) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `prenom` varchar(128) NOT NULL,
  `nom_entreprise` varchar(256) NOT NULL,
  `adresse_1` varchar(256) NOT NULL,
  `adresse_2` varchar(256) NOT NULL,
  `cp` varchar(16) NOT NULL,
  `ville` varchar(256) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adresses`
--

LOCK TABLES `adresses` WRITE;
/*!40000 ALTER TABLE `adresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `adresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agendas`
--

DROP TABLE IF EXISTS `agendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agendas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `page_id` int(11) NOT NULL,
  `last_even_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agendas`
--

LOCK TABLES `agendas` WRITE;
/*!40000 ALTER TABLE `agendas` DISABLE KEYS */;
/*!40000 ALTER TABLE `agendas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros`
--

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;
/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros_acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) unsigned NOT NULL,
  `aco_id` int(10) unsigned NOT NULL,
  `_create` char(2) NOT NULL DEFAULT '0',
  `_read` char(2) NOT NULL DEFAULT '0',
  `_update` char(2) NOT NULL DEFAULT '0',
  `_delete` char(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros_acos`
--

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;
/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_families`
--

DROP TABLE IF EXISTS `attribute_families`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_families` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `name_admin` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_families`
--

LOCK TABLES `attribute_families` WRITE;
/*!40000 ALTER TABLE `attribute_families` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_families` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_family_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributes`
--

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_assocs`
--

DROP TABLE IF EXISTS `content_assocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_assocs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(128) DEFAULT NULL,
  `model_id` int(11) NOT NULL,
  `model_asso` varchar(128) DEFAULT NULL,
  `model_asso_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_assocs`
--

LOCK TABLES `content_assocs` WRITE;
/*!40000 ALTER TABLE `content_assocs` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_assocs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_modules`
--

DROP TABLE IF EXISTS `content_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(128) NOT NULL,
  `model_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `module` varchar(128) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_modules`
--

LOCK TABLES `content_modules` WRITE;
/*!40000 ALTER TABLE `content_modules` DISABLE KEYS */;
INSERT INTO `content_modules` VALUES (117,'Page',6,0,'txtimg','{\"title\":\"\",\"txt\":\"<p>En plus des<strong> chambres fun\\u00e9raires<\\/strong>, les pompes fun\\u00e8bres Warluzelle mettent \\u00e0 votre disposition une <strong>salle de convivialit\\u00e9<\\/strong>, r\\u00e9serv\\u00e9e aux proches du d\\u00e9funt.<\\/p><p>Les <strong>familles <\\/strong>et <strong>amis <\\/strong>peuvent ainsi \\u00e9changer et discuter au calme tout en se restaurant.<\\/p><p>Vous trouverez dans notre <strong>salle de convivialit\\u00e9 <\\/strong>une petite kitchenette, une grande table et des chaises.<\\/p><p>Cet espace a \\u00e9t\\u00e9 pens\\u00e9 pour \\u00eatre accueillant et pratique, vous pourrez m\\u00eame avoir acc\\u00e8s \\u00e0 l\'ext\\u00e9rieur pour souffler un peu.<\\/p><p>C\'est un endroit id\\u00e9al pour se rem\\u00e9morer les moments pass\\u00e9s en compagnie du <strong>d\\u00e9funt<\\/strong>.<\\/p>\",\"img\":{\"File\":{\"id\":\"7\",\"name\":\"chambre-funeraire5\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"chambre_funeraire556950466da95a\",\"nom_fichier\":\"chambre_funeraire556950466da95a.jpg\",\"poids\":\"172\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/01\\/chambre_funeraire556950466da95a.jpg\",\"created\":\"2016-01-12 14:49:26\",\"updated\":\"2016-01-12 14:49:26\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":1}'),(118,'Page',6,1,'txt','{\"title\":\"\",\"txt\":\"<p><strong>Les Etablissements Warluzelle<\\/strong>, entreprise situ\\u00e9e \\u00e0 <strong>Amiens <\\/strong>dans <strong>la Somme<\\/strong> (<strong>80<\\/strong>), tient \\u00e0 vous offrir des prestations compl\\u00e8tes pour vous accompagner au mieux dans votre <strong>deuil<\\/strong>.<\\/p><p>Pour en conna\\u00eetre d\'avantage sur les services disponibles, n\'h\\u00e9sitez pas \\u00e0 nous <a href=\\\"http:\\/\\/www.warluzelle.com\\/core\\/admin\\/page8\\/contact\\\" _cke_saved_href=\\\".\\/page8\\/contact\\\"><strong>contacter par t\\u00e9l\\u00e9phone<\\/strong><\\/a> ou en remplissant le <strong><a href=\\\"http:\\/\\/www.warluzelle.com\\/core\\/admin\\/page8\\/contact\\\" _cke_saved_href=\\\".\\/page8\\/contact\\\">formulaire de contact.<\\/a><\\/strong><\\/p><p>D\\u00e9couvrez l\'ensemble de nos prestations :<\\/p><ul><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page2\\/chambre-funeraire\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page2\\/chambre-funeraire\\\"><strong>Chambres fun\\u00e9raires<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page3\\/pompes-funebres\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page3\\/pompes-funebres\\\"><strong>Pompes fun\\u00e8bres<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page4\\/fleurs\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page4\\/fleurs\\\"><strong>Fleurs<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page5\\/marbrerie\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page5\\/marbrerie\\\"><strong>Marbrerie<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page6\\/prevoyance\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page6\\/prevoyance\\\"><strong>Pr\\u00e9voyance<\\/strong><\\/a><\\/li><\\/ul>\",\"position\":2}'),(119,'Page',6,2,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"6\",\"name\":\"chambre-funeraire4\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"chambre_funeraire456950458b89eb\",\"nom_fichier\":\"chambre_funeraire456950458b89eb.jpg\",\"poids\":\"160\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"2\",\"url\":\"\\/files\\/2016\\/01\\/chambre_funeraire456950458b89eb.jpg\",\"created\":\"2016-01-12 14:49:12\",\"updated\":\"2016-01-12 14:49:12\"},\"Folder\":{\"id\":\"2\",\"name\":\"diapo salle de convivialit\\u00e9\",\"parent_id\":null,\"lft\":\"3\",\"rght\":\"4\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"4\",\"name\":\"chambre-funeraire5\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"chambre_funeraire556950457364b5\",\"nom_fichier\":\"chambre_funeraire556950457364b5.jpg\",\"poids\":\"172\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"2\",\"url\":\"\\/files\\/2016\\/01\\/chambre_funeraire556950457364b5.jpg\",\"created\":\"2016-01-12 14:49:11\",\"updated\":\"2016-01-12 14:49:11\"},\"Folder\":{\"id\":\"2\",\"name\":\"diapo salle de convivialit\\u00e9\",\"parent_id\":null,\"lft\":\"3\",\"rght\":\"4\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"5\",\"name\":\"chambre-funeraire3\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"chambre_funeraire3569504580bc6e\",\"nom_fichier\":\"chambre_funeraire3569504580bc6e.jpg\",\"poids\":\"179\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"2\",\"url\":\"\\/files\\/2016\\/01\\/chambre_funeraire3569504580bc6e.jpg\",\"created\":\"2016-01-12 14:49:12\",\"updated\":\"2016-01-12 14:49:12\"},\"Folder\":{\"id\":\"2\",\"name\":\"diapo salle de convivialit\\u00e9\",\"parent_id\":null,\"lft\":\"3\",\"rght\":\"4\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":3}'),(123,'Page',5,0,'txtimg','{\"title\":\"\",\"txt\":\"<p>Les <strong>Etablissements Warluzelle<\\/strong> disposent de diff\\u00e9rentes <strong>chambres<\\/strong> <strong>fun\\u00e9raires<\\/strong>mis \\u00e0 votre disposition.<\\/p><p>Nos locaux sont situ\\u00e9s \\u00e0 <strong>Amiens<\\/strong>, dans le d\\u00e9partement de la <strong>Somme <\\/strong>(<strong>80<\\/strong>).<\\/p><p>Pour que le souvenir de la s\\u00e9paration avec votre proche soit att\\u00e9nu\\u00e9 avec le temps, et qu\'aucune pi\\u00e8ce de votre maison ne puisse vous le rappeler, Les <strong>Etablissements Warluzelle<\\/strong> mettent \\u00e0 votre disposition ses <strong>chambres fun\\u00e9raires<\\/strong>.<\\/p><p>Nous disposons de <strong>deux salons<\\/strong> accessibles aux familles <strong>24h\\/24<\\/strong> et <strong>7j\\/7<\\/strong><\\/p><p>Nos deux salons sont diff\\u00e9rents, de par leur cadre et leur d\\u00e9coration.<\\/p><p>Nous disposons \\u00e9galement d\'une salle de c\\u00e9r\\u00e9monie et d\'un parking pour ceux et celles qui viennent.<\\/p>\",\"img\":{\"File\":{\"id\":\"29\",\"name\":\"chambre-funeraire2\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"chambre_funeraire25695185310752\",\"nom_fichier\":\"chambre_funeraire25695185310752.jpg\",\"poids\":\"169\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/01\\/chambre_funeraire25695185310752.jpg\",\"created\":\"2016-01-12 16:14:27\",\"updated\":\"2016-01-12 16:14:27\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":1}'),(124,'Page',5,1,'txt','{\"title\":\"\",\"txt\":\"<p>L\'\\u00e9quipe de l\'entreprise <strong>Les Etablissements Warluzelle<\\/strong> situ\\u00e9s \\u00e0 Amiens dans <strong>la Somme<\\/strong> (<strong>80<\\/strong>) r\\u00e9alise des devis gratuits et personnalis\\u00e9s pour toujours mieux vous servir.<br>Pour en conna\\u00eetre d\'avantage sur les services disponibles, n\'h\\u00e9sitez pas \\u00e0 nous <a href=\\\"http:\\/\\/www.warluzelle.com\\/core\\/admin\\/page8\\/contact\\\" _cke_saved_href=\\\".\\/page8\\/contact\\\"><strong>contacter par t\\u00e9l\\u00e9phone<\\/strong><\\/a> ou en remplissant le <a href=\\\"http:\\/\\/www.warluzelle.com\\/core\\/admin\\/page8\\/contact\\\" _cke_saved_href=\\\".\\/page8\\/contact\\\"><strong>formulaire de contact<\\/strong>.<\\/a><br>Partez \\u00e0 la d\\u00e9couverte de l\'ensemble de nos services :<\\/p><ul><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page3\\/pompes-funebres\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page3\\/pompes-funebres\\\"><strong>Pompes fun\\u00e8bres<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page4\\/fleurs\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page4\\/fleurs\\\"><strong>Fleurs<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page5\\/marbrerie\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page5\\/marbrerie\\\"><strong>Marbrerie<\\/strong><\\/a><\\/li><li><strong><a href=\\\"http:\\/\\/www.warluzelle.com\\/page9\\/salle-de-convivialite\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page9\\/salle-de-convivialite\\\">Salle de convivialit\\u00e9<\\/a><\\/strong><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page6\\/prevoyance\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page6\\/prevoyance\\\"><strong>Pr\\u00e9voyance<\\/strong><\\/a><\\/li><\\/ul>\",\"position\":2}'),(125,'Page',5,2,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"27\",\"name\":\"chambre-funeraire3\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"chambre_funeraire3569517f0847b3\",\"nom_fichier\":\"chambre_funeraire3569517f0847b3.jpg\",\"poids\":\"179\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"6\",\"url\":\"\\/files\\/2016\\/01\\/chambre_funeraire3569517f0847b3.jpg\",\"created\":\"2016-01-12 16:12:48\",\"updated\":\"2016-01-12 16:12:48\"},\"Folder\":{\"id\":\"6\",\"name\":\"diapo chambre fun\\u00e9raire\",\"parent_id\":null,\"lft\":\"11\",\"rght\":\"12\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"28\",\"name\":\"chambre-funeraire4\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"chambre_funeraire45695180ab6193\",\"nom_fichier\":\"chambre_funeraire45695180ab6193.jpg\",\"poids\":\"160\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"6\",\"url\":\"\\/files\\/2016\\/01\\/chambre_funeraire45695180ab6193.jpg\",\"created\":\"2016-01-12 16:13:14\",\"updated\":\"2016-01-12 16:13:14\"},\"Folder\":{\"id\":\"6\",\"name\":\"diapo chambre fun\\u00e9raire\",\"parent_id\":null,\"lft\":\"11\",\"rght\":\"12\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"26\",\"name\":\"chambre-funeraire2\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"chambre_funeraire2569517ef390b4\",\"nom_fichier\":\"chambre_funeraire2569517ef390b4.jpg\",\"poids\":\"169\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"6\",\"url\":\"\\/files\\/2016\\/01\\/chambre_funeraire2569517ef390b4.jpg\",\"created\":\"2016-01-12 16:12:47\",\"updated\":\"2016-01-12 16:12:47\"},\"Folder\":{\"id\":\"6\",\"name\":\"diapo chambre fun\\u00e9raire\",\"parent_id\":null,\"lft\":\"11\",\"rght\":\"12\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"25\",\"name\":\"chambre-funeraire1\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"chambre_funeraire1569517ede7fc0\",\"nom_fichier\":\"chambre_funeraire1569517ede7fc0.jpg\",\"poids\":\"191\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"6\",\"url\":\"\\/files\\/2016\\/01\\/chambre_funeraire1569517ede7fc0.jpg\",\"created\":\"2016-01-12 16:12:45\",\"updated\":\"2016-01-12 16:12:45\"},\"Folder\":{\"id\":\"6\",\"name\":\"diapo chambre fun\\u00e9raire\",\"parent_id\":null,\"lft\":\"11\",\"rght\":\"12\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"24\",\"name\":\"chambre-funeraire7\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"chambre_funeraire7569517ec0dc19\",\"nom_fichier\":\"chambre_funeraire7569517ec0dc19.jpg\",\"poids\":\"140\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"6\",\"url\":\"\\/files\\/2016\\/01\\/chambre_funeraire7569517ec0dc19.jpg\",\"created\":\"2016-01-12 16:12:44\",\"updated\":\"2016-01-12 16:12:44\"},\"Folder\":{\"id\":\"6\",\"name\":\"diapo chambre fun\\u00e9raire\",\"parent_id\":null,\"lft\":\"11\",\"rght\":\"12\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"23\",\"name\":\"chambre-funeraire6\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"chambre_funeraire6569517d471ba2\",\"nom_fichier\":\"chambre_funeraire6569517d471ba2.jpg\",\"poids\":\"163\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"6\",\"url\":\"\\/files\\/2016\\/01\\/chambre_funeraire6569517d471ba2.jpg\",\"created\":\"2016-01-12 16:12:20\",\"updated\":\"2016-01-12 16:12:20\"},\"Folder\":{\"id\":\"6\",\"name\":\"diapo chambre fun\\u00e9raire\",\"parent_id\":null,\"lft\":\"11\",\"rght\":\"12\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"22\",\"name\":\"chambre-funeraire5\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"chambre_funeraire5569517b87d8dc\",\"nom_fichier\":\"chambre_funeraire5569517b87d8dc.jpg\",\"poids\":\"172\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"6\",\"url\":\"\\/files\\/2016\\/01\\/chambre_funeraire5569517b87d8dc.jpg\",\"created\":\"2016-01-12 16:11:52\",\"updated\":\"2016-01-12 16:11:52\"},\"Folder\":{\"id\":\"6\",\"name\":\"diapo chambre fun\\u00e9raire\",\"parent_id\":null,\"lft\":\"11\",\"rght\":\"12\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":3}'),(126,'Page',8,0,'txt','{\"title\":\"\",\"txt\":\"<iframe src=\\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m14!1m8!1m3!1d5142.302195630772!2d2.316382!3d49.877189!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcc3f1efd110753ac!2sPompes+Fun%C3%A8bres+Warluzelle!5e0!3m2!1sfr!2sfr!4v1452614353660\\\" width=\\\"100%\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen><\\/iframe>\",\"position\":3}'),(127,'Page',8,1,'txt','{\"title\":\"\",\"txt\":\"<p>D\\u00e9couvrez plus de d\\u00e9tails sur nos prestations:<\\/p><ul><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page2\\/chambre-funeraire\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page2\\/chambre-funeraire\\\"><strong>Chambres fun\\u00e9raires<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page3\\/pompes-funebres\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page3\\/pompes-funebres\\\"><strong>Pompes fun\\u00e8bres<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page4\\/fleurs\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page4\\/fleurs\\\"><strong>Fleurs<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page5\\/marbrerie\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page5\\/marbrerie\\\"><strong>Marbrerie<\\/strong><\\/a><\\/li><li><strong><a href=\\\"http:\\/\\/www.warluzelle.com\\/page9\\/salle-de-convivialite\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page9\\/salle-de-convivialite\\\">Salle de convivialit\\u00e9<\\/a><\\/strong><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page6\\/prevoyance\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page6\\/prevoyance\\\"><strong>Pr\\u00e9voyance<\\/strong><\\/a><\\/li><li><\\/li><\\/ul>\",\"position\":2}'),(129,'Page',2,0,'txtimg','{\"title\":\"\",\"txt\":\"<p>Notre but est de vous fournir un <strong>service de qualit\\u00e9<\\/strong> pour vous <strong>soulager<\\/strong> au maximum de toutes les \\u00e9tapes li\\u00e9es \\u00e0 ce moment si particulier de votre vie.<\\/p><p>Les <strong>Etablissements Warluzelle se chargent<\\/strong>:<\\/p><ul><li>Des <strong>formalit\\u00e9s administratives<\\/strong> relatives aux <strong>obs\\u00e8ques<\\/strong><\\/li><li>De la <strong>r\\u00e9alisation des faire-part<\\/strong> \\u00e0 destination des proches du d\\u00e9funt<\\/li><li>De la <strong>publication de l\'avis de d\\u00e9c\\u00e8s<\\/strong> dans les journaux de presse \\u00e9crite<\\/li><li>Des <strong>pr\\u00e9paratifs de la pr\\u00e9sentation<\\/strong> du d\\u00e9funt<\\/li><li>De <strong>l\'organisation de la cr\\u00e9mation<\\/strong> ou <strong>l\'inhumation<\\/strong><\\/li><li>De <strong>l\'organisation de la c\\u00e9r\\u00e9monie<\\/strong> qu\'elle soit religieuse ou civile<\\/li><li>Du <strong>choix<\\/strong> de la <strong>composition des fleurs<\\/strong><\\/li><li>Du <strong>transport avant\\/apr\\u00e8s mise en bi\\u00e8re<\\/strong> m\\u00eame en cas de transfert d\'une ville \\u00e0 une autre<\\/li><\\/ul><p>Nous nous mettons \\u00e0 votre service afin de vous offrir un <strong>service personnalis\\u00e9<\\/strong> et en <strong>toute discr\\u00e9tion<\\/strong>.<\\/p>\",\"img\":{\"File\":{\"id\":\"12\",\"name\":\"pompes-funebres1\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"pompes_funebres1569504cc84392\",\"nom_fichier\":\"pompes_funebres1569504cc84392.jpg\",\"poids\":\"271\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/01\\/pompes_funebres1569504cc84392.jpg\",\"created\":\"2016-01-12 14:51:08\",\"updated\":\"2016-01-12 14:51:08\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":4}'),(130,'Page',2,1,'txt','{\"title\":\"\",\"txt\":\"<p><strong>Les Etablissements Warluzelle<\\/strong>, entreprise situ\\u00e9e \\u00e0 Amiens dans <strong>la Somme<\\/strong> (<strong>80<\\/strong>) r\\u00e9alise des devis gratuits et personnalis\\u00e9s pour toujours mieux vous servir.<br>Pour en conna\\u00eetre d\'avantage sur les services disponibles, n\'h\\u00e9sitez pas \\u00e0 nous <a href=\\\"http:\\/\\/www.warluzelle.com\\/core\\/admin\\/page8\\/contact\\\" _cke_saved_href=\\\".\\/page8\\/contact\\\"><strong>contacter par t\\u00e9l\\u00e9phone<\\/strong><\\/a> ou en remplissant le<a href=\\\"http:\\/\\/www.warluzelle.com\\/core\\/admin\\/page8\\/contact\\\" _cke_saved_href=\\\".\\/page8\\/contact\\\"> <strong>formulaire de contact<\\/strong>.<\\/a><br>D\\u00e9couvrez l\'ensemble de nos services :<\\/p><ul><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page2\\/chambre-funeraire\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page2\\/chambre-funeraire\\\"><strong>Chambres fun\\u00e9raires<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page4\\/fleurs\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page4\\/fleurs\\\"><strong>Fleurs<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page5\\/marbrerie\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page5\\/marbrerie\\\"><strong>Marbrerie<\\/strong><\\/a><\\/li><li><strong><a href=\\\"http:\\/\\/www.warluzelle.com\\/page9\\/salle-de-convivialite\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page9\\/salle-de-convivialite\\\">Salle de convivialit\\u00e9<\\/a><\\/strong><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page6\\/prevoyance\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page6\\/prevoyance\\\"><strong>Pr\\u00e9voyance<\\/strong><\\/a><\\/li><\\/ul>\",\"position\":2}'),(131,'Page',2,2,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"11\",\"name\":\"pompes-funebres3\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"pompes_funebres35695049253eec\",\"nom_fichier\":\"pompes_funebres35695049253eec.jpg\",\"poids\":\"221\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"3\",\"url\":\"\\/files\\/2016\\/01\\/pompes_funebres35695049253eec.jpg\",\"created\":\"2016-01-12 14:50:10\",\"updated\":\"2016-01-12 14:50:10\"},\"Folder\":{\"id\":\"3\",\"name\":\"diapo pompes fun\\u00e8bres\",\"parent_id\":null,\"lft\":\"5\",\"rght\":\"6\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"10\",\"name\":\"pompes-funebres2\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"pompes_funebres256950490a9a20\",\"nom_fichier\":\"pompes_funebres256950490a9a20.jpg\",\"poids\":\"296\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"3\",\"url\":\"\\/files\\/2016\\/01\\/pompes_funebres256950490a9a20.jpg\",\"created\":\"2016-01-12 14:50:08\",\"updated\":\"2016-01-12 14:50:08\"},\"Folder\":{\"id\":\"3\",\"name\":\"diapo pompes fun\\u00e8bres\",\"parent_id\":null,\"lft\":\"5\",\"rght\":\"6\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"9\",\"name\":\"pompes-funebres1\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"pompes_funebres15695048fa4749\",\"nom_fichier\":\"pompes_funebres15695048fa4749.jpg\",\"poids\":\"271\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"3\",\"url\":\"\\/files\\/2016\\/01\\/pompes_funebres15695048fa4749.jpg\",\"created\":\"2016-01-12 14:50:07\",\"updated\":\"2016-01-12 14:50:07\"},\"Folder\":{\"id\":\"3\",\"name\":\"diapo pompes fun\\u00e8bres\",\"parent_id\":null,\"lft\":\"5\",\"rght\":\"6\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"8\",\"name\":\"pompes-funebres4\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"pompes_funebres45695048ec8309\",\"nom_fichier\":\"pompes_funebres45695048ec8309.jpg\",\"poids\":\"200\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"3\",\"url\":\"\\/files\\/2016\\/01\\/pompes_funebres45695048ec8309.jpg\",\"created\":\"2016-01-12 14:50:06\",\"updated\":\"2016-01-12 14:50:06\"},\"Folder\":{\"id\":\"3\",\"name\":\"diapo pompes fun\\u00e8bres\",\"parent_id\":null,\"lft\":\"5\",\"rght\":\"6\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":3}'),(132,'Page',3,0,'txtimg','{\"title\":\"\",\"txt\":\"Si vous souhaitez offrir un <strong>simple bouquet<\\/strong>, un <strong>coussin<\\/strong>, une <strong>gerbe<\\/strong>, une <strong>couronne<\\/strong> ou une <strong>corbeille<\\/strong>,<strong> Romain Warluzelle<\\/strong> et son \\u00e9quipe vous proposent une grande vari\\u00e9t\\u00e9 de <strong>compositions florales<\\/strong> adapt\\u00e9es \\u00e0 vos souhaits.<p>Pour les choisir dans nos <strong>magasins<\\/strong>, nos conseillers vous guideront dans les possibilit\\u00e9s qui s\'offrent \\u00e0 vous.<\\/p><p>Par ailleurs, nous nous chargeons \\u00e9galement de <strong>fleurir vos s\\u00e9pultures<\\/strong>.<\\/p><p>Nous pouvons d\\u00e9poser des fleurs \\u00e0 la demande, de fa\\u00e7on <strong>occasionnelle<\\/strong>, aux <strong>dates anniversaires<\\/strong>, mais \\u00e9galement de fa\\u00e7on r\\u00e9guli\\u00e8re avec <strong>diff\\u00e9rentes formules<\\/strong> que nous vous proposerons.<\\/p>\",\"img\":{\"File\":{\"id\":\"21\",\"name\":\"fleurs4\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"fleurs456950547c58fe\",\"nom_fichier\":\"fleurs456950547c58fe.jpg\",\"poids\":\"265\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/01\\/fleurs456950547c58fe.jpg\",\"created\":\"2016-01-12 14:53:11\",\"updated\":\"2016-01-12 14:53:11\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":1}'),(133,'Page',3,1,'txt','{\"title\":\"\",\"txt\":\"<p>L\'\\u00e9quipe de l\'entreprise <strong>Les Etablissements Warluzelle<\\/strong> situ\\u00e9s \\u00e0 Amiens dans <strong>la Somme<\\/strong> (<strong>80<\\/strong>) r\\u00e9alise des devis gratuits et personnalis\\u00e9s pour toujours mieux vous servir.<br>Pour en conna\\u00eetre d\'avantage sur les services disponibles, n\'h\\u00e9sitez pas \\u00e0 nous <a href=\\\"http:\\/\\/www.warluzelle.com\\/core\\/admin\\/page8\\/contact\\\" _cke_saved_href=\\\".\\/page8\\/contact\\\"><strong>joindre par t\\u00e9l\\u00e9phone<\\/strong><\\/a> ou en remplissant le <a href=\\\"http:\\/\\/www.warluzelle.com\\/core\\/admin\\/page8\\/contact\\\" _cke_saved_href=\\\".\\/page8\\/contact\\\"><strong>formulaire de contact<\\/strong>.<\\/a><br>D\\u00e9couvrez l\'ensemble de nos autres services :<\\/p><ul><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page2\\/chambre-funeraire\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page2\\/chambre-funeraire\\\"><strong>Chambres fun\\u00e9raires<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page3\\/pompes-funebres\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page3\\/pompes-funebres\\\"><strong>Pompes fun\\u00e8bres<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page5\\/marbrerie\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page5\\/marbrerie\\\"><strong>Marbrerie<\\/strong><\\/a><\\/li><li><strong><a href=\\\"http:\\/\\/www.warluzelle.com\\/page9\\/salle-de-convivialite\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page9\\/salle-de-convivialite\\\">Salle de convivialit\\u00e9<\\/a><\\/strong><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page6\\/prevoyance\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page6\\/prevoyance\\\"><strong>Pr\\u00e9voyance<\\/strong><\\/a><\\/li><\\/ul>\",\"position\":2}'),(134,'Page',3,2,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"20\",\"name\":\"fleurs5\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"fleurs556950539b3c86\",\"nom_fichier\":\"fleurs556950539b3c86.jpg\",\"poids\":\"296\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"5\",\"url\":\"\\/files\\/2016\\/01\\/fleurs556950539b3c86.jpg\",\"created\":\"2016-01-12 14:52:57\",\"updated\":\"2016-01-12 14:52:57\"},\"Folder\":{\"id\":\"5\",\"name\":\"diapo fleurs\",\"parent_id\":null,\"lft\":\"9\",\"rght\":\"10\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"19\",\"name\":\"fleurs4\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"fleurs456950538b3c23\",\"nom_fichier\":\"fleurs456950538b3c23.jpg\",\"poids\":\"265\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"5\",\"url\":\"\\/files\\/2016\\/01\\/fleurs456950538b3c23.jpg\",\"created\":\"2016-01-12 14:52:56\",\"updated\":\"2016-01-12 14:52:56\"},\"Folder\":{\"id\":\"5\",\"name\":\"diapo fleurs\",\"parent_id\":null,\"lft\":\"9\",\"rght\":\"10\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"17\",\"name\":\"fleurs2\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"fleurs256950535af4f8\",\"nom_fichier\":\"fleurs256950535af4f8.jpg\",\"poids\":\"261\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"5\",\"url\":\"\\/files\\/2016\\/01\\/fleurs256950535af4f8.jpg\",\"created\":\"2016-01-12 14:52:53\",\"updated\":\"2016-01-12 14:52:53\"},\"Folder\":{\"id\":\"5\",\"name\":\"diapo fleurs\",\"parent_id\":null,\"lft\":\"9\",\"rght\":\"10\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"18\",\"name\":\"fleurs3\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"fleurs356950537905c8\",\"nom_fichier\":\"fleurs356950537905c8.jpg\",\"poids\":\"257\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"5\",\"url\":\"\\/files\\/2016\\/01\\/fleurs356950537905c8.jpg\",\"created\":\"2016-01-12 14:52:55\",\"updated\":\"2016-01-12 14:52:55\"},\"Folder\":{\"id\":\"5\",\"name\":\"diapo fleurs\",\"parent_id\":null,\"lft\":\"9\",\"rght\":\"10\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"16\",\"name\":\"fleurs1\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"fleurs156950531e7f88\",\"nom_fichier\":\"fleurs156950531e7f88.jpg\",\"poids\":\"278\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"5\",\"url\":\"\\/files\\/2016\\/01\\/fleurs156950531e7f88.jpg\",\"created\":\"2016-01-12 14:52:49\",\"updated\":\"2016-01-12 14:52:49\"},\"Folder\":{\"id\":\"5\",\"name\":\"diapo fleurs\",\"parent_id\":null,\"lft\":\"9\",\"rght\":\"10\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":3}'),(135,'Page',4,0,'txtimg','{\"title\":\"\",\"txt\":\"<p>Les <strong>Etablissements Warluzelle<\\/strong> disposent d\'un large choix de <strong>marbreries<\\/strong>.<\\/p><p>Nos locaux sont situ\\u00e9s \\u00e0 <strong>Amiens<\\/strong>, dans le d\\u00e9partement de la <strong>Somme <\\/strong>(<strong>80<\\/strong>).<\\/p><p>Pour les <strong>Etablissements Warluzelle<\\/strong>, les <strong>granits<\\/strong> utilis\\u00e9s dans ses <strong>cr\\u00e9ations fun\\u00e9raires<\\/strong> se doivent d\'\\u00eatre irr\\u00e9prochables.<\\/p><p>Ces attentions se conjuguent avec un souci d\'<strong>innovation<\\/strong> et de <strong>cr\\u00e9ativit\\u00e9<\\/strong> dans la <strong>conception des mobiliers fun\\u00e9raires<\\/strong>.<\\/p><p>Avec nous, chacun peut <strong>inventer sa propre s\\u00e9pulture<\\/strong> ou choisir ses <strong>formes<\\/strong>, ses <strong>couleurs<\\/strong>, son type de granit, etc. parmi une vaste gamme de mod\\u00e8les de <strong>st\\u00e8les<\\/strong>, de <strong>pierres tombales<\\/strong>, de <strong>soubassements<\\/strong>, mais aussi des <strong>vasques<\\/strong> et <strong>monuments cin\\u00e9raires<\\/strong>, des <strong>croix<\\/strong>, des <strong>plaques<\\/strong> ou encore des <strong>vases<\\/strong>.<\\/p>\",\"img\":{\"File\":{\"id\":\"15\",\"name\":\"marbrerie2\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"marbrerie2569505030bdf6\",\"nom_fichier\":\"marbrerie2569505030bdf6.jpg\",\"poids\":\"296\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":null,\"url\":\"\\/files\\/2016\\/01\\/marbrerie2569505030bdf6.jpg\",\"created\":\"2016-01-12 14:52:03\",\"updated\":\"2016-01-12 14:52:03\"},\"Folder\":{\"id\":null,\"name\":null,\"parent_id\":null,\"lft\":null,\"rght\":null},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},\"params\":{\"prop\":\"img-right\"},\"position\":1}'),(136,'Page',4,1,'txt','{\"title\":\"\",\"txt\":\"<p><strong>Les Etablissements Warluzelle<\\/strong>, entreprise situ\\u00e9e \\u00e0 <strong>Amiens <\\/strong>dans <strong>la Somme<\\/strong> (<strong>80<\\/strong>) vous propose un devis gratuit et personnalis\\u00e9 par rapport \\u00e0 vos attentes, pour une satisfaction compl\\u00e8te de votre part.<br>Pour en conna\\u00eetre d\'avantage sur les services disponibles, n\'h\\u00e9sitez pas \\u00e0 nous <a href=\\\"http:\\/\\/www.warluzelle.com\\/core\\/admin\\/page8\\/contact\\\" _cke_saved_href=\\\".\\/page8\\/contact\\\"><strong>joindre par t\\u00e9l\\u00e9phone<\\/strong><\\/a> ou gr\\u00e2ce au <a href=\\\"http:\\/\\/www.warluzelle.com\\/core\\/admin\\/page8\\/contact\\\" _cke_saved_href=\\\".\\/page8\\/contact\\\"><strong>formulaire de contact<\\/strong>.<\\/a><br>Partez \\u00e0 la d\\u00e9couverte de nos autres prestations :<\\/p><ul><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page2\\/chambre-funeraire\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page2\\/chambre-funeraire\\\"><strong>Chambres fun\\u00e9raires<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page3\\/pompes-funebres\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page3\\/pompes-funebres\\\"><strong>Pompes fun\\u00e8bres<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page4\\/fleurs\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page4\\/fleurs\\\"><strong>Fleurs<\\/strong><\\/a><\\/li><li><strong><a href=\\\"http:\\/\\/www.warluzelle.com\\/page9\\/salle-de-convivialite\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page9\\/salle-de-convivialite\\\">Salle de convivialit\\u00e9<\\/a><\\/strong><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page6\\/prevoyance\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page6\\/prevoyance\\\"><strong>Pr\\u00e9voyance<\\/strong><\\/a><\\/li><\\/ul>\",\"position\":2}'),(137,'Page',4,2,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"14\",\"name\":\"marbrerie1\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"marbrerie1569504f62e6ed\",\"nom_fichier\":\"marbrerie1569504f62e6ed.jpg\",\"poids\":\"271\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"4\",\"url\":\"\\/files\\/2016\\/01\\/marbrerie1569504f62e6ed.jpg\",\"created\":\"2016-01-12 14:51:50\",\"updated\":\"2016-01-12 14:51:50\"},\"Folder\":{\"id\":\"4\",\"name\":\"diapo marbrerie\",\"parent_id\":null,\"lft\":\"7\",\"rght\":\"8\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"13\",\"name\":\"marbrerie2\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"marbrerie2569504f516b1d\",\"nom_fichier\":\"marbrerie2569504f516b1d.jpg\",\"poids\":\"296\",\"dimenssions\":{\"width\":602,\"height\":342,\"reco\":\"normal\"},\"file_folder_id\":\"4\",\"url\":\"\\/files\\/2016\\/01\\/marbrerie2569504f516b1d.jpg\",\"created\":\"2016-01-12 14:51:49\",\"updated\":\"2016-01-12 14:51:49\"},\"Folder\":{\"id\":\"4\",\"name\":\"diapo marbrerie\",\"parent_id\":null,\"lft\":\"7\",\"rght\":\"8\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":3}'),(138,'Page',1,0,'galerie','{\"title\":\"\",\"files\":[{\"File\":{\"id\":\"1\",\"name\":\"\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"diapo_accueil15694dcdc49076\",\"nom_fichier\":\"diapo_accueil15694dcdc49076.jpg\",\"poids\":\"382\",\"dimenssions\":{\"width\":1920,\"height\":660,\"reco\":\"trop grande\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2016\\/01\\/diapo_accueil15694dcdc49076.jpg\",\"created\":\"2016-01-12 12:00:44\",\"updated\":\"2016-01-12 12:00:44\"},\"Folder\":{\"id\":\"1\",\"name\":\"diapo accueil\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"2\",\"name\":\"\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"diapo_accueil25694fd512d696\",\"nom_fichier\":\"diapo_accueil25694fd512d696.jpg\",\"poids\":\"369\",\"dimenssions\":{\"width\":1920,\"height\":660,\"reco\":\"trop grande\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2016\\/01\\/diapo_accueil25694fd512d696.jpg\",\"created\":\"2016-01-12 14:19:13\",\"updated\":\"2016-01-12 14:19:13\"},\"Folder\":{\"id\":\"1\",\"name\":\"diapo accueil\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"3\",\"name\":\"\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"diapo_accueil3569503e27eee9\",\"nom_fichier\":\"diapo_accueil3569503e27eee9.jpg\",\"poids\":\"443\",\"dimenssions\":{\"width\":1920,\"height\":660,\"reco\":\"trop grande\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2016\\/01\\/diapo_accueil3569503e27eee9.jpg\",\"created\":\"2016-01-12 14:47:14\",\"updated\":\"2016-01-12 14:47:14\"},\"Folder\":{\"id\":\"1\",\"name\":\"diapo accueil\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"31\",\"name\":\"diapo-accueil1\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"diapo_accueil15a1687196de7f\",\"nom_fichier\":\"diapo_accueil15a1687196de7f.jpg\",\"poids\":\"557\",\"dimenssions\":{\"width\":2650,\"height\":911,\"reco\":\"trop grande\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2017\\/11\\/diapo_accueil15a1687196de7f.jpg\",\"created\":\"2017-11-23 09:30:17\",\"updated\":\"2017-11-23 09:30:17\"},\"Folder\":{\"id\":\"1\",\"name\":\"diapo accueil\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}},{\"File\":{\"id\":\"32\",\"name\":\"diapo-accueil2\",\"description\":\"\",\"lien\":\"\",\"embed\":\"\",\"extention_id\":\"jpg\",\"type\":\"image\",\"slug\":\"diapo_accueil25a16871aaf724\",\"nom_fichier\":\"diapo_accueil25a16871aaf724.jpg\",\"poids\":\"575\",\"dimenssions\":{\"width\":2650,\"height\":911,\"reco\":\"trop grande\"},\"file_folder_id\":\"1\",\"url\":\"\\/files\\/2017\\/11\\/diapo_accueil25a16871aaf724.jpg\",\"created\":\"2017-11-23 09:30:18\",\"updated\":\"2017-11-23 09:30:18\"},\"Folder\":{\"id\":\"1\",\"name\":\"diapo accueil\",\"parent_id\":null,\"lft\":\"1\",\"rght\":\"2\"},\"Extention\":{\"id\":\"jpg\",\"icone\":\"picture\",\"type\":\"image\"}}],\"position\":4}'),(139,'Page',7,0,'txt','{\"title\":\"\",\"txt\":\"<p>L\'\\u00e9quipe de l\'entreprise <strong>Les Etablissements Warluzelle<\\/strong> situ\\u00e9s \\u00e0 Amiens dans <strong>la Somme<\\/strong> (<strong>80<\\/strong>) vous propose un devis gratuit et personnalis\\u00e9 par rapport \\u00e0 vos attentes, pour une satisfaction compl\\u00e8te de votre part.<br>Pour en conna\\u00eetre d\'avantage sur les services disponibles, n\'h\\u00e9sitez pas \\u00e0 nous <a href=\\\"http:\\/\\/www.warluzelle.com\\/core\\/admin\\/page8\\/contact\\\" _cke_saved_href=\\\".\\/page8\\/contact\\\"><strong>contacter par t\\u00e9l\\u00e9phone<\\/strong><\\/a> ou en remplissant le <a href=\\\"http:\\/\\/www.warluzelle.com\\/core\\/admin\\/page8\\/contact\\\" _cke_saved_href=\\\".\\/page8\\/contact\\\"><strong>formulaire de contact<\\/strong>.<\\/a><br>D\\u00e9couvrez nos services :<\\/p><ul><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page2\\/chambre-funeraire\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page2\\/chambre-funeraire\\\"><strong>Chambres fun\\u00e9raires<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page3\\/pompes-funebres\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page3\\/pompes-funebres\\\"><strong>Pompes fun\\u00e8bres<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page4\\/fleurs\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page4\\/fleurs\\\"><strong>Fleurs<\\/strong><\\/a><\\/li><li><a href=\\\"http:\\/\\/www.warluzelle.com\\/page5\\/marbrerie\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page5\\/marbrerie\\\"><strong>Marbrerie<\\/strong><\\/a><\\/li><li><strong><a href=\\\"http:\\/\\/www.warluzelle.com\\/page9\\/salle-de-convivialite\\\" _cke_saved_href=\\\"http:\\/\\/www.warluzelle.com\\/page9\\/salle-de-convivialite\\\">Salle de convivialit\\u00e9<\\/a><\\/strong><\\/li><\\/ul>\",\"position\":2}');
/*!40000 ALTER TABLE `content_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_types`
--

DROP TABLE IF EXISTS `content_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_types` (
  `id` varchar(128) NOT NULL COMMENT 'Nom de la class model',
  `name` varchar(128) NOT NULL,
  `model` varchar(256) NOT NULL,
  `class_model` varchar(256) NOT NULL,
  `controleur` varchar(256) NOT NULL,
  `prefixe_url` varchar(256) NOT NULL,
  `sufixe_url` varchar(128) DEFAULT NULL,
  `construct_url` tinyint(1) DEFAULT NULL,
  `description` varchar(256) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `ordre` int(11) NOT NULL DEFAULT '0',
  `arborescence` tinyint(1) DEFAULT NULL,
  `associable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_types`
--

LOCK TABLES `content_types` WRITE;
/*!40000 ALTER TABLE `content_types` DISABLE KEYS */;
INSERT INTO `content_types` VALUES ('page','Page','Page','Page','pages','p-','',1,'Une page','file',2,1,1),('home','Accueil','Home','Home','homes','','',NULL,'La page d\'accueil','home',1,1,0),('contact','Contact','ContactManager.Contact','Contact','contact_manager/contacts','contact-','',1,'Une page contact','envelope',3,1,1),('actualite','Actualite','Blog.Actualite','Actualite','blog/actualites','news-','',NULL,'Une actualite','',0,0,1),('image','Image','','','','','',NULL,'','',0,0,0),('file','Fichier','','','','','',NULL,'','',0,0,0),('marque','Marque','Marque','Catalogue.Marque','catalogue/marques','',NULL,NULL,'','',0,NULL,0);
/*!40000 ALTER TABLE `content_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etats`
--

DROP TABLE IF EXISTS `etats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etats` (
  `id` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `class` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etats`
--

LOCK TABLES `etats` WRITE;
/*!40000 ALTER TABLE `etats` DISABLE KEYS */;
INSERT INTO `etats` VALUES ('draft','brouillon','danger'),('publish','en ligne','success');
/*!40000 ALTER TABLE `etats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `even_inscrits`
--

DROP TABLE IF EXISTS `even_inscrits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `even_inscrits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `even_id` int(11) NOT NULL,
  `first_name` varchar(128) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `even_id` (`even_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `even_inscrits`
--

LOCK TABLES `even_inscrits` WRITE;
/*!40000 ALTER TABLE `even_inscrits` DISABLE KEYS */;
/*!40000 ALTER TABLE `even_inscrits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extentions`
--

DROP TABLE IF EXISTS `extentions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extentions` (
  `id` varchar(10) CHARACTER SET latin1 NOT NULL,
  `icone` varchar(128) NOT NULL,
  `type` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extentions`
--

LOCK TABLES `extentions` WRITE;
/*!40000 ALTER TABLE `extentions` DISABLE KEYS */;
INSERT INTO `extentions` VALUES ('png','picture','image'),('jpg','picture','image'),('gif','picture','image'),('jpeg','picture','image'),('pdf','pdf','fichier'),('doc','word','fichier'),('xls','excel','fichier'),('ppt','powerpoint','fichier'),('docx','word','fichier'),('xlsx','excel','fichier'),('zip','archive','fichier'),('mp3','audio','audio'),('mp4','video','video'),('flv','video','video');
/*!40000 ALTER TABLE `extentions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_assocs`
--

DROP TABLE IF EXISTS `file_assocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_assocs` (
  `id` int(11) NOT NULL,
  `file_id` int(11) DEFAULT NULL,
  `model` varchar(128) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `group` varchar(256) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `default` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_assocs`
--

LOCK TABLES `file_assocs` WRITE;
/*!40000 ALTER TABLE `file_assocs` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_assocs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_folders`
--

DROP TABLE IF EXISTS `file_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_folders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_folders`
--

LOCK TABLES `file_folders` WRITE;
/*!40000 ALTER TABLE `file_folders` DISABLE KEYS */;
INSERT INTO `file_folders` VALUES (1,'diapo accueil',NULL,1,2),(2,'diapo salle de convivialité',NULL,3,4),(3,'diapo pompes funèbres',NULL,5,6),(4,'diapo marbrerie',NULL,7,8),(5,'diapo fleurs',NULL,9,10),(6,'diapo chambre funéraire',NULL,11,12);
/*!40000 ALTER TABLE `file_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_olds`
--

DROP TABLE IF EXISTS `file_olds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_olds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `lien` varchar(256) NOT NULL,
  `embed` text NOT NULL,
  `extention_id` varchar(10) DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  `slug` varchar(256) DEFAULT NULL,
  `nom_fichier` varchar(256) DEFAULT NULL,
  `poids` decimal(10,0) DEFAULT NULL,
  `dossier` varchar(256) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_olds`
--

LOCK TABLES `file_olds` WRITE;
/*!40000 ALTER TABLE `file_olds` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_olds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `lien` varchar(256) NOT NULL,
  `embed` text NOT NULL,
  `extention_id` varchar(10) DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  `slug` varchar(256) DEFAULT NULL,
  `nom_fichier` varchar(256) DEFAULT NULL,
  `poids` decimal(10,0) DEFAULT NULL,
  `dimenssions` text NOT NULL,
  `file_folder_id` int(11) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (1,'diapo-accueil1','','','','jpg','image','diapo_accueil15694dcdc49076','diapo_accueil15694dcdc49076.jpg',382,'{\"width\":1920,\"height\":660,\"reco\":\"trop grande\"}',1,'/files/2016/01/diapo_accueil15694dcdc49076.jpg','2016-01-12 12:00:44','2016-01-12 12:00:44'),(2,'diapo-accueil2','','','','jpg','image','diapo_accueil25694fd512d696','diapo_accueil25694fd512d696.jpg',369,'{\"width\":1920,\"height\":660,\"reco\":\"trop grande\"}',1,'/files/2016/01/diapo_accueil25694fd512d696.jpg','2016-01-12 14:19:13','2016-01-12 14:19:13'),(3,'diapo-accueil3','','','','jpg','image','diapo_accueil3569503e27eee9','diapo_accueil3569503e27eee9.jpg',443,'{\"width\":1920,\"height\":660,\"reco\":\"trop grande\"}',1,'/files/2016/01/diapo_accueil3569503e27eee9.jpg','2016-01-12 14:47:14','2016-01-12 14:47:14'),(4,'chambre-funeraire5','','','','jpg','image','chambre_funeraire556950457364b5','chambre_funeraire556950457364b5.jpg',172,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',2,'/files/2016/01/chambre_funeraire556950457364b5.jpg','2016-01-12 14:49:11','2016-01-12 14:49:11'),(5,'chambre-funeraire3','','','','jpg','image','chambre_funeraire3569504580bc6e','chambre_funeraire3569504580bc6e.jpg',179,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',2,'/files/2016/01/chambre_funeraire3569504580bc6e.jpg','2016-01-12 14:49:12','2016-01-12 14:49:12'),(6,'chambre-funeraire4','','','','jpg','image','chambre_funeraire456950458b89eb','chambre_funeraire456950458b89eb.jpg',160,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',2,'/files/2016/01/chambre_funeraire456950458b89eb.jpg','2016-01-12 14:49:12','2016-01-12 14:49:12'),(7,'chambre-funeraire5','','','','jpg','image','chambre_funeraire556950466da95a','chambre_funeraire556950466da95a.jpg',172,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',NULL,'/files/2016/01/chambre_funeraire556950466da95a.jpg','2016-01-12 14:49:26','2016-01-12 14:49:26'),(8,'pompes-funebres4','','','','jpg','image','pompes_funebres45695048ec8309','pompes_funebres45695048ec8309.jpg',200,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',3,'/files/2016/01/pompes_funebres45695048ec8309.jpg','2016-01-12 14:50:06','2016-01-12 14:50:06'),(9,'pompes-funebres1','','','','jpg','image','pompes_funebres15695048fa4749','pompes_funebres15695048fa4749.jpg',271,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',3,'/files/2016/01/pompes_funebres15695048fa4749.jpg','2016-01-12 14:50:07','2016-01-12 14:50:07'),(10,'pompes-funebres2','','','','jpg','image','pompes_funebres256950490a9a20','pompes_funebres256950490a9a20.jpg',296,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',3,'/files/2016/01/pompes_funebres256950490a9a20.jpg','2016-01-12 14:50:08','2016-01-12 14:50:08'),(11,'pompes-funebres3','','','','jpg','image','pompes_funebres35695049253eec','pompes_funebres35695049253eec.jpg',221,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',3,'/files/2016/01/pompes_funebres35695049253eec.jpg','2016-01-12 14:50:10','2016-01-12 14:50:10'),(12,'pompes-funebres1','','','','jpg','image','pompes_funebres1569504cc84392','pompes_funebres1569504cc84392.jpg',271,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',NULL,'/files/2016/01/pompes_funebres1569504cc84392.jpg','2016-01-12 14:51:08','2016-01-12 14:51:08'),(13,'marbrerie2','','','','jpg','image','marbrerie2569504f516b1d','marbrerie2569504f516b1d.jpg',296,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',4,'/files/2016/01/marbrerie2569504f516b1d.jpg','2016-01-12 14:51:49','2016-01-12 14:51:49'),(14,'marbrerie1','','','','jpg','image','marbrerie1569504f62e6ed','marbrerie1569504f62e6ed.jpg',271,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',4,'/files/2016/01/marbrerie1569504f62e6ed.jpg','2016-01-12 14:51:50','2016-01-12 14:51:50'),(15,'marbrerie2','','','','jpg','image','marbrerie2569505030bdf6','marbrerie2569505030bdf6.jpg',296,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',NULL,'/files/2016/01/marbrerie2569505030bdf6.jpg','2016-01-12 14:52:03','2016-01-12 14:52:03'),(16,'fleurs1','','','','jpg','image','fleurs156950531e7f88','fleurs156950531e7f88.jpg',278,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',5,'/files/2016/01/fleurs156950531e7f88.jpg','2016-01-12 14:52:49','2016-01-12 14:52:49'),(17,'fleurs2','','','','jpg','image','fleurs256950535af4f8','fleurs256950535af4f8.jpg',261,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',5,'/files/2016/01/fleurs256950535af4f8.jpg','2016-01-12 14:52:53','2016-01-12 14:52:53'),(18,'fleurs3','','','','jpg','image','fleurs356950537905c8','fleurs356950537905c8.jpg',257,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',5,'/files/2016/01/fleurs356950537905c8.jpg','2016-01-12 14:52:55','2016-01-12 14:52:55'),(19,'fleurs4','','','','jpg','image','fleurs456950538b3c23','fleurs456950538b3c23.jpg',265,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',5,'/files/2016/01/fleurs456950538b3c23.jpg','2016-01-12 14:52:56','2016-01-12 14:52:56'),(20,'fleurs5','','','','jpg','image','fleurs556950539b3c86','fleurs556950539b3c86.jpg',296,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',5,'/files/2016/01/fleurs556950539b3c86.jpg','2016-01-12 14:52:57','2016-01-12 14:52:57'),(21,'fleurs4','','','','jpg','image','fleurs456950547c58fe','fleurs456950547c58fe.jpg',265,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',NULL,'/files/2016/01/fleurs456950547c58fe.jpg','2016-01-12 14:53:11','2016-01-12 14:53:11'),(22,'chambre-funeraire5','','','','jpg','image','chambre_funeraire5569517b87d8dc','chambre_funeraire5569517b87d8dc.jpg',172,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',6,'/files/2016/01/chambre_funeraire5569517b87d8dc.jpg','2016-01-12 16:11:52','2016-01-12 16:11:52'),(23,'chambre-funeraire6','','','','jpg','image','chambre_funeraire6569517d471ba2','chambre_funeraire6569517d471ba2.jpg',163,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',6,'/files/2016/01/chambre_funeraire6569517d471ba2.jpg','2016-01-12 16:12:20','2016-01-12 16:12:20'),(24,'chambre-funeraire7','','','','jpg','image','chambre_funeraire7569517ec0dc19','chambre_funeraire7569517ec0dc19.jpg',140,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',6,'/files/2016/01/chambre_funeraire7569517ec0dc19.jpg','2016-01-12 16:12:44','2016-01-12 16:12:44'),(25,'chambre-funeraire1','','','','jpg','image','chambre_funeraire1569517ede7fc0','chambre_funeraire1569517ede7fc0.jpg',191,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',6,'/files/2016/01/chambre_funeraire1569517ede7fc0.jpg','2016-01-12 16:12:45','2016-01-12 16:12:45'),(26,'chambre-funeraire2','','','','jpg','image','chambre_funeraire2569517ef390b4','chambre_funeraire2569517ef390b4.jpg',169,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',6,'/files/2016/01/chambre_funeraire2569517ef390b4.jpg','2016-01-12 16:12:47','2016-01-12 16:12:47'),(27,'chambre-funeraire3','','','','jpg','image','chambre_funeraire3569517f0847b3','chambre_funeraire3569517f0847b3.jpg',179,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',6,'/files/2016/01/chambre_funeraire3569517f0847b3.jpg','2016-01-12 16:12:48','2016-01-12 16:12:48'),(28,'chambre-funeraire4','','','','jpg','image','chambre_funeraire45695180ab6193','chambre_funeraire45695180ab6193.jpg',160,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',6,'/files/2016/01/chambre_funeraire45695180ab6193.jpg','2016-01-12 16:13:14','2016-01-12 16:13:14'),(29,'chambre-funeraire2','','','','jpg','image','chambre_funeraire25695185310752','chambre_funeraire25695185310752.jpg',169,'{\"width\":602,\"height\":342,\"reco\":\"normal\"}',NULL,'/files/2016/01/chambre_funeraire25695185310752.jpg','2016-01-12 16:14:27','2016-01-12 16:14:27'),(30,'page-prevoyance','','','','jpg','image','page_prevoyance56951a94f0988','page_prevoyance56951a94f0988.jpg',85,'{\"width\":282,\"height\":342,\"reco\":\"normal\"}',NULL,'/files/2016/01/page_prevoyance56951a94f0988.jpg','2016-01-12 16:24:04','2016-01-12 16:24:04'),(31,'diapo-accueil1','','','','jpg','image','diapo_accueil15a1687196de7f','diapo_accueil15a1687196de7f.jpg',557,'{\"width\":2650,\"height\":911,\"reco\":\"trop grande\"}',1,'/files/2017/11/diapo_accueil15a1687196de7f.jpg','2017-11-23 09:30:17','2017-11-23 09:30:17'),(32,'diapo-accueil2','','','','jpg','image','diapo_accueil25a16871aaf724','diapo_accueil25a16871aaf724.jpg',575,'{\"width\":2650,\"height\":911,\"reco\":\"trop grande\"}',1,'/files/2017/11/diapo_accueil25a16871aaf724.jpg','2017-11-23 09:30:18','2017-11-23 09:30:18'),(33,'diapo-accueil3','','','','jpg','image','diapo_accueil35a16871c37b5b','diapo_accueil35a16871c37b5b.jpg',713,'{\"width\":2650,\"height\":911,\"reco\":\"trop grande\"}',1,'/files/2017/11/diapo_accueil35a16871c37b5b.jpg','2017-11-23 09:30:20','2017-11-23 09:30:20'),(34,'diapo-accueil1','','','','jpg','image','diapo_accueil15a168732699dd','diapo_accueil15a168732699dd.jpg',557,'{\"width\":2650,\"height\":911,\"reco\":\"trop grande\"}',NULL,'/files/2017/11/diapo_accueil15a168732699dd.jpg','2017-11-23 09:30:42','2017-11-23 09:30:42');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_elements`
--

DROP TABLE IF EXISTS `form_elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `short_name` varchar(16) NOT NULL,
  `type` varchar(16) DEFAULT NULL,
  `sub_type` varchar(64) DEFAULT NULL,
  `mandatory` tinyint(1) NOT NULL,
  `configuration` text NOT NULL,
  `position` int(11) NOT NULL,
  `created` datetime NOT NULL COMMENT 'CakePHP',
  `updated` datetime NOT NULL COMMENT 'CakePHP',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_elements`
--

LOCK TABLES `form_elements` WRITE;
/*!40000 ALTER TABLE `form_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `form_elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_entries`
--

DROP TABLE IF EXISTS `form_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_ip` varchar(15) NOT NULL,
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `collection_id` varchar(13) NOT NULL,
  `created` datetime NOT NULL COMMENT 'CakePHP',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_entries`
--

LOCK TABLES `form_entries` WRITE;
/*!40000 ALTER TABLE `form_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `form_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms`
--

DROP TABLE IF EXISTS `forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `elements_number` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) DEFAULT NULL,
  `etat_id` varchar(128) DEFAULT NULL,
  `created` datetime NOT NULL COMMENT 'CakePHP',
  `updated` datetime NOT NULL COMMENT 'CakePHP',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms`
--

LOCK TABLES `forms` WRITE;
/*!40000 ALTER TABLE `forms` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,NULL,1,2,'Administrateurs','2015-01-19 11:37:29'),(2,NULL,0,0,'Redacteurs','2015-11-29 21:28:38');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homes`
--

DROP TABLE IF EXISTS `homes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homes` (
  `id` int(11) NOT NULL,
  `content_type_id` varchar(128) NOT NULL,
  `etat_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL DEFAULT '',
  `h1` varchar(256) NOT NULL,
  `url` varchar(1) DEFAULT '/',
  `url_r` varchar(1) NOT NULL DEFAULT '/',
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL DEFAULT '',
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL DEFAULT '',
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `menu_item_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homes`
--

LOCK TABLES `homes` WRITE;
/*!40000 ALTER TABLE `homes` DISABLE KEYS */;
/*!40000 ALTER TABLE `homes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `i18n`
--

DROP TABLE IF EXISTS `i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `i18n` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `i18n`
--

LOCK TABLES `i18n` WRITE;
/*!40000 ALTER TABLE `i18n` DISABLE KEYS */;
/*!40000 ALTER TABLE `i18n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_campaigns`
--

DROP TABLE IF EXISTS `newsletter_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_campaigns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(128) DEFAULT NULL COMMENT 'sms ou email',
  `etat_id` varchar(128) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `template_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `list_id` int(11) NOT NULL,
  `default_from_name` varchar(128) NOT NULL,
  `default_from_email` varchar(256) NOT NULL,
  `default_from_telephone` varchar(128) DEFAULT NULL,
  `default_from_subject` varchar(256) NOT NULL,
  `how_on_list` text NOT NULL,
  `send` tinyint(1) NOT NULL DEFAULT '0',
  `nbr` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_campaigns`
--

LOCK TABLES `newsletter_campaigns` WRITE;
/*!40000 ALTER TABLE `newsletter_campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_emails`
--

DROP TABLE IF EXISTS `newsletter_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_emails` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) CHARACTER SET utf16 NOT NULL,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unsuscribe_link` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `liste_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_emails`
--

LOCK TABLES `newsletter_emails` WRITE;
/*!40000 ALTER TABLE `newsletter_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_list_emails`
--

DROP TABLE IF EXISTS `newsletter_list_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_list_emails` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `list_id` int(10) NOT NULL,
  `email_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_list_emails`
--

LOCK TABLES `newsletter_list_emails` WRITE;
/*!40000 ALTER TABLE `newsletter_list_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_list_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_lists`
--

DROP TABLE IF EXISTS `newsletter_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_lists` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_lists`
--

LOCK TABLES `newsletter_lists` WRITE;
/*!40000 ALTER TABLE `newsletter_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_templates`
--

DROP TABLE IF EXISTS `newsletter_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `folder_name` varchar(256) NOT NULL,
  `is_uploaded` int(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_templates`
--

LOCK TABLES `newsletter_templates` WRITE;
/*!40000 ALTER TABLE `newsletter_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_olds`
--

DROP TABLE IF EXISTS `page_olds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_olds` (
  `id` int(11) NOT NULL,
  `menu_item_id` int(11) DEFAULT NULL,
  `content_type_id` varchar(128) NOT NULL,
  `etat_id` tinyint(1) DEFAULT NULL,
  `menu_id` tinyint(1) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `h1` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `url_r` varchar(256) NOT NULL DEFAULT '',
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `icone` varchar(128) DEFAULT NULL,
  `template` varchar(128) DEFAULT NULL,
  `slug` varchar(256) NOT NULL,
  `rght` int(11) NOT NULL,
  `lft` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_olds`
--

LOCK TABLES `page_olds` WRITE;
/*!40000 ALTER TABLE `page_olds` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_olds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_type_id` varchar(128) NOT NULL DEFAULT 'page',
  `etat_id` varchar(128) NOT NULL DEFAULT 'draft',
  `publication_date` date NOT NULL,
  `menu_id` tinyint(1) DEFAULT NULL,
  `acces_membre` tinyint(1) DEFAULT '0',
  `name` varchar(128) NOT NULL,
  `h1` varchar(256) NOT NULL,
  `sous_titre` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `url_r` varchar(256) NOT NULL DEFAULT '',
  `vignette` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_title` varchar(256) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_robots` varchar(128) NOT NULL,
  `section` varchar(128) DEFAULT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `rss_name` varchar(128) DEFAULT NULL,
  `rss_description` text,
  `rss_niveau` varchar(20) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `icone` varchar(128) DEFAULT NULL,
  `template` varchar(128) DEFAULT NULL,
  `slug` varchar(256) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'page','publish','2015-12-07',1,0,'Accueil','Accueil','Pompes Funèbres Warluzelle à Amiens, dans la Somme (80)','/accueil/p-1','','','Découvrez l’univers des Pompes Funèbres Warluzelle, situées à Amiens, dans le département de la Somme (80) – Marbrerie, fleurs, chambres funéraires, et prévoyance','Pompes Funèbres – Amiens Somme 80','','',NULL,'','<p>\n	Les <strong>Etablissements Warluzelle </strong>vous proposent d\'être votre interlocuteur privilégié pour l\'organisation complète des <strong>funérailles</strong> de vos proches.\n</p><p>\n	 Les <strong>pompes funèbres</strong><strong> Warluzelle </strong>sont situés à <strong>Amiens</strong>, dans le département de la <strong>Somme (80)</strong>.\n</p><h2><strong>Pompes Funèbres Warluzelle, une famille au service des familles</strong></h2><p>\n	Le plus important pour les <strong>pompes funèbres Warluzelle</strong>, c\'est adhérer à une éthique et à un professionnalisme qui se déploient autour de notre sens de l\'écoute, du conseil et du service. Les obsèques sont organisées selon vos valeurs, vos souhaits et vos envies.\n</p><p>\n	Nous sommes là pour vous tous les jours de l\'année, à toute heure du jour ou de la nuit.\n</p><h2>Pompes Funèbres Warluzelle, la qualité au meilleur prix</h2><p>\n	Au-delà des aspects administratifs et légaux dont nous nous occupons, nous mettons à votre disposition notre expérience de marbrier en vous proposant un choix très riche de monuments qui vont du plus simple au plus majestueux et toujours au meilleur prix.\n</p><p>\n	Nos gammes sont diversifiées, créatives, innovantes, pour personnaliser dans leurs moindres détails les obsèques de vos proches.\n</p><h2>Anticipez avec les Pompes Funèbres Warluzelle</h2><p>\n	Préparer ses obsèques à l\'avance avec des contrats obsèques, c\'est simple, facile et rassurant ! Vos volontés sont scrupuleusement respectées et tout ce qui a été défini sera mis en place.\n</p><p>\n	<strong>Les Etablissements Warluzelle</strong>, entreprise située à <strong>Amiens </strong>dans <strong>la Somme</strong> (<strong>80</strong>) vous propose un devis gratuit et personnalisé par rapport à vos attentes, pour une satisfaction complète de votre part.<br>\n	Pour en connaître d\'avantage sur les services disponibles, n\'hésitez pas à nous contacter par téléphone au <strong>03.22.47.02.02</strong><span></span> ou en remplissant le <a href=\"/pages/view/9\">formulaire de contact</a>.<br>\n	Découvrez l\'ensemble de nos prestations :\n</p><ul>\n	<li><a href=\"/pages/view/5\" _cke_saved_href=\"http://www.warluzelle.com/page2/chambre-funeraire\">Chambres funéraires</a></li>\n	<li><a href=\"/pages/view/2\" _cke_saved_href=\"http://www.warluzelle.com/page3/pompes-funebres\">Pompes funèbres</a></li>\n	<li><a href=\"http://www.warluzelle.com/page4/fleurs\" _cke_saved_href=\"http://www.warluzelle.com/page4/fleurs\"><strong>Fleurs</strong></a></li>\n	<li><a href=\"http://www.warluzelle.com/page5/marbrerie\" _cke_saved_href=\"http://www.warluzelle.com/page5/marbrerie\"><strong>Marbrerie</strong></a></li>\n	<li><strong><a href=\"http://www.warluzelle.com/page9/salle-de-convivialite\" _cke_saved_href=\"http://www.warluzelle.com/page9/salle-de-convivialite\">Salle de convivialité</a></strong></li>\n	<li><a href=\"http://www.warluzelle.com/page6/prevoyance\" _cke_saved_href=\"http://www.warluzelle.com/page6/prevoyance\"><strong>Prévoyance</strong></a></li>\n</ul>',NULL,NULL,NULL,'2017-11-23 09:37:14','2015-12-07 16:23:50',NULL,'home','',0,NULL,1,2),(2,'page','publish','2015-12-07',1,0,'Pompes funèbres','Pompes funèbres','Services des Pompes Funèbres Warluzelle à Amiens, dans la Somme (80)','/pages/view/2','','','Découvrez les services de pompes funèbres ainsi que l’univers des Pompes Funèbres Warluzelle, situées à Amiens, dans le département de la Somme (80)','Pompes funèbres Amiens Somme 80','','',NULL,'','<p>Les <strong>Etablissements Warluzelle</strong> vous proposent l\'organisation de vos <strong>obsèques de A à Z</strong>.</p><p>Nos locaux sont situés à <strong>Amiens</strong>, dans le département de la <strong>Somme </strong>(<strong>80</strong>).</p><p>Dans le cadre de l\'<strong>organisation d\'obsèques</strong>, les <strong>Pompes Funèbres Warluzelle</strong> offrent une large gamme de services.</p>',NULL,NULL,NULL,'2016-01-12 16:30:56','2015-12-07 16:25:26',NULL,'view-pompes-funebres','pompes-funebres-warluzelle',1,NULL,3,4),(3,'page','publish','2015-12-07',1,0,'Fleurs','Fleurs','Fleurs et compositions florales à Amiens, dans la Somme (80)','/pages/view/3','','','Découvrez les fleurs et les compositions florales ainsi que l’univers des Pompes Funèbres Warluzelle, situées à Amiens, dans le département de la Somme (80)','Fleurs et composition florale – Amiens Somme 80','','',NULL,'','<p>Les <strong>Etablissements Warluzelle</strong> disposent d\'un large choix de fleurs et de compositions florales.</p><p>Nos locaux sont situés à <strong>Amiens</strong>, dans le département de la <strong>Somme </strong>(<strong>80</strong>).</p>',NULL,NULL,NULL,'2016-01-12 16:30:56','2015-12-07 16:26:31',NULL,'view-fleurs','fleurs-composition-florale-de-deuil-amiens',2,NULL,5,6),(4,'page','publish','2015-12-07',1,0,'Marbrerie','Marbrerie','Marbrerie à Amiens, dans la Somme (80)','/pages/view/4','','','Découvrez les marbreries ainsi que l’univers des Pompes Funèbres Warluzelle, situées à Amiens, dans le département de la Somme (80)','Marbrerie – Amiens Somme 80','','',NULL,'','',NULL,NULL,NULL,'2016-01-12 16:30:56','2015-12-07 16:27:52',NULL,'view-marbrerie','',3,NULL,7,8),(5,'page','publish','2015-12-07',1,0,'Chambre funéraire','Chambre funéraire','Chambres funéraires à Amiens, dans la Somme (80)','/pages/view/5','','','Découvrez les chambres funéraires ainsi que l’univers des Pompes Funèbres Warluzelle, situées à Amiens, dans le département de la Somme (80)','Chambres funéraires – Amiens Somme 80','','',NULL,'','',NULL,NULL,NULL,'2016-01-12 16:30:56','2015-12-07 16:29:33',NULL,'view-chambre','',4,NULL,9,10),(6,'page','publish','2015-12-07',1,0,'Salle de convivialité','Salle de convivialité','Salle de convivialité à Amiens, dans la Somme (80)','/pages/view/6','','','Découvrez la salle de convivialité ainsi que l’univers des Pompes Funèbres Warluzelle, situées à Amiens, dans le département de la Somme (80)','Salle de convivialité – Amiens Somme 80','','',NULL,'','',NULL,NULL,NULL,'2016-01-12 16:30:56','2015-12-07 16:37:50',NULL,'view-convivialite','',5,NULL,11,12),(7,'page','publish','2015-12-07',1,0,'Prévoyance','Prévoyance','Contrat de prévoyance à Amiens, dans la Somme (80)','/prevoyance/p-7','','','Découvrez les contrats prévoyances ainsi que l’univers des Pompes Funèbres Warluzelle, situées à Amiens, dans le département de la Somme (80)','Contrat prévoyance – Amiens Somme 80','','',NULL,'','<p>Les <strong>Etablissements Warluzelle</strong> disposent d\'un large choix de fleurs et de compositions florales.</p><p>Nos locaux sont situés à <strong>Amiens</strong>, dans le département de la <strong>Somme </strong>(<strong>80</strong>).</p><p>La <strong>prévoyance obsèques</strong> permet de votre vivant d\'<strong>organiser vos obsèques </strong>ou celle d\'un proche en toute sérénité et d\'en sélectionner le moyen de financement.</p><p>Ainsi, vous <strong>déchargez financièrement</strong> et <strong>matériellement</strong> votre famille au moment venu.</p><p>Il existe deux types de contrats obsèques :</p><ul><li>Le <strong>contrat en prestations</strong> qui permet de <strong>prévoir</strong> les <strong>modalités de financement</strong> et le <strong>déroulement des obsèques</strong>. Il peut être souscrit auprès de notre <strong>agence de pompes funèbres</strong> : Les <strong>Pompes Funèbres Warluzelle</strong>.</li><li>Le <strong>contrat en capital</strong> prévoit uniquement le <strong>financement des obsèques</strong> en <strong>reversant une somme aux bénéficiaires désignés</strong> après le décès du souscripteur. L\'argent reversé est destiné à l\'<strong>organisation des obsèques</strong>. Il peut être souscrit auprès de notre <strong>agence de pompes funèbres</strong> : Les <strong>Pompes Funèbres Warluzelle</strong>. Dans ce cas, nous mettrons en relation l\'assuré avec une banque ou un assureur.</li></ul><p>Les <strong>Etablissements Warluzelle</strong> vous proposent ces deux contrats.</p><p style=\"text-align: center;\"><a href=\"http://warluzelle.kobaltis.net/conditions-generales.pdf\" _cke_saved_href=\"/data/documents/Conditions-generales.pdf\" target=\"_blank\"><img alt=\"\" src=\"http://www.warluzelle.com/data/documents/btn-conditions.png\"></a></p><p>Découvrez les avantages que nous proposons :</p><ul><li><strong>Aucune condition d\'âge</strong></li><li><strong>Aucun </strong><strong>questionnaire médical</strong> à remplir</li><li><strong>Libre choix du montant des cotisations</strong></li><li><strong>Libre choix du mode de paiement</strong></li><li><strong>Avantages fiscaux</strong></li></ul>',NULL,NULL,NULL,'2017-11-23 09:45:39','2015-12-07 16:40:37',NULL,'view-prevoyance','',6,NULL,13,14),(8,'page','publish','2015-12-07',1,0,'Plan','Plan','Les Pompes Funèbres Warluzelle à Amiens, dans la Somme (80)','/pages/view/8','','','Situez et découvrez l’univers des Pompes Funèbres Warluzelle, situées à Amiens, dans le département de la Somme (80) – Marbrerie, fleurs, chambres funéraires, et prévoyance','Pompes Funèbres – Amiens Plan Somme 80','','',NULL,'','<p><a href=\"http://www.warluzelle.com/\" _cke_saved_href=\"http://www.warluzelle.com/\"><strong>Les Etablissements Warluzelle</strong></a> vous accueillent à Amiens, dans <strong>la Somme</strong> (<strong>80</strong>) dans ses locaux pour pouvoir vous renseigner et répondre à vos attentes: <a href=\"http://www.warluzelle.com/page2/chambre-funeraire\" _cke_saved_href=\"http://www.warluzelle.com/page2/chambre-funeraire\"><strong>Chambres funéraires</strong></a>, <a href=\"http://www.warluzelle.com/page3/pompes-funebres\" _cke_saved_href=\"http://www.warluzelle.com/page3/pompes-funebres\"><strong>Pompes funèbres</strong></a>, <a href=\"http://www.warluzelle.com/page4/fleurs\" _cke_saved_href=\"http://www.warluzelle.com/page4/fleurs\"><strong>Fleurs</strong></a>, <a href=\"http://www.warluzelle.com/page5/marbrerie\" _cke_saved_href=\"http://www.warluzelle.com/page5/marbrerie\"><strong>Marbrerie</strong></a>, et <a href=\"http://www.warluzelle.com/page6/prevoyance\" _cke_saved_href=\"http://www.warluzelle.com/page6/prevoyance\"><strong>Prévoyance</strong></a>.<br>Nous sommes situés au 94-96, rue de la Troisième D.I, 80090 Amiens<br>Afin de situer plus facilement Les Pompes Funèbres Warluzelle, consultez le plan d\'accès ci-dessous.<br><br>Prenez <a href=\"http://www.warluzelle.com/core/admin/page8/contact\" _cke_saved_href=\"./page8/contact\"><strong>contact avec nous</strong></a> pour toute information complémentaire sur nos services.</p>',NULL,NULL,NULL,'2016-01-12 16:30:56','2015-12-07 16:42:00',NULL,'page','',7,NULL,15,16),(9,'page','publish','2015-12-07',1,0,'Contact','Contact','Contactez nous pour toutes informations supplémentaires ou tout devis','/pages/view/9','','','Contactez et découvrez l’univers des Pompes Funèbres Warluzelle, situées à Amiens, dans le département de la Somme (80) – Marbrerie, fleurs, chambres funéraires, et prévoyance','Pompes Funèbres – Amiens Contact Somme 80','','',NULL,'','<p>Pour plus de renseignement concernant nos différents services (<a href=\"http://www.warluzelle.com/page2/chambre-funeraire\" _cke_saved_href=\"http://www.warluzelle.com/page2/chambre-funeraire\"><strong>Chambres funéraires</strong></a>, <a href=\"http://www.warluzelle.com/page3/pompes-funebres\" _cke_saved_href=\"http://www.warluzelle.com/page3/pompes-funebres\"><strong>Pompes funèbres</strong></a>, <a href=\"http://www.warluzelle.com/page4/fleurs\" _cke_saved_href=\"http://www.warluzelle.com/page4/fleurs\"><strong>Fleurs</strong></a>, <a href=\"http://www.warluzelle.com/page5/marbrerie\" _cke_saved_href=\"http://www.warluzelle.com/page5/marbrerie\"><strong>Marbrerie</strong></a>, et <a href=\"http://www.warluzelle.com/page6/prevoyance\" _cke_saved_href=\"http://www.warluzelle.com/page6/prevoyance\"><strong>Prévoyance</strong></a>.), contactez-nous par téléphone ou via le formulaire de contact ci-dessous.</p><p style=\"text-align: center;\"><a href=\"http://www.warluzelle.com/\" _cke_saved_href=\"http://www.warluzelle.com/\"><strong>Les Etablissements Warluzelle</strong></a><br>94-96, rue de la Troisième D.I - 80090 <strong>Amiens </strong>- Tel.: 03 22 47 02 02 - Fax.: 03 22 47 57 26</p>',NULL,NULL,NULL,'2016-01-12 16:30:56','2015-12-07 16:43:46',NULL,'contact','',8,NULL,17,18),(10,'page','publish','2016-01-12',0,0,'Mentions légales','Mentions légales','','/pages/view/10','','','','','','',NULL,'','<p>En vertu de l\'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l\'économie numérique, il est précisé aux utilisateurs du site http://www.warluzelle.com/<span></span> l\'identité des différents intervenants dans le cadre de sa réalisation et de son suivi :<br><strong>Propriétaire Mr Warluzelle Romain</strong><br><strong>Webdesign :</strong><a href=\"http://www.kobaltis.com/\" target=\"_blank\"> Agence KOBALTIS</a> – <strong>Développements :</strong><a href=\"http://www.kobaltis.com/\" target=\"_blank\"> KOBALTIS</a><br><strong>Responsable publication : </strong>kobaltis-webmaster@kobaltis.com<br>Le responsable publication est une personne physique ou une personne morale.<br><strong>Hébergeur :</strong> OVH – 2 rue Kellermann 59100 Roubaix – France</p><p><br><br><strong>2. Conditions générales d\'utilisation du site et des services proposés.</strong><br>L\'utilisation du site http://www.warluzelle.com/<span></span> implique l\'acceptation pleine et entière des conditions générales d\'utilisation ci-après décrites. Ces conditions d\'utilisation sont susceptibles d\'être modifiées ou complétées à tout moment, les utilisateurs du site http://www.warluzelle.com/<span></span> sont donc invités à les consulter de manière régulière. Ce site est normalement accessible à tout moment aux utilisateurs. Une interruption pour raison de maintenance technique peut être toutefois décidée par Matra Electronique. Le site http://www.warluzelle.com/<span></span> est mis à jour régulièrement. De la même façon, les mentions légales peuvent être modifiées à tout moment : elles s\'imposent néanmoins à l\'utilisateur qui est invité à s\'y référer le plus souvent possible afin d\'en prendre connaissance.<br>Les copies et téléchargements ne sont autorisées que pour un usage personnel, privé et non-commercial.</p><p><br><br><strong>3. Description des services fournis.</strong> Le site http://www.warluzelle.com/<span></span> a pour objet de fournir une information concernant l\'ensemble des activités de la société.<br>http://www.warluzelle.com/<span></span> possible. Toutefois, il ne pourra être tenue responsable des omissions, des inexactitudes et des carences dans la mise à jour, qu\'elles soient de son fait ou du fait des tiers partenaires qui lui fournissent ces informations. Tous les informations indiquées sur le site http://www.warluzelle.com/<span></span> sont données à titre indicatif, et sont susceptibles d\'évoluer. Par ailleurs, les renseignements figurant sur le site http://www.warluzelle.com/<span></span> ne sont pas exhaustifs. Ils sont donnés sous réserve de modifications ayant été apportées depuis leur mise en ligne.</p><p><br><br><strong>4. Limitations contractuelles sur les données techniques.</strong><br>KOBALTIS ne pourra être tenu responsable de dommages matériels liés à l\'utilisation du site. De plus, l\'utilisateur du site s\'engage à accéder au site en utilisant un matériel récent, ne contenant pas de virus et avec un navigateur de dernière génération mis-à-jour.</p><p><br><br><strong>5. Propriété intellectuelle et contrefaçons.</strong> <strong>Mr Warluzelle Romain</strong><span></span> est propriétaire des droits de propriété intellectuelle ou détient les droits d\'usage sur tous les éléments accessibles sur le site, notamment les textes, images, graphismes, logo, icônes, sons, logiciels.<br>Toute reproduction, représentation, modification, publication, adaptation de tout ou partie des éléments du site, quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable de : ************.<br>Toute exploitation non autorisée du site ou de l\'un quelconque des éléments qu\'il contient sera considérée comme constitutive d\'une contrefaçon et poursuivie conformément aux dispositions des articles L.335-2 et suivants du Code de Propriété Intellectuelle.</p><p><br><br><strong>6. Limitations de responsabilité.</strong> <strong>Mr Warluzelle Romain</strong><span></span> ne pourra être tenue responsable des dommages directs et indirects causés au matériel de l\'utilisateur, lors de l\'accès au site http://www.warluzelle.com/<span></span>, et résultant soit de l\'utilisation d\'un matériel ne répondant pas aux spécifications indiquées au point 4, soit de l\'apparition d\'un bug ou d\'une incompatibilité.<br><strong>Mr Warluzelle Romain</strong><span></span> ne pourra également être tenue responsable des dommages indirects (tels par exemple qu\'une perte de marché ou perte d\'une chance) consécutifs à l\'utilisation du site http://www.warluzelle.com/<span></span></p><p><br><br><strong>7. Gestion des données personnelles.</strong> En France, les données personnelles sont notamment protégées par la loi n° 78-87 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l\'article L. 226-13 du Code pénal et la Directive Européenne du 24 octobre 1995.<br>A l\'occasion de l\'utilisation du site http://www.warluzelle.com/<span></span>, peuvent être recueillies : l\'URL des liens par l\'intermédiaire desquels l\'utilisateur a accédé au site http://www.warluzelle.com/<span></span>, le fournisseur d\'accès de l\'utilisateur, l\'adresse de protocole Internet (IP) de l\'utilisateur.<br>En tout état de cause http://www.warluzelle.com/<span></span> ne collecte des informations personnelles relatives à l\'utilisateur que pour le besoin de certains services proposés par le site http://www.warluzelle.com/<span></span>. L\'utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu\'il procède par lui-même à leur saisie. Il est alors précisé à l\'utilisateur du site http://www.warluzelle.com/<span></span> l\'obligation ou non de fournir ces informations.<br>Conformément aux dispositions des articles 38 et suivants de la loi 78-17 du 6 janvier 1978 relative à l\'informatique, aux fichiers et aux libertés, tout utilisateur dispose d\'un droit d\'accès, de rectification et d\'opposition aux données personnelles le concernant, en effectuant sa demande écrite et signée, accompagnée d\'une copie du titre d\'identité avec signature du titulaire de la pièce, en précisant l\'adresse à laquelle la réponse doit être envoyée.<br>Aucune information personnelle de l\'utilisateur du site http://www.warluzelle.com/<span></span> n\'est publiée à l\'insu de l\'utilisateur, échangée, transférée, cédée ou vendue sur un support quelconque à des tiers.</p><p><br><br><strong>8. Liens hypertextes et cookies.</strong><br>Le site http://www.warluzelle.com/<span></span> contient un certain nombre de liens hypertextes vers d\'autres sites, mis en place avec l\'autorisation de <strong>Mr Warluzelle Romain</strong><span></span>. Cependant, <strong>Mr Warluzelle Romain</strong><span></span> n\'a pas la possibilité de vérifier le contenu des sites ainsi visités, et n\'assumera en conséquence aucune responsabilité de ce fait.<br>La navigation sur le site http://www.warluzelle.com/<span></span> est susceptible de provoquer l\'installation de cookie(s) sur l\'ordinateur de l\'utilisateur. Un cookie est un fichier de petite taille, qui ne permet pas l\'identification de l\'utilisateur, mais qui enregistre des informations relatives à la navigation d\'un ordinateur sur un site. Les données ainsi obtenues visent à faciliter la navigation ultérieure sur le site, et ont également vocation à permettre diverses mesures de fréquentation.</p><p><br><br><strong>9. Droit applicable et attribution de juridiction.</strong> Tout litige en relation avec l\'utilisation du site http://www.warluzelle.com/<span></span> est soumis au droit français. Il est fait attribution exclusive de juridiction aux tribunaux compétents de Lille.</p><p><br><br><strong>10. Les lois concernées.</strong> Loi n° 78-87 du 6 janvier 1978, notamment modifiée par la loi n° 2004-801 du 6 août 2004 relative à l\'informatique, aux fichiers et aux libertés.<br>Loi n° 2004-575 du 21 juin 2004 pour la confiance dans l\'économie numérique.<br>Informations personnelles : « les informations qui permettent, sous quelque forme que ce soit, directement ou non, l\'identification des personnes physiques auxquelles elles s\'appliquent » (article 4 de la loi n° 78-17 du 6 janvier 1978).</p>',NULL,NULL,NULL,'2016-01-12 16:30:56','2016-01-12 11:31:36',NULL,'mention_legale','',9,NULL,19,20);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametres`
--

DROP TABLE IF EXISTS `parametres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `raison_sociale` varchar(128) DEFAULT NULL,
  `about` text,
  `contact_name` varchar(128) DEFAULT NULL,
  `contact_email` varchar(128) DEFAULT NULL,
  `telephone` varchar(128) DEFAULT NULL,
  `adresse_1` varchar(128) DEFAULT NULL,
  `adresse_2` varchar(128) DEFAULT NULL,
  `cp` varchar(128) DEFAULT NULL,
  `ville` varchar(128) DEFAULT NULL,
  `ndd` varchar(128) DEFAULT NULL,
  `telephone_noreply` varchar(128) DEFAULT NULL,
  `email_noreply` varchar(128) DEFAULT NULL,
  `email_name` varchar(128) DEFAULT NULL,
  `email_serveur_smtp` varchar(128) DEFAULT NULL,
  `email_serveur_port` varchar(128) DEFAULT NULL,
  `email_serveur_login` varchar(128) DEFAULT NULL,
  `email_serveur_mdp` varchar(128) DEFAULT NULL,
  `lien_facebook` varchar(128) DEFAULT NULL,
  `lien_twitter` varchar(128) DEFAULT NULL,
  `lien_linkedin` varchar(128) DEFAULT NULL,
  `lien_youtube` varchar(128) DEFAULT NULL,
  `lien_pinterest` varchar(128) DEFAULT NULL,
  `lien_google_plus` varchar(128) DEFAULT NULL,
  `active_brochure` tinyint(1) DEFAULT NULL,
  `google_ua` varchar(128) DEFAULT NULL,
  `google_login` varchar(128) DEFAULT NULL,
  `google_mdp` varchar(128) DEFAULT NULL,
  `contact_page` varchar(128) DEFAULT NULL,
  `updated` datetime NOT NULL,
  `siren` varchar(255) DEFAULT NULL,
  `hebergeur` varchar(255) DEFAULT NULL,
  `resp_publication` varchar(255) DEFAULT NULL,
  `url_site_web` varchar(255) DEFAULT NULL,
  `google_tag_manager` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametres`
--

LOCK TABLES `parametres` WRITE;
/*!40000 ALTER TABLE `parametres` DISABLE KEYS */;
INSERT INTO `parametres` VALUES (1,'POMPES FUNEBRES WARLUZELLE',NULL,'Mr Warluzelle Romain','sarlwarluzelle@outlook.fr','03.22.47.02.02','94.96 rue de la 3ème D.I',NULL,'80090','AMIENS',NULL,'03/22/47/02/02','sarlwarluzelle@outlook.fr',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-01-12 11:32:45',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `parametres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partenaire_types`
--

DROP TABLE IF EXISTS `partenaire_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partenaire_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `etat_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partenaire_types`
--

LOCK TABLES `partenaire_types` WRITE;
/*!40000 ALTER TABLE `partenaire_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `partenaire_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partenaires`
--

DROP TABLE IF EXISTS `partenaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partenaires` (
  `id` int(11) NOT NULL,
  `partenaire_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `etat_id` int(11) NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partenaires`
--

LOCK TABLES `partenaires` WRITE;
/*!40000 ALTER TABLE `partenaires` DISABLE KEYS */;
/*!40000 ALTER TABLE `partenaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_type_id` varchar(128) DEFAULT NULL,
  `etat_id` varchar(32) NOT NULL,
  `rubrique_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `h1` varchar(255) NOT NULL,
  `resume` varchar(255) NOT NULL,
  `prix_ttc` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `vignette` text NOT NULL,
  `galerie` text NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taches`
--

DROP TABLE IF EXISTS `taches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taches` (
  `id` int(11) NOT NULL,
  `plugin` varchar(256) NOT NULL,
  `controller` varchar(256) NOT NULL,
  `action` varchar(256) NOT NULL,
  `fond` int(11) NOT NULL DEFAULT '1',
  `author` varchar(256) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `etat` int(11) NOT NULL DEFAULT '0' COMMENT '0 : en attente, 1 : tache commencee, 2 taches terminee',
  `donnees` text NOT NULL,
  `resultat` longtext NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taches`
--

LOCK TABLES `taches` WRITE;
/*!40000 ALTER TABLE `taches` DISABLE KEYS */;
/*!40000 ALTER TABLE `taches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tests`
--

DROP TABLE IF EXISTS `tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tests`
--

LOCK TABLES `tests` WRITE;
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;
/*!40000 ALTER TABLE `tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `txts`
--

DROP TABLE IF EXISTS `txts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `txts` (
  `id` varchar(128) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 NOT NULL,
  `txt` text CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `txts`
--

LOCK TABLES `txts` WRITE;
/*!40000 ALTER TABLE `txts` DISABLE KEYS */;
/*!40000 ALTER TABLE `txts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typepages`
--

DROP TABLE IF EXISTS `typepages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typepages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typepages`
--

LOCK TABLES `typepages` WRITE;
/*!40000 ALTER TABLE `typepages` DISABLE KEYS */;
INSERT INTO `typepages` VALUES (1,'Page d\'accueil','home','Liste toutes les pages métiers'),(2,'Page classique','page',''),(3,'Page métier','metier',''),(4,'Blog (liste des articles)','blog','Liste toutes les actualités'),(5,'Page contact','contact',''),(6,'Accueil liste des metiers','home_metier','Liste toutes les pages de type métier'),(7,'Page d\'accueil références','realisation','Liste toutes les actualités flaguées \"références\"'),(8,'Mention légale','mention_legale',''),(0,'pages pompes funèbres','view-pompes-funebres',''),(0,'pages prevoyance','view-prevoyance',''),(0,'pages marbrerie','view-marbrerie',''),(0,'pages salle de convivialité','view-convivialite',''),(0,'pages fleurs','view-fleurs',''),(0,'pages chambre funeraire','view-chambre','');
/*!40000 ALTER TABLE `typepages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_groups`
--

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` VALUES (2,2,2),(4,3,2),(26,4,2),(40,607123,1),(42,607130,1),(43,607125,1);
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` mediumint(8) unsigned NOT NULL,
  `nom_connexion` varchar(256) NOT NULL,
  `nom` varchar(256) NOT NULL,
  `prenom` varchar(256) NOT NULL,
  `nom_entreprise` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `mobile` varchar(32) NOT NULL,
  `password` char(40) NOT NULL,
  `vignette` text NOT NULL,
  `resume` text NOT NULL,
  `body` text NOT NULL,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,1,'Jerome Lebleu','Lebleu','Jerome','','jerome@whatson-web.com','','','8cfaeb35eeb36111e2c70cc824fe92d5d46903db','','','',0,1,'2015-11-29 21:28:38'),(3,1,'Prod Kobaltis','Kobaltis','Prod','','prod@kobaltis.net','','','fe2af495788471d1dcc3146601b19e630e58bf87','','','',0,1,'2015-11-29 21:28:38'),(4,2,'Redacteur Kobaltis','Kobaltis','Redacteur','','redacteur@kobaltis.net','','','fe2af495788471d1dcc3146601b19e630e58bf87','','','',0,1,'2015-11-29 21:28:38'),(5,2,'Romain Warluzelle','Warluzelle','Romain','','sarlwarluzelle@outlook.fr','','','138c7ff2e71ffbdd0ec7edc82b548b9747f14eed','','','',0,1,'2016-04-13 17:46:03');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-25 12:14:05
