<?php

/**
 * Class Tache
 */
class Tache extends AppModel
{
    /**
     * The belongs to
     *
     * @var array $belongsTo
     */
	public $belongsTo = array('User');

    /**
     * @param bool $created
     */
	public function afterSave($created)
    {
		if ($created) {

			$id = $this->getLastInsertID();

            /**
             * Create task
             */
        	if ($this->data['Tache']['fond']) {

        		exec(APP . 'cakephp/vendors/cakeshell taches bdo ' . $id . ' -cli /usr/bin -console /cakephp/lib/Cake/Console -app ' . APP . ' >> '.APP.'/htdocs/log.log &');

        	} else {

	            exec(APP.'cakephp/vendors/cakeshell taches bdo '.$id.' -cli /usr/bin -console /cakephp/lib/Cake/Console -app ' . APP . ' ', $res);
	            debug($res);
	            exit();
        	}
		}
	}

    /**
     * The after find
     *
     * @param   mixed   $results
     * @param   bool    $primary
     * @return  mixed
     */
	public function afterFind($results, $primary = false)
    {
		foreach($results as $k => $v) {

			if(isset($v['Tache']['etat'])) {

				switch($v['Tache']['etat']) {

					case '0' : $results[$k]['Tache']['etat_name'] = 'En attente'; break;
					case '1' : $results[$k]['Tache']['etat_name'] = 'Commencé'; break;
					case '2' : $results[$k]['Tache']['etat_name'] = 'Terminée'; break;

				}
			}

			if(isset($v['Tache']['resultat'])) {

				$results[$k]['Tache']['resultat'] = unserialize($v['Tache']['resultat']);
			}
		} 

		return $results;
	}
}