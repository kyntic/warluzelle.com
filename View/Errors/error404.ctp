<?php
$this->assign('meta_title', '404 : Page introuvable');

$this->assign('meta_robots', 'NOINDEX, FOLLOW');

$this->assign('class_page', 'page_404');

?>
<section id="principale" class="equilibre">
	<div class="titre">Page introuvable</div>
	<h1>Page non trouvée</h1>	
	<?php echo '<div id="menu_marque">'.$this->element('Catalogue.marques_liste', array('aff_tout' => true), array('cache' => array('config' => 'short', 'key' => 'list_marque_long'))).'</div>';?>
</section>
<aside id="col_gauche" class="equilibre">
	<?php 
	echo $this->Element('ContactManager.infos_contact_front', compact('ContactSite'));
	?>
</aside>

