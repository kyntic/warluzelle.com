<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
		&times;
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $this->fetch('h1');?></h4>
</div>
<?php echo $this->fetch('content');?>

