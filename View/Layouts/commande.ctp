<!DOCTYPE html>
<html lang="fr">
	<head>
		<?php 
		echo $this->Html->charset();
		echo '<title>'.$this->fetch('meta_title').'</title>'; 
		echo $this->Html->meta('description', $this->fetch('meta_description'));
		echo $this->Html->meta('keywords', $this->fetch('meta_keywords'));
		echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0')); // Avec responsive
		echo $this->Html->meta(array('name' => 'author', 'content' => 'Whatson Web'));
		echo $this->Html->meta(array('name' => 'robots', 'content' => $this->fetch('meta_robots')));

		//Google analytics
		if(!empty($Parametres['Parametre']['google_analytics'])) {
			echo $this->element('Google.google_analytics', array(
				'code_analytics' 	=> $Parametres['Parametre']['google_analytics'], 
				'ndd'				=> $Parametres['Parametre']['ndd'], 
				'param_complt' 		=> $this->fetch('param_complt')
			));
		}

		//Css
		echo $this->Html->css('/bootstrap/css/bootstrap.min.css');
		echo $this->Html->css('/bootstrap/css/bootstrap-select.min.css'); 
		echo $this->Html->css('/bootstrap/css/fhmm.css');
		echo $this->Html->css('layout.css');
		echo $this->Html->css('http://fast.fonts.net/cssapi/3153a2d0-76e7-4a9a-8287-1c3379c58495.css'); 
		echo $this->Html->css('http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css'); 
	
		echo $this->fetch('css'); 
		?>		

		<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<link rel="shortcut icon" href="/img/favicon.png">
		<!-- Le fav and touch icons -->
		<!--
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/img/ico/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="/img/ico/apple-touch-icon-57-precomposed.png">
		-->


	</head>
	<!--[if IE ]>
		<body data-spy="scroll" data-target=".subnav" data-offset="50" class="ie <?php 
		echo $this->fetch('class_page').' '; 
		echo $this->fetch('class_moteur_recherche').' ';
		?>">
	<![endif]-->
	<!--[if !IE]><!-->
		<body data-spy="scroll" data-target=".subnav" data-offset="50" class="<?php 
		echo $this->fetch('class_page').' '; 
		echo $this->fetch('class_moteur_recherche').' ';
		?>">
	<!--<![endif]-->

    <div class="container" id="centre">
    	
		<?php 
		echo $this->Session->flash();
		echo $this->fetch('content');
		?>

	</div>


	<header>

		<div class="haut">
			<div class="container">

				<?=$this->element('btn_login');?>

			</div>
		</div>

		<div class="milieu">
			<div class="container">

				<?php echo $this->Html->link($this->Html->image('logo.png'), FULL_BASE_URL, array('escape' => false, 'id' => 'logo')); ?>

				<div class="droite" >

				</div>

			</div>
		</div>

	</header>
    
    <footer>

    	<?=$this->element('menu_ecom');?>

		<div class="bas">
			<div class="container">

				<nav>
					<ul class="nav">
						<li><?php echo $this->Html->link('Mentions légales', '/pages/view/10'); ?></li>
						<li><?php echo $this->Html->link('Notre boutique', '/pages/view/1'); ?></li>
						<li><?php echo $this->Html->link('Contact', '/contact_manager/contacts/view/1'); ?></li>
						<li><?php echo $this->Html->link('Conditions générales de vente', '/pages/view/4'); ?></li>
					</ul>
				</nav>

			</div>
		</div>

    </footer>

	<!-- Modal -->
	<div class="modal fade" id="popup_ajx" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"></div></div></div><!-- /.modal -->



	<?php 
	$this->Html->scriptStart(array('inline' => false));

	if(!empty($_breadcrumb)) {

		$item = array();
		foreach($_breadcrumb['Chemin'] as $v) $item[] = '.item.'.strtolower($_breadcrumb['Model']).'_'.$v[$_breadcrumb['Model']]['id'];

		echo '$(\''.implode(', ', $item).'\').addClass(\'active\');'; 


	}

	?>

		$('a.dropdown-toggle, .dropdown-menu a').on('touchstart', function(e) {e.stopPropagation();});

		$('body').on('hidden.bs.modal', '.modal', function () {
		  $(this).removeData('bs.modal');
		});
		
		$('.flash').css({'top' : '90px', 'opacity' : 1});
		$('.flash').delay(5000).fadeOut(400);


		function Equilibre(Indentifier){

			$(Indentifier).each(function(){
				var h=0;
				$(">*", this)
				$(Indentifier).each(function(){ h=Math.max(h,this.offsetHeight); }).css({'min-height': h+'px'});
			});
		}

		$('.dropdown-toggle').dropdownHover().dropdown();
		$(document).on('click', '.fhmm .dropdown-menu', function(e) {
		  e.stopPropagation()
		});
		 
		$(document).ready(function() {
			Equilibre(".equilibre");
		});

		$(window).load(function(){

			if($(window).width() > 768)
			{

				Equilibre('footer .haut .bloc_menu > div');

			}

		});


	<?php 
	$this->Html->scriptEnd();
    echo $this->Html->script('jquery'); 
    echo $this->Html->script('/bootstrap/js/bootstrap.min.js');
    echo $this->Html->script('/bootstrap/js/bootstrap-select.min.js');
    echo $this->Html->script('/bootstrap/js/fhmm.js');
    echo $this->Html->script('/js/imgLoad/imagesloaded.pkgd.min.js');
    echo $this->Html->script('loading/jquery.showLoading.min.js');
	echo $this->fetch('scriptBottom');
    echo $this->fetch('script'); //Ce sont les feuilles javascripts
    echo $this->Js->writeBuffer(); // Le code jascript en cache
    echo $this->fetch('scriptSuperBottom');
    ?>
</body>

</html>