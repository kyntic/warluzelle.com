<?php
/* /app/View/Helper/LienHelper.php */
App::uses('AppHelper', 'View/Helper');

class LienHelper extends AppHelper {
    
    public $helpers = array('Html');

    public function lancerEdition($titre, $url) {
        // Utilisation du helper HTML pour sortir une donnée formatée

        $link = $this->Html->link($title, $url, array('class' => 'edit'));

        return '<div class="editOuter" style="color:red;">' . $link . '</div>';
    }
}
?>