<?php

App::uses('AppHelper', 'View/Helper');

/**
 * Class SmartHelper
 */
class SmartHelper extends AppHelper
{
    /**
     * The helpers
     *
     * @var array $helpers
     */
    public $helpers = array(
        'Form',
        'Html'
    );

    /**
     * Definition des inputs
     *
     * @var array $inputDefinition
     */
    public $inputDefinition  = array(
            'label'     => array(
                'class'     => 'label', 
                'text'      => null
            ), 
            'div'       => array(
                'tag'   => 'section',
                'class' => ''
            ),
            'between'   => '<label class="input">', 
            'after'     => '</label>', 
            'error'     => array(
                'attributes' => array(
                    'wrap'  => 'span',
                    'class' => 'help-block note'
                )
            )
        );

    /**
     * Initialisation du helper
     *
     * @param View $view
     * @param array $settings
     */
    public function __construct(View $view, $settings = array())
    {
        if (isset($settings['inputDefinition'])) {

            $this->inputDefinition  = $settings['inputDefinition'];
        }

        parent::__construct($view, $settings);
    }

    /**
     * Open box
     *
     * @param   array   $opt
     * @return  string
     */
    public function openBox($opt = array())
    {
        $id = (!empty($opt['id'])) ? 'id="'.$opt['id'].'"' : '';

        $html = '<div class="jarviswidget jarviswidget-color-blueDark" '.$id.'>';
        $html .= '<header>';

        if(!empty($opt['icone'])) $html .= '<span class="widget-icon"> <i class="fa '.$opt['icone'].'"></i> </span>';
        if(!empty($opt['titre'])) $html .= '<h2>'.$opt['titre'].'</h2>';
        if(!empty($opt['tab']))   $html .= $opt['tab'];
        if(!empty($opt['btns'])) {          

            foreach($opt['btns'] as $v) $html .= '<div class="widget-toolbar" role="menu">'.$v.'</div>';

        }

        $html .= '</header><div role="content">';

        return $html;
    }

    /**
     * Close box
     *
     * @return string
     */
    public function closeBox()
    {
        return '</div></div>';
    }

    /**
     * Prepare the input
     *
     * @param   null        $label
     * @param   array       $options
     * @return  array
     */
    private function prepare_input($label = null, $options = array())
    {
        $input = $this->inputDefinition;
        
        if($label) $input['label']['text'] = $label;

        if(!empty($options['col'])) $input['div']['class'] = 'col col-'.$options['col'];

        if(!empty($options['help'])) {
            $input['between'] = '<label class="input"> <i class="icon-append fa fa-question-circle"></i>';
            $input['after']   = '<b class="tooltip tooltip-bottom-right"><i class="fa fa-info txt-color-teal"></i> '.$options['help'].'</b>';
        }

        if(!empty($options['append'])) $input['between'] = '<label class="input"> <i class="icon-append fa '.$options['append'].'"></i>';

        if(!empty($options['tooltip'])) $input['after'] = '<b class="tooltip tooltip-top-right">'.$options['tooltip'].'</b></label>';

        if(!empty($options['options'])) {

            //$input['between'] = '<label class="select">';
            $input['after'] = '<i></i></label>';            
        }

        $input = array_merge($input, $options);
        
        return $input;   
    }

    /**
     * Create the form
     *
     * @param bool $name
     * @param array $options
     * @return mixed
     */
    public function create($name = false, $options = array())
    {
        return $this->Form->create($name, $options);
    }

    /**
     * End
     *
     * @return mixed
     */
    public function end()
    {
        return $this->Form->end();
    }

    /**
     * Input
     *
     * @param $name
     * @param null $label
     * @param array $options
     * @return string
     */
    public function input($name, $label = null, $options = array())
    {
        $input = $this->prepare_input($label, $options);

        if(Configure::read('multilingue')) {

            if(!empty($input['trad'])) {

                $html = '';

                $t_name = explode('.', $name);

                // On récupère les éventuelles traductions existantes :
                $T_champ = 'Translate'.Inflector::camelize($t_name[1]);

                if(isset($this->data[$t_name[0]][$T_champ])) {

                    $value = Set::combine($this->data[$t_name[0]][$T_champ], '{n}.locale', '{n}.content');

                } elseif(isset($this->data[$T_champ])) {

                    $value = Set::combine($this->data[$T_champ], '{n}.locale', '{n}.content');
                    
                } 
                
                foreach(Configure::read('Config.languages') as $k => $v) : ;

                    $new_name = $name.'.'.$v;
                    $new_input = $input;

                    if(!empty($input['label']['text'])) $new_input['label']['text'] = $new_input['label']['text'].' ('.$k.')';

                    $new_input['value'] = (isset($value[$v])) ? $value[$v] : '';

                    $html .= $this->Form->input($new_name, $new_input);

                endforeach;

                return $html;
            }
        }

        return $this->Form->input($name, $input);    
    }

    /**
     * Textarea
     *
     * @param $name
     * @param null $label
     * @param array $options
     * @return string
     */
    public function textarea($name, $label = null, $options = array()) {

        $options['between'] = '<label class="textarea">';

        $input = $this->prepare_input($label, $options);

        if(Configure::read('multilingue')) {

            if(!empty($input['trad'])) {

                $html = '';

                $t_name = explode('.', $name);

                // On récupère les éventuelles traductions existantes :
                $T_champ = 'Translate'.Inflector::camelize($t_name[1]);

                if(isset($this->data[$t_name[0]][$T_champ])) {

                    $value = Set::combine($this->data[$t_name[0]][$T_champ], '{n}.locale', '{n}.content');

                } elseif(isset($this->data[$T_champ])) {

                    $value = Set::combine($this->data[$T_champ], '{n}.locale', '{n}.content');
                    
                } 
                
                foreach(Configure::read('Config.languages') as $k => $v) : ;

                    $new_name = $name.'.'.$v;
                    $new_input = $input;

                    if(!empty($input['label']['text'])) $new_input['label']['text'] = $new_input['label']['text'].' ('.$k.')';

                    $new_input['value'] = (isset($value[$v])) ? $value[$v] : '';

                    $html .= $this->Form->input($new_name, $new_input);

                endforeach;

                return $html;
            }
        }

        return $this->Form->input($name, $input);    
    }

    /**
     * File
     *
     * @param           $name
     * @param   null    $label
     * @param   array   $options
     * @return  mixed
     */
    public function file($name, $label = null, $options = array())
    {
        $placeholder = (!empty($options['placeholder'])) ? $options['placeholder'] : 'choisir un fichier';

        $options['between'] = '<label class="input input-file"><div class="button">';
        $options['after']   = 'Chercher</div><input type="text" placeholder="'.$placeholder.'" readonly=""></label>';
        $options['type']    = 'file';
        $options['onchange']= 'this.parentNode.nextSibling.value = this.value';


        $input = $this->prepare_input($label, $options);

        return $this->Form->input($name, $input);
    }

    /**
     * Hidden
     *
     * @param           $name
     * @param   array   $params
     * @return  mixed
     */
    public function hidden($name, $params = array())
    {

        $params['type'] = 'hidden';

        return $this->Form->input($name, $params);       
    }

    /**
     * Select
     *
     * @param               $name
     * @param   null        $label
     * @param               $select
     * @param   array       $options
     * @return  string
     */
    public function select($name, $label = null, $select, $options = array())
    {
        $html = '';
        if(!empty($label))
        {
            $html .= '<section';
            if(!empty($options['col'])) $html .= ' class="col col-' . $options['col'] . '" ';
            $html .= '>';
            $html .= '<label class="label">';
            $html .= $label;
            $html .= '</label>';
        }
        $html .= '<label class="select">';
        $html .= $this->Form->select($name, $select, $options);
        $html .= '<i></i> </label>';

        if (!empty($label)) {

            $html .= '</section>';
        }

        return $html;
    }

    /**
     * Submit button
     *
     * @param   null    $txt_btn
     * @return  string
     */
    public function submit($txt_btn = null)
    {
        $txt_btn = (empty($txt_btn)) ? __('Enregistrer') : $txt_btn;

        $html = '';
        $html .= $this->Form->button($txt_btn, array('class' => 'btn btn-primary', 'type' => 'submit'));
        
        return $html;
    }

    /**
     * Check button
     *
     * @param           $name
     * @param   null    $label
     * @param   array   $options
     * @return  mixed
     */
    public function check($name, $label = null, $options = array()) {

        $options['before']      = '<label class="checkbox">';
        $options['after']       = '<i></i>'.$label.'</label>';
        $options['label']       = false;
        $options['div']         = array('tag' => 'section', 'class' => '');

        return $this->Form->input($name, $options);
    }

    /**
     * Check On
     *
     * @param           $name
     * @param   null    $label
     * @param   array   $options
     * @return  mixed
     */
    public function checkOn($name, $label = null, $options = array()) {

        $options['before']   = '<label class="toggle">';
        $options['after']    = '<i data-swchon-text="OUI" data-swchoff-text="NON"></i>'.$label.'</label>';
        $options['label']    = false;
        $options['div']      = array('tag' => 'section', 'class' => '');
        $options['type']     = 'checkbox';

        return $this->Form->input($name, $options);
    }

    /**
     * Creates the FormManager config elements
     *
     * @param   array   $fields
     * @return  string
     */
    public function createFormManagerElements(array $fields = array())
    {
        /**
         * No fields
         */
        if (empty($fields)) {

            return false;
        }

        $html = '';

        foreach ($fields as $field) {

            if (!isset($field['type'])) {

                continue;
            }

            switch($field['type']) {

                case 'text' :

                    $html .= $this->input('FormElement.configuration.' . $field['@name'], $field['value'], array());

                    break;

                case 'checkbox' :

                    $html .= '<label class="checkbox">';
                        $html .= '<input type="checkbox" name="data[FormElement][configuration][' . $field['@name']  . ']">';
                        $html .= '<i></i> ' . $field['value'];
                    $html .= '</label>';

                    break;

                case 'textarea' :

                    $html .= $this->input(
                        'FormElement.configuration.' . $field['@name'],
                        $field['value'],
                        array(
                            'type' => 'textarea'
                        )
                    );

                    break;
            }
        }

        return $html;
    }

    /**
     * Display the form elements
     *
     * @param   array   $formElements
     * @return  array
     */
    public function displayFormManagerElements(array $formElements = array())
    {
        $formHtml   = '';

        foreach ($formElements as $formElement) {

            $type       = $formElement['FormElement']['type'];
            $subType    = $formElement['FormElement']['sub_type'];

            switch($type) {

                case 'input' :

                    if ($subType) {

                        switch ($subType) {

                            case 'text' :

                                $formHtml .= $this->input(
                                    $formElement['FormElement']['configuration']['name'],
                                    $formElement['FormElement']['name'],
                                    array(
                                        'size'      => $formElement['FormElement']['configuration']['size'],
                                        'maxlength' => $formElement['FormElement']['configuration']['max_size'],
                                        'value'     => $formElement['FormElement']['configuration']['value'],
                                        'mandatory' => ($formElement['FormElement']['mandatory']) ? 'mandatory' : '',
                                        'name'      => 'data[Form][element]['. $formElement['FormElement']['id'] . ']'
                                    )
                                );

                                break;
                        }
                    }

                    break;

                case 'select' :

                    $formHtml .= $this->select(
                        $formElement['FormElement']['configuration']['name'],
                        $formElement['FormElement']['name'],
                        $this->_prepareOptions($formElement['FormElement']['configuration']['options']),
                        array(
                            'class'     => 'select2 ' . ($formElement['FormElement']['configuration']['multiple'] == 'on' ? 'multiple' : ''),
                            'multiple'  => ($formElement['FormElement']['configuration']['multiple'] == 'on' ? 'multiple' : ''),
                            'name'      => 'data[Form][element]['. $formElement['FormElement']['id'] . ']'
                        )
                    );

                    break;

                case 'textarea' :

                    $formHtml .= $this->textarea(
                        $formElement['FormElement']['configuration']['name'],
                        $formElement['FormElement']['name'],
                        array(
                            'rows'  => $formElement['FormElement']['configuration']['rows'],
                            'cols'  => $formElement['FormElement']['configuration']['cols'],
                            'value' => $formElement['FormElement']['configuration']['value'],
                            'name'  => 'data[Form][element]['. $formElement['FormElement']['id'] . ']'
                        )
                    );

                    break;
            }
        }

        return $formHtml;
    }

    /**
     * Explode a "," separated string
     *
     * @param   string  $options
     * @return  array
     */
    private function _prepareOptions($options = null)
    {
        if ($options == null) {

            return array();
        }

        return explode(',', $options);
    }
}
