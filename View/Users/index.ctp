<div class="bloc_page">
<h1>Inscription</h1>

<?php
echo $this->Form->create('Membres', array('url' => '/profils/inscription', 'class' => 'form-horizontal'));
echo $this->WhForm->input('Membre.prenom', 'Votre nom : ', array(''));
echo $this->WhForm->input('Membre.nom', 'Votre prénom : ', array(''));
echo $this->WhForm->input('Membre.email', 'Votre adresse email : ', array(''));
echo $this->WhForm->input('Membre.password', 'Choisissez un mot de passe : ');
echo $this->WhForm->input('Membre.confirm_password', 'Confirmez votre mot de passe : ', array('type' => 'password'));
echo $this->WhForm->submit('Créer mon compte');
echo $this->Form->end();
?>
</div>