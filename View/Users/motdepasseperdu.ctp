<?php 

$this->extend('/Templates/template_ecommerce');

$this->assign('meta_title', 'Mot de passe perdu : '.Configure::read('Projet.prefixe_title'));
$this->assign('meta_robots', 'NOINDEX, NOFOLLOW');

$this->start('body');
?>

<div class="row">

	<div class="col-md-6">

		<h2>Vous avez déjà un compte ?</h2>

		<?= $this->Form->create('User', array('url' => '/users/motdepasseperdu')); ?>
		<?= $this->Form->input('User.email', array('label' => 'Email')); ?>
		<div style="text-align:right;" >
			<?= $this->WhForm->submit('Réinitialiser mon mot de passe'); ?>
		</div>

		<?= $this->Form->end(); ?>

	</div>

</div>

<?php
$this->end();
?>