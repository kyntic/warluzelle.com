<div class="full-width-container gray-border-bott padding-top24 padding-bottom24 pale-gray-img">
    <div class="container">
        <div class="pull-left">
            <h1 id="titre_page"><?=$this->fetch('h1');?></h1>
        </div>
        <div class="pull-right margin-top6">

            <ul class="no-style margin0 padding0 breadcrumbs">
                <?php
                echo $this->Html->getCrumbsPublic(' ', array(
                    'text' => 'Accueil',
                    'url' => '/',
                    'escape' => false,
                ));
                ?>
            </ul>

        </div>
        <div class="clearfix"></div>
    </div>
</div>
<?php echo $this->fetch('body');?>