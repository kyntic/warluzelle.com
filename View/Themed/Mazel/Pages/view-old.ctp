<section class="inner-intro bg-img25 light-color overlay-dark parallax parallax-background2" style="background-position: 0% 0px;">
    <div class="container">
        <div class="row title">
            <h2 class="h2"><?=$page['Page']['h1'];?></h2>
            <div class="page-breadcrumb">
                <a>Home</a>/<a>Portfolio</a>/<span>Grid</span>
            </div>
        </div>
    </div>
</section>
<section class="ptb ptb-sm-80">
    <?=$this->element('compteur');?>
</section>
