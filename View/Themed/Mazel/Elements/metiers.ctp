<div class="full-width-container pale-gray-bg gray-border-top gray-border-bott" id="zone_metier">

    <div class="container padding-top48 padding-bottom96 text-center">

        <?=($titre) ? '<h1>'.$titre.'</h1>' : '';?>

        <div class="row text-center full-circles">

            <?php if(!empty($metiers)) :; foreach($metiers as $k => $m) : ;?>

                <div class="col-md-4 padding-top48">
                    <a href="<?=$m['Page']['url'];?>" title="<?=$m['Page']['meta_title'];?>">
                        <div class="inline-block services-icon static-icon" style="padding: 24px 30px;">
                            <i class="fa <?=$m['Page']['icone'];?>"></i>
                        </div>
                        <div class="padding-top24">
                            <h5 class="padding-bottom12"><?=$m['Page']['name'];?></h5>
                            <p><?=$m['Page']['resume'];?></p>
                        </div>
                    </a>
                </div>

                <?php if($k && !$k%3) echo '<div class="clearfix"></div>';?>

            <?php endforeach;endif;?>

        </div>

    </div>

</div>
