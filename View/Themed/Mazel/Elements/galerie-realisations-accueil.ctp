
        <!-- Work Section -->
        <section id="work" class="wow fadeIn  pt-sm-80 text-center">
            <div class="container">
                <div class="spacer-30"></div>
                
            </div>
            <!-- Work Gallary -->
            <div class="container-fluid ">
                <div class="row">
                    <div class="container-grid nf-col-4">

                        <div class="nf-item branding design coffee">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="/mazel/img/portfolio/1.jpg" title="Pompes funèbres Waluzelle à Amiens">
                                    <img class="item-container" src="/mazel/img/portfolio/1.jpg" alt="1" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Pompes funèbres Waluzelle à Amiens</h5>
											<p class="white">Devanture de notre magasin</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item photo">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="/mazel/img/portfolio/2.jpg" title="Pompes funèbres Warluzelle">
                                    <img class="item-container" src="/mazel/img/portfolio/2.jpg" alt="2" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Pompes funèbres Warluzelle</h5>
											<p class="white">Un large choix de fleurs</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item branding design coffee">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="/mzelimg/portfolio/3.jpg" title="Pompes funèbres Warluzelle">
                                    <img class="item-container" src="/mazel/img/portfolio/3.jpg" alt="4" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Pompes funèbres Warluzelle</h5>
											<p class="white">Des fleurs pour toutes occasions à Amiens</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item branding design">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="/mazel/img/portfolio/4.jpg" title="Pompes funèbres Warluzelle">
                                    <img class="item-container" src="/mazel/img/portfolio/4.jpg" alt="4" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Pompes funèbres Warluzelle</h5>
											<p class="white">Un large choix d'articles funéraires</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item branding design">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="/mazelimg/portfolio/5.jpg" title="Chambre funéraire">
                                    <img class="item-container" src="/mazel/img/portfolio/5.jpg" alt="5" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Chambre funéraire</h5>
											<p class="white">Des pompes funèbres Warluzelle à Amiens</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item photo">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="/mazel/img/portfolio/6.jpg" title="Pompes funèbres Warluzelle">
                                    <img class="item-container" src="/mazel/img/portfolio/6.jpg" alt="6" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Pompes funèbres Warluzelle</h5>
											<p class="white">Un grand choix de scépultures</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item design">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="/mazel/img/portfolio/7.jpg" title="Petit salon funéraire">
                                    <img class="item-container" src="/mazel/img/portfolio/7.jpg" alt="7" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Petit salon funéraire</h5>
											<p class="white">Pour la famille chez les pompes funèbres Warluzelle</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item coffee">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="/mazel/img/portfolio/8.jpg" title="Magasin des pompes funèbres Warluzelle">
                                    <img class="item-container" src="/mazel/img/portfolio/8.jpg" alt="8" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Magasin des pompes funèbres Warluzelle</h5>
                                            <p class="white">Un large choix d'articles funéraires à Amiens</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End Work Gallary -->
        </section>
        <!-- End Work Section -->
        
    
