<div class="alert alert-block alert-warning">
	<a class="close" data-dismiss="alert" href="#">×</a>
	<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Attention</h4>
	<p>
		<?php echo $message ?>
	</p>
</div>