  <!-- Intro Section -->
        <section class="home-slider dark-bg">

            <!-- Hero Slider Section -->
            <div class="owl-carousel fullwidth-slider white o-flow-hidden">
             
            <?php
            foreach($portfolioItems as $v) : ; 
            ?>
                <div class="item bg-img parallax parallax-section1" >
                	<img src="<?=$v['File']['url'];?>" alt="portfolio" />
                    <!-- Content -->
                    <div class="full-cap-wraper light-color">
                        <div class="content-caption light-color">
                            <div class="container">
                               <h2 class="h2"><?=$v['File']['name'];?></h2>

                                <br />
                                <p class="lead"><?=$v['File']['description'];?></p>
                                <br>
                            </div>
                        </div>
                    </div>
                    <!-- End Content -->
                </div>
            <?php
            endforeach;
            ?>
            </div>
            <!-- End Hero Slider Section -->
        </section>
        <div class="clearfix"></div>
        <!-- End Intro Section -->
