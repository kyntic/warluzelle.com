 <!-- Tab Style2 -->
                    <div class="col-md-8 col-md-offset-2 mt-30 ">
                    <p class="lub">Découvrir la région à vélo</p>
                    <p class="lub2">Si vous souhaitez découvrir la région à vélo, n'hésitez pas à nous demander nos dépliants à ce sujet. Très bien faits, ils vous guideront afin de choisir au mieux vos itinéraires et d'être informé sur les coins à découvrir et les points de chute à ne pas manquer !
                    Découvrez ci-dessous les différents parcours que nous vous conseillons :</p>
                        <div class="tabs b-lrb-none">
                            <ul>
                                <li><a href="#tabs-01" class="bleu"><i class="ion ion-ios-arrow-forward"></i>La Véloroute de Calavon</a></li>
                                <li><a href="#tabs-02" class="bleu"><i class="ion ion-ios-arrow-forward"></i>Les Ocres à vélo</a></li>
                                <li><a href="#tabs-04" class="bleu"><i class="ion ion-ios-arrow-forward"></i>Le pays de Forcalquier</a></li>
                                <li><a href="#tabs-05" class="bleu"><i class="ion ion-ios-arrow-forward"></i>Autour du Lubéron</a></li>

                            </ul>
                            <div class="ui-tab-content">
                                <div id="tabs-01" class="plr-0">
                                    <p>Un itinétaire de niveau facile, accessible à toute la famille, pour les VTC, VAE et vélos de route. Ce petit guide vous propose 37km de ballade pour des sorties allant de 3h à 6h. Vous pédalerez sur le Véloroute du Calavon, une ancienne voie ferrée, seule voie verte du Luberon, réservée en grande partie aux véhicules non-motorisés.Longeant la rivière du Calavon, elle permet de découvrir les plus beaux villages perchés alentours. Votre parcours sera ponctué de champs de lavande, coquelicots et vergers.Vous ferez étape au célèbre ouvrage romain du Pont Julien ou encore visiterez Apt et son marché traditionnel.</p>
                                    <p>Venez nous en parler !</p>
                                </div>
                                <div id="tabs-02" class="plr-0">
                                    <p>Ce guide de type découverte, accessible à tous, convient aux VTC, VAE et vélos de route. Il vous fera découvrir des parcours de 15 à 51km pour des durées de 1h3 à 5h. Il vous fera passer par des petites routes pittoresques et peu fréquentées. Vous traverserez alors quelques-uns des plus beaux paysages et des villages les plus remarquables du Luberon.</p>
                                    <p>Venez nous en parler !</p>
                                </div>
                              
                                <div id="tabs-04" class="plr-0">
                                    <p>Des itinéraires de 44 à 78km pour des randonnées de 4h à 8h en VTC, VAE ou vélos de route, voilà ce qui vous attend dans ce guide. Des hautes plaines provençales, vous découvrirez la grande variété de leurs paysages dans un infini de couleurs, de senteurs et d'atmosphères... Les hommes ont dû, pour l'exploiter, y adapter des cultures typées : lavande, olives, miel, truffe, ... Point de "nez dans le guidon" pour parcourir le pays. Plusieurs étapes sont possibles en hôtels. Point de fringale non plus, les Bistrots de Pays vous proposent une cuisine de terroir ou un casse-croûte réparateur.</p>
                                    <p>Venez nous en parler !</p>
                                </div>
                                <div id="tabs-05" class="plr-0">
                                    <p>Pour les vrais amoureux du vélo, cet itinéraire fait de 236km pour offrira une découverte unique du Luberon. Découvrez en effet un itinéraire au coeur du parc naturel régional du Luberon, territoire préservé depuis 1977, classé Réserve de Biosphère et Réserve naturelle géologique (Géopark). De vallons en collines, découvrez par des routes "secrètes" une alternance de paysages du Luberon et de la Haute-Provence : villages perchés, cabanons pointus, châteaux, falaises d'ocre, vignobles, lavandes et vergers, ainsi que les marchés traditionnels et paysans... Le balisage dans les deux sens offre un parcours idéal pour une journée ou 7 jours entre amis ou en famille.</p>
                                    <p>Venez nous en parler !</p>
                                </div>
                            </div>
                        </div>
                    </div>
