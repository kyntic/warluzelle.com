<!-- footer nav -->
<div class="extra">

	<div class="container">

		<div class="row">
			
			<div class="col-md-4">
				
				<h4>Chambre d’agri</h4>
				
				<ul>
					<li><a href="javascript:;">Qui sommes nous ?</a></li>
					<li><a href="javascript:;">Twitter</a></li>
					<li><a href="javascript:;">Facebook</a></li>
					<li><a href="javascript:;">Google+</a></li>
				</ul>
				
			</div> <!-- /span3 -->
			
			<div class="col-md-4">
				
				<h4>Support</h4>
				
				<ul>
					<li><a href="javascript:;">Questions frequentes</a></li>
					<li><a href="javascript:;">Poser une question</a></li>
				</ul>
				
			</div> <!-- /span3 -->
			
			<div class="col-md-4">
				
				<h4>Legale</h4>
				
				<ul>
					<li><a href="javascript:;">Licence</a></li>
					<li><a href="javascript:;">Conditions d'utilisation</a></li>
					<li><a href="javascript:;">Mentions légales</a></li>
				</ul>
				
			</div> <!-- /span3 -->
							
		</div> <!-- /row -->

	</div> <!-- /container -->

</div> <!-- footer -->

<div class="footer">
		
	<div class="container">
		
		<div class="row">
			
			<div id="footer-copyright" class="col-md-6">
				&copy; 2014-15 
			</div> <!-- /span6 -->
			
			<div id="footer-terms" class="col-md-6">
				<a href="http://www.chambre-agriculture-arras.com" target="_blank">Chambre d’agriculture d’Arras</a>
			</div> <!-- /.span6 -->
			
		</div> <!-- /row -->
		
	</div> <!-- /container -->
	
</div> <!-- /footer -->


