	<div class="subnavbar">

		<div class="subnavbar-inner">
		
			<div class="container">
				
				<a href="javascript:;" class="subnav-toggle" data-toggle="collapse" data-target=".subnav-collapse">
			      <span class="sr-only">Toggle navigation</span>
			      <i class="icon-reorder"></i>
			      
			    </a>

				<div class="collapse subnav-collapse">
					<ul class="mainnav">
					
						<li class="<?php if($this->request->params['controller'] == 'users' && $this->request->params['action'] == 'membre_home') echo 'active';?>">
							<a href="/membre/users/home">
								<i class="icon-home"></i>
								<span>Accueil</span>
							</a>	    				
						</li>
						
						<li class="<?php if($this->request->params['controller'] == 'producteurs') echo 'active';?>">					
							<a href="/membre/producteurs" >
								<i class="fa fa-group"></i>
								<span>Les producteurs</span>
							</a>	
						</li>
										
					</ul>
				</div> <!-- /.subnav-collapse -->

			</div> <!-- /container -->
		
		</div> <!-- /subnavbar-inner -->

	</div> <!-- /subnavbar -->