/**=========================================================
 * SERVICE : Even
 =========================================================*/
App.service('Partenaire', function($http, $q, $log, Module) {

    this.params = {

        Url : {
            save    : '/admin/partenaires/edit',
            view    : '/admin/partenaires/view',
            all     : '/admin/partenaires/index/',
            ordre   : '/admin/partenaires/ordre/',
            delete  : '/admin/partenaires/delete/'
        }

    }


    /**
     * Récupérer
     * @param id
     */
    this.getById = function (id) {

        var _this = this;

        var d = $q.defer();

        if(!id) id = 0;

        $http

            .get(this.params.Url.view + '/' + id )

            .success(function(response) {

                if(!response.Data.Contents.length) response.Data.Contents.push(new Module.getInstance('txt'));

                d.resolve(response.Data);

            }).error(function(res) {

                d.reject(res);

            }

        );

        return d.promise;

    };

    /**
     * Sauvegarder un evènement
     * @param data
     */
    this.save = function (data) {

        var _this = this;

        var d = $q.defer(); //Initialisation de la promesse de fonction

        $http
            .post(this.params.Url.save, data)

            .success(function(response) {

                if(response.ok) {

                    d.resolve(response);

                }else{

                    d.reject(response);

                }


            })
            .error(function(x) {

                d.reject(x);

            });

        return d.promise; //On retourne le résultat de promise

    };


    /**
     *
     * @param nouvelOrdre
     * @returns {promise}
     */
    this.ordre = function (nouvelOrdre) {

        var d = $q.defer();

        $http
            .post(this.params.Url.ordre, {data : nouvelOrdre})

            .success(function (data) {

                d.resolve(data);

            })
            .error(function(data) {

                d.reject(data);

            });

        return d.promise;
    }

    /**
     * Supprimer un partenaire
     * @param data
     */
    this.delete = function (id) {

        var d = $q.defer(); //Initialisation de la promesse de fonction

        $http.get(this.params.Url.delete + id)

            .success(function(data) {

                if(data.ok) {

                    d.resolve('ok');

                }else{

                    d.reject('Une erreur est survenue ');

                }

            })
            .error(function(x) {

                d.reject(x);

            });

        return d.promise; //On retourne le résultat de promise

    };

});



App.controller('PartenairesIndexCtrl', ['$scope' , '$log', '$rootScope', '$http', '$state', 'Partenaire', 'toaster', function($scope, $log, $rootScope, $http, $state, Partenaire, toaster) {

    'use strict';

    /**
     * Liste des fichiers
     * @type {Array}
     */
    $scope.Partenaires = [];


    $scope.load = function () {

        $rootScope.loading = true;

        $http.get(WH_ROOT + '/admin/partenaires/index/').

            success(function(data) {

                $scope.Partenaires = data;

                $rootScope.loading = false;

            }).
            error(function() {

                $rootScope.loading = false;

            });


    }

    $scope.load();

    $scope.sortableOptions = {

        stop: function(e, ui) {

            var nouvelOrdre = $scope.Partenaires.map(function(i){
                return i.Partenaire.id;
            }).join(',');

            Partenaire.ordre(nouvelOrdre).then(

                function(data) {

                    toaster.pop('success', 'L’ordre des pages a été modifié');

                },
                function (data) {

                    $scope.log = data;

                    toaster.pop('danger', 'Une erreur est survenue');

                }


            );

        }
    };


    /**
     * Delete
     */
    $scope.delete = function(index) {

        if(confirm('Etes vous sûre de vouloir supprimer cette page ? ')) {


            $rootScope.loading  = true;

            Partenaire.delete($scope.Partenaires[index].Partenaire.id).then(function(reponse) {

                $rootScope.loading = false;

                toaster.pop('success', 'Partenaire supprimée', '');

                $scope.Partenaires.splice(index, 1);

            }, function(erreur) {

                console.log(erreur);

                toaster.pop('danger', 'Erreur', erreur);

                $rootScope.loading = false;
            });


        }

    };


}]);


App.controller('PartenairesEditionCtrl', ['$scope' , '$log', '$rootScope', '$http', '$state', '$modal', 'toaster', 'Partenaire', function($scope, $log, $rootScope, $http, $state, $modal, toaster, Partenaire) {


    /**
     * Données initiales
     *
     */
    $scope.data     = [];

    $rootScope.loading = true;

    Partenaire.getById($state.params.id).then(

        function (data) {

            $scope.data     = data;

            $rootScope.loading = false;

        }, function() {

            toaster.pop('warning', 'Attention', 'Impossible de retrouver la page');

            $rootScope.loading = false;

        }
    );


    /**
     * Enregistrement
     */
    $scope.submit = function() {

        $scope.log          = '';
        $rootScope.loading  = true;

        Partenaire.save($scope.data).then(function(reponse) {

            $rootScope.loading = false;

            $scope.data.Partenaire.id = reponse.id;

            toaster.pop('success', 'Enregistrement effectué');


        }, function(reponse) {

            $rootScope.loading = false;

            toaster.pop('warning', 'Une erreur est survenue');

            $scope.log = reponse;

            if(reponse.error) $scope.error = reponse.error;


        });
    };


}]);
