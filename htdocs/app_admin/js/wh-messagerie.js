/**=========================================================
 * Module: Messagerie.js
 * Provides a simple demo for pagination
 =========================================================*/

App.service('Message', function($http, $q, $log) {

    this.params = {

        Url : {
            save        : WH_ROOT + '/admin/newsletter_manager/newsletter_campaigns/edit',
            all         : WH_ROOT + '/admin/newsletter_manager/newsletter_campaigns/index',
            view        : WH_ROOT + '/admin/newsletter_manager/newsletter_campaigns/view',
            delete      : WH_ROOT + '/admin/newsletter_manager/newsletter_campaigns/delete/',
            byType      : WH_ROOT + '/admin/newsletter_manager/newsletter_campaigns/get_by_type/',
            saveDraft   : WH_ROOT + '/admin/newsletter_manager/newsletter_campaigns/save_draft'
        }
    };

    this.load       = false;
    this.messages   = [];

    /**
     * Récuéper toutes les actualités
     */
    this.getAll = function() {

        var _this = this, d = $q.defer();

        if(!_this.load) {

            $http.get(_this.params.Url.all)
                .success(function(data){

                    _this.load      = true;
                    _this.messages  = data;

                    d.resolve(data);
                })
                .error(function(data){

                    d.reject(data);
                });

        } else {

            d.resolve(_this.messages);
        }

        return d.promise;
    };

    /**
     * Get all the messages by type
     *
     * @param type
     */
    this.getAllByType = function(type) {

        var _this   = this;
        var d       = $q.defer();


        $http.get(_this.params.Url.byType + type)

            .success(function(response) {

                console.log(response);
                _this.messages  = response;

                d.resolve(response);

            }).error(function(response) {

                d.reject(response);

            });


        return d.promise;
    };

    /**
     * Récupérer
     * @param id
     */
    this.getById = function (id) {

        var _this = this, d = $q.defer(), message = {};

        if(!id) id = 0;

        $http.get(this.params.Url.view + '/' + id)
            .success(function(response) {

                d.resolve(response.data);
            })
            .error(function(res) {

                d.reject(res);
            }
        );

        return d.promise;
    };

    /**
     * Sauvegarder
     * @param data
     */
    this.save = function (data) {

        var _this = this;

        var deferred = $q.defer(); //Initialisation de la promesse de fonction

        $http.post(this.params.Url.save, data)
            .success(function(response) {

                //On met à jour la list des pages
                _this.load = false;

                if (response.ok) {

                    deferred.resolve(response.message);

                } else {

                    deferred.reject(response.error);
                }
            })
            .error(function(x) {

                deferred.reject(x);
            });

        return deferred.promise; //On retourne le résultat de promise
    };

    this.saveDraft = function(data) {

        var _this       = this;
        var deferred    = $q.defer(); //Initialisation de la promesse de fonction

        $http.post(this.params.Url.saveDraft, data)
            .success(function(response) {

                //On met à jour la liste des pages
                _this.load = false;

                if (response.ok) {

                    deferred.resolve(response.message);

                } else {

                    deferred.reject(response.error);
                }
            })
            .error(function(x) {

                deferred.reject(x);
            });

        return deferred.promise; //On retourne le résultat de promise
    };

    /**
     * Sauvegarder une actualité
     * @param data
     */
    this.delete = function (index) {

        var _this = this, d = $q.defer(); //Initialisation de la promesse de fonction

        $http.get(this.params.Url.delete + _this.messages[index]['NewsletterCampaign']['id'])

            .success(function(data) {

                if(data.ok) {

                    d.resolve('ok');

                }else{

                    d.reject('Ce message a déjà été envoyé, et ne peut donc être supprimé. ');
                }
            })
            .error(function(x) {

                d.reject(x);
            });

        return d.promise; //On retourne le résultat de promise
    };
});


/**
 * Mailbox controller
 */
App.controller('MailboxController', ['$scope', '$rootScope', 'Message', '$state', 'toaster', function($scope, $rootScope, Message, $state, toaster) {

    $scope.Messages = {

        type : 'email',
        liste: []

    }

}]);


/**
 * Mailbox controller
 */
App.controller('MailboxIndoxController', ['$scope', '$rootScope', 'Message', '$state', 'toaster', function($scope, $rootScope, Message, $state, toaster) {


    if($state.params.type) $scope.Messages.type = $state.params.type;

    $rootScope.MessagesLoading = true;

    /**
     * Charger les messages
     */
    Message.getAllByType($scope.Messages.type).then(

      function (data) {

          console.log(data);
          //$scope.Messages.liste = data;

          $rootScope.MessagesLoading = false;
      },
      function (error) {

            console.log(error);

          $rootScope.MessagesLoading = false;

      }
    );

    /**
     * Supprimer
     * @param index
     */
    $scope.delete = function (index) {

        if(confirm('Etes-vous sûr(e) de vouloir supprimer ce message ?')) {

            $rootScope.loading = true;

            Message.delete(index).then(

                function() {

                    $rootScope.loading = false;

                    toaster.pop('success', 'Message supprimé');

                    $scope.Messages.splice(index, 1);
                },
                function(message) {

                    $rootScope.loading = false;

                    toaster.pop('error', message);
                }
            );
        }
    }
}]);

/**
 * Message composer
 */
App.controller('MailboxComposeController', ['$scope', '$rootScope', 'Message', 'Ldap', '$state', 'toaster', function($scope, $rootScope, Message, Ldap, $state, toaster) {

    /**
     * Desactivitation du menu
     * @type {boolean}
     */
    $scope.Messages.type = false;

    /**
     * Params par default
     * @type {{NewsletterCampaign: {type: string}}}
     */
    $scope.data         = {
        NewsletterCampaign : {
            type : 'email',
            send_to : []
        }
    };

    /**
     * Get the message
     */
    Message.getById($state.params.id).then(

        function (data) {

            $scope.data     = data;

            $scope.data.NewsletterCampaign.etat_id = 'a envoyer';

            $rootScope.loading = false;

        }, function() {

            toaster.pop('warning', 'Attention', 'Impossible de retrouver le message');

            $rootScope.loading = false;
        }
    );

    /**
     * Get all
     */
    Ldap.getAll().then(

        function(ldapData) {

            /**
             * The groups and the users
             */
            $scope.ldapData = ldapData;
        }
    );

    /**
     * Send the data
     */
    $scope.ok = function() {

        $rootScope.loading  = true;
        $scope.error        = {};

        Message.save($scope.data).then(
            function (send) {

                $rootScope.loading = false;

                toaster.pop('success', send);

                $state.go('app.mailbox.inbox', {}, {reload: true});
            },
            function (error) {

                $rootScope.loading = false;

                $scope.error = error;
            }
        );
    };

    /**
     * Send campaign
     */
    $scope.envoyer = function() {

        if (confirm('Etes-vous sûr(e) de vouloir envoyer ce message ?')) {

            $scope.data.NewsletterCampaign.etat_id  = 'a_envoyer';

            $scope.ok();
        }
    };

    /**
     * Save sms/mail in draft
     */
    $scope.saveDraft = function() {

        /**
         * Write some data
         *
         * @type {string}
         */
        $scope.data.NewsletterCampaign.draft    = 1;
        $scope.data.NewsletterCampaign.etat_id  = 'a_envoyer';

        Message.saveDraft($scope.data).then(
            function(response) {

                $rootScope.loading = false;

                toaster.pop('success', response.message);

                $state.go('app.mailbox.inbox', {}, {reload: true});
            },
            function(error) {

                $rootScope.loading = false;

                $scope.error = error;
            }
        );
    };
}]);