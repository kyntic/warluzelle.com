/**=========================================================
 * SERVICE : Even
 =========================================================*/
App.service('Even', function($http, $q, $log, Module) {

    this.params = {

        Url : {
            save    : WH_ROOT + '/admin/blog/evens/edit',
            view    : WH_ROOT + '/admin/blog/evens/view',
            all     : WH_ROOT + '/admin/blog/evens/index/',
            delete  : WH_ROOT + '/admin/blog/evens/delete/',
            inscrits : {
                all : WH_ROOT + '/admin/blog/even_inscrits/index/',
                delete : WH_ROOT + '/admin/blog/even_inscrits/delete/'
            }
        }

    }


    /**
     * Récupérer
     * @param id
     */
    this.getById = function (id, agenda_id) {

        var _this = this;

        var d = $q.defer();

        if(!id) id = 0;
        if(!agenda_id) agenda_id = 0;

        $http



            .get(this.params.Url.view + '/' + id + '/' + agenda_id)

            .success(function(response) {

                if(!response.Data.Contents.length) response.Data.Contents.push(new Module.getInstance('txt'));

                if(angular.isObject(response.Data.Page)) {

                    response.Data.Page.ids = $.map(response.Data.Page.ids, function(value, index) {
                        return [value];
                    });

                }else{

                    response.Data.Page = {ids : []};

                }


                d.resolve(response.Data);

            }).error(function(res) {

                d.reject(res);

            }

        );

        return d.promise;

    };

    /**
     * Sauvegarder un evènement
     * @param data
     */
    this.save = function (data) {

        var _this = this;

        var d = $q.defer(); //Initialisation de la promesse de fonction

        $http
            .post(this.params.Url.save, data)

            .success(function(response) {

                console.log(response);

                if(response.ok) {

                    d.resolve(response);

                }else{

                    d.reject(response);

                }


            })
            .error(function(x) {

                d.reject(x);

            });



        return d.promise; //On retourne le résultat de promise

    };

    /**
     * Delete
     * @param id
     * @returns {promise}
     */
    this.delete = function (id) {

        var _this = this;

        var d = $q.defer(); //Initialisation de la promesse de fonction

        $http
            .get(this.params.Url.delete + id)

            .success(function(response) {

                if(response.ok) {

                    d.resolve(response);

                }else{

                    d.reject(response);

                }


            })
            .error(function(x) {

                d.reject(x);

            });

        return d.promise; //On retourne le résultat de promise

    };


    this.loadInscrit = function (even_id) {

        var _this = this;

        var d = $q.defer(); //Initialisation de la promesse de fonction

        $http
            .get(this.params.Url.inscrits.all + even_id)

            .success(function(response) {

                d.resolve(response);

            })
            .error(function(x) {

                d.reject(x);

            });

        return d.promise; //On retourne le résultat de promise


    }



});

App.service('Agenda', function($http, $q, $log, Module) {

    this.params = {

        Url : {
            save    : WH_ROOT + '/admin/blog/agendas/edit',
            all     : WH_ROOT + '/admin/blog/agendas/index',
            view    : WH_ROOT + '/admin/blog/agendas/view/',
            delete  : WH_ROOT + '/admin/blog/agendas/delete'
        }

    }

    this.liste = [];


    /**
     * Liste de toutes les calendriers
     * @returns {promise}
     */
    this.load = function () {

        var _this = this, d = $q.defer();

        $http.get(this.params.Url.all)

            .success(function(reponse){

                _this.liste = reponse;

                d.resolve(_this.liste);

            })
            .error(function(res){

                d.reject(res);

            });


        return d.promise;

    }

    /**
     * Enregistrment d'un évènement
     * @returns {promise}
     */
    this.save = function (data) {

        var _this = this, d = $q.defer();

        $http.post(this.params.Url.save, data)

            .success(function(reponse){

                d.resolve(reponse.id);

            })
            .error(function(res){

                d.reject(res);

            });


        return d.promise;

    }

    /**
     * Affichage d'un calendrier
     */
    this.getById = function (id) {

        var _this = this, d = $q.defer();

        $http.get(this.params.Url.view + id)

            .success(function(reponse){

                d.resolve(reponse);

            })
            .error(function(res){

                d.reject(res);

            });


        return d.promise;

    }



});


/**=========================================================
 * AGENDA :
 =========================================================*/
App.controller('AgendaIndexCtrl', ['$scope' , '$log', '$rootScope', '$http', '$state', '$modal', 'toaster', 'Agenda', function($scope, $log, $rootScope, $http, $state, $modal, toaster, Agenda) {

    'use strict';

    /**
     * Action d'edtion d'un calendrier
     * @param index
     */
    $scope.editAgenda = function (index) {

        var modalInstance = $modal.open({
                templateUrl: '/edit.html',
                controller: ModalInstanceCtrl,
                resolve: {
                    data: function () {
                        if (!isNaN(index)) {

                            return $scope.Agendas[index];

                        } else {

                            return {};
                        }
                    }
                }
            })
            .result.then(
                function (id) {

                    $scope.loadAgendas(id);
                }
        );
    };

    /**
     * Controlleur de la popup d'edition d'un calendrier
     * @param $scope
     * @param $modalInstance
     * @param data
     * @constructor
     */
    var ModalInstanceCtrl = function ($scope, $modalInstance, data) {

        $scope.data = data;

        $scope.ok = function () {

            Agenda.save($scope.data).then(
              function(id) {

                  $modalInstance.close(id);
              },
              function(x) {

                  $scope.log = x;
              }
            );
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    /**
     * Chargement des calendriers
     */
    $scope.loadAgendas = function (id) {

        $rootScope.loading = true;

        Agenda.load().then(

            function(reponse) {

                $rootScope.loading = false;

                $scope.Agendas = reponse;

                if(!id && $scope.Agendas.length) $scope.loadAgenda($scope.Agendas[0].Agenda.id);
                if(id) $scope.loadAgenda(id);

            },
            function (reponse) {

                $scope.Agendas = reponse;

                $rootScope.loading = false;

            }

        );

    };

    $scope.loadAgendas($state.params.id);

    /**
     * Chargement d'un calendrier
     */
    $scope.loadAgenda = function (id) {

        $rootScope.loading = true;

        Agenda.getById(id).then(

            function(reponse) {

                $rootScope.loading = false;

                $scope.Agda = reponse;

                var Evens = [];

                angular.forEach(reponse.Evens, function(Even) {

                     var e = {
                        title           : Even.Even.name,
                        start           : Even.Even.date_even_deb_int * 1000 + 3600000,
                        date_deb_fr     : Even.Even.date_even_deb_fr,
                        heure_deb_fr    : Even.Even.heure_even_deb_fr,
                        date_fin_fr     : Even.Even.date_even_fin_fr,
                        heure_fin_fr    : Even.Even.heure_even_fin_fr,
                        backgroundColor : Even.Even.color,
                        resume          : Even.Even.resume,
                        allDay          : (Even.Even.all_day == '0') ? false : true,
                        borderColor     : Even.Even.color,
                        id              : Even.Even.id
                    };

                    Evens.push(e);
                });

                //Insérer les évements à l'agenda
                initCalendar($('#calendar'), Evens);
            },
            function (reponse) {

                $scope.Agda = reponse;

                $rootScope.loading = false;
            }
        );
    };

    /**
     * Delete
     */
    $scope.deleteAgenda = function (index) {

        if (confirm('Etes vous sûre de supprimer ?')) {

            Agenda.delete(index).then(

                function() {

                    $scope.Agendas.splice(index, 1);

                    toaster.pop('success', 'Calendrier supprimé');

                },
                function() {

                    toaster.pop('error', 'Une erreur est survenue');
                }
            );
        }
    };

    /**
     * Calendrier
     */

    /**
     * Invoke full calendar plugin and attach behavior
     * @param  jQuery [calElement] The calendar dom element wrapped into jQuery
     * @param  EventObject [events] An object with the event list to load when the calendar displays
     */
    function initCalendar(calElement, events) {

        // check to remove elements from the list
        var removeAfterDrop = $('#remove-after-drop');

        calElement.fullCalendar({
            isRTL: false,
            lang: 'fr',
            header: {
                left:   'prev,next today',
                center: 'title',
                right:  'month,agendaWeek,agendaDay'
            },
            buttonIcons: { // note the space at the beginning
                prev:    ' fa fa-caret-left',
                next:    ' fa fa-caret-right'
            },
            buttonText: {
                today: 'Aujourd’hui',
                month: 'Mois',
                week:  'Semaine',
                day:   'Jour'
            },
            eventClick: function(calEvent, jsEvent, view) {

                var modalInstance = $modal
                    .open({
                        templateUrl: '/popupeven.html',
                        controller: EvenPopupInstanceCtrl,
                        resolve: {
                            data: function () {
                                return calEvent;
                            }
                        }
                    })
                    .result.then(

                        function(id){

                            calElement.fullCalendar( 'removeEvents', id)

                        },
                        function(){}

                    );

            },
            //editable: true,
            events: events
        });
    }


    //

    /**
     * Popup Evènement
     * @param $scope
     * @param $modalInstance
     * @param data
     * @constructor
     */
    var EvenPopupInstanceCtrl = function ($scope, $modalInstance, data, $state, Even, toaster) {

        $scope.data = data;

        $scope.edit = function () {

            $state.go('app.agenda.edit', {id : data.id});

            $modalInstance.dismiss();

        };

        $scope.delete = function () {

            if(confirm('Etes vous sûre de vouloir supprimer cet évènement ? ')) {

                Even.delete(data.id).then(
                    function() {

                        toaster.pop('success', 'Evenement supprimé');

                        $modalInstance.close(data.id);

                    },
                    function() {
                        alert('Une erreur est survenue');
                    }
                );


            }


        };

        $scope.cancel = function () {

            $modalInstance.dismiss();

        };
    };



}]);


App.controller('AgendaEditionCtrl', ['$scope' , '$log', '$rootScope', '$http', '$state', '$modal', 'toaster', 'Even', 'Page', function($scope, $log, $rootScope, $http, $state, $modal, toaster, Even, Page) {

    /**
     * Calendrier
     */
    $scope.today        = function() {$scope.dt = new Date();};
    $scope.clear        = function () {$scope.dt = null;};
    $scope.open         = function($event) {$event.preventDefault();$event.stopPropagation();$scope.opened = true;};
    $scope.dateOptions  = {formatYear: 'yy', startingDay: 1};


    /**
     * Rubriqes
     */
    $scope.Rubriques = [];

    Page.getList().then(
        function(data) {
            $scope.Rubriques = data;
        }
    );



    /**
     * Slection couleur
     */
    $scope.colors = [
        {class : 'circle-danger', color : '#f05050'},
        {class : 'circle-primary', color : '#5d9cec'},
        {class : 'circle-info', color : '#23b7e5'},
        {class : 'circle-success', color : '#27c24c'},
        {class : 'circle-warning', color : '#ff902b'},
        {class : 'circle-green', color : '#37bc9b'},
        {class : 'circle-pink', color : '#f532e5'},
        {class : 'circle-inverse', color : '#131e26'},
        {class : 'circle-purple', color : '#7266ba'}
    ];


    /**
     * Données initiales
     *
     */
    $scope.data     = [];

    $rootScope.loading = true;

    Even.getById($state.params.id, $state.params.agenda_id).then(

        function (data) {

            $scope.data     = data;

            $rootScope.loading = false;

        }, function() {

            toaster.pop('warning', 'Attention', 'Impossible de retrouver la page');

            $rootScope.loading = false;

        }
    );


    /**
     * Enregistrement
     */
    $scope.submit = function() {

        $scope.log          = '';
        $rootScope.loading  = true;

        Even.save($scope.data).then(function(reponse) {

            $rootScope.loading = false;

            $scope.data.Even.id = reponse.id;

            toaster.pop('success', 'Enregistrement effectué');

            $state.go('app.agenda.index', {id:$scope.data.Even.agenda_id});


        }, function(reponse) {

            $rootScope.loading = false;

            toaster.pop('warning', 'Une erreur est survenue');

            $scope.log = reponse;

            if(reponse.error) $scope.error = reponse.error;


        });
    };


}]);

App.controller('EvenInscritCtrl', ['$scope' , '$log', '$rootScope', '$http', '$state', '$modal', 'toaster', 'Even', function($scope, $log, $rootScope, $http, $state, $modal, toaster, Even) {

    $scope.loadInscrit = function (even_id) {

        $scope.loadingInscrit = true;

        Even.loadInscrit(even_id).then(

            function(data) {

                $scope.Inscrits = data;

                $rootScope.nbrIncrits = $scope.Inscrits.length;

                $scope.loadingInscrit = false;

            },
            function (x){

                $scope.loadingInscrit = false;

            }


        );

    }

    $scope.loadInscrit($state.params.id);


    $scope.editInscrit = function (index) {

        var modalInstance = $modal
            .open({
                templateUrl: '/popupevenInscrit.html',
                controller: EvenInscritPopupInstanceCtrl,
                resolve: {
                    data: function () {

                        if (!isNaN(index)) {
                            return $scope.Inscrits[index];
                        } else {
                            return {
                                EvenInscrit : {
                                    even_id : $state.params.id
                                }
                            };
                        }
                    }
                }
            })
            .result.then(

                function(){

                    $scope.loadInscrit($state.params.id);

                },
                function(){}

        );

    }




    /**
     * Popup E
     * @param $scope
     * @param $modalInstance
     * @param data
     * @constructor
     */
    var EvenInscritPopupInstanceCtrl = function ($scope, $modalInstance, data) {

        $scope.data = data;

        $scope.ok = function () {

            $http
                .post(WH_ROOT + '/admin/blog/even_inscrits/edit', $scope.data)

                .then(function (response) {

                    $modalInstance.close();

                }, function (x) {

                    $scope.log = x;

                }

            );


        };

        $scope.cancel = function () {

            $modalInstance.dismiss();

        };
    };



    $scope.deleteInscrit = function (index) {


        if (confirm('Etes vous sûre de supprimer cette personne ?')) {

            $http
                .post(WH_ROOT + '/admin/blog/even_inscrits/delete/' + $scope.Inscrits[index].EvenInscrit.id)

                .then(function (response) {

                    $scope.Inscrits.splice(index, 1);
                    toaster.pop('success', 'Opération réussie', 'Inscrit supprimé');

                }

            );

        }


    }



}]);


