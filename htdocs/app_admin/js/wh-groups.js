
/**=========================================================
 * CONTROLLER GROUPE INDEX
 =========================================================*/
App.controller('GroupesCtrl', ['$scope', function ($scope) {


}]);

App.controller('GroupesIndexCtrl', ['$scope' , '$log', '$rootScope', '$http', '$modal', 'toaster', '$state', function ($scope, $log, $rootScope, $http, $modal, toaster, $state) {

    'use strict';

    /**
     * Liste des groupes :
     * @type {Array}
     */
    $scope.Groups = [];


    $scope.load = function () {

        $rootScope.loading = true;

        $http.get(WH_ROOT + '/admin/groups/index').
            success(function (data) {

                $scope.Groups = data;

                $rootScope.loading = false;

            }).
            error(function (data) {

                console.log(data);
                $rootScope.loading = false;
            });
    };

    $scope.load();

    $scope.importer = function () {

        $http.get(WH_ROOT + '/admin/right/rights/import')
            .success(function(reponse) {

                $state.go($state.current, {}, {reload: true});

            })
            .error(function() {

            })
    };

    /**
     * Delete
     */
    $scope.delete = function (index) {

        if (confirm('Etes vous sûre de supprimer ?')) {

            $http
                .post(WH_ROOT + '/admin/groups/delete/' + $scope.Groups[index].Group.id)

                .then(function (response) {

                    $scope.Groups.splice(index, 1);
                    toaster.pop('success', 'Opération réussie', 'Groupe supprimé');

                }

            );

        }
        ;

    }


    /**
     * Edition d'un group
     */
    $scope.edit = function (index) {

        var modalInstance = $modal.open({
            templateUrl: '/group-edit.html',
            controller: ModalInstanceCtrl,
            resolve: {
                data: function () {

                    if (!isNaN(index)) {
                        return $scope.Groups[index];
                    } else {
                        return {};
                    }

                }
            }
        }).result.then(function () {

                $scope.load();

            });
    };

    // Please note that $modalInstance represents a modal window (instance) dependency.
    // It is not the same as the $modal service used above.

    var ModalInstanceCtrl = function ($scope, $modalInstance, data) {

        $scope.data = data;


        $scope.ok = function () {

            $http
                .post(WH_ROOT + '/admin/groups/edit', $scope.data)

                .then(function (response) {

                    $modalInstance.close();

                }, function (x) {

                    $scope.log = x;

                }

            );


        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };


}]);
