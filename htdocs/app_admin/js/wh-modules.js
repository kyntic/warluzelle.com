/**=========================================================
 * FACTORY : CONTENT TYPE
 =========================================================*/


App.factory('ModuleProvider',function(){

    var ModuleProvider = function(){


    };

    ModuleProvider.prototype.getInstance = function (type, all) {

        var data = {
            txt : {
                params : {
                    name : 'Bloc texte',
                    module : 'txt',
                    icone : 'align-justify',
                    bloc_url : 'app_admin/views/elements/cms/bloc-text.html'
                },
                content : {
                    title : '',
                    txt : 'Votre texte'
                }
            },
            txtimg : {
                params : {
                    name        : 'Bloc texte + image',
                    module        : 'txtimg',
                    icone : 'align-justify',
                    options     : [
                        {
                            name : 'image à droite',
                            value : 'img-right'
                        },
                        {
                            name : 'image à gauche',
                            value : 'img-left'
                        }
                    ],
                    bloc_url    : 'app_admin/views/elements/cms/bloc-textimg.html'
                },
                content : {
                    title : '',
                    txt : 'Votre texte',
                    img : false,
                    params : {
                        prop : 'img-right'
                    }
                }

            },
            img : {
                params : {
                    name : 'Bloc image',
                    module : 'img',
                    icone : 'file-image-o',
                    bloc_url : 'app_admin/views/elements/cms/bloc-img.html'
                },
                content : {
                    title : '',
                    img : false
                }
            },
            videolocal : {
                params : {
                    name: 'Bloc video local',
                    module: 'videolocal',
                    icone: 'video-camera',
                    bloc_url: 'app_admin/views/elements/cms/bloc-video-local.html'
                },
                content: {
                    title: '',
                    path: ''
                }
            },
            video : {
                params : {
                    name : 'Bloc video',
                    module : 'video',
                    icone : 'video-camera',
                    bloc_url : 'app_admin/views/elements/cms/bloc-video.html'
                },
                content : {
                    title : '',
                    url : '',
                    embed : ''
                }
            },
            galerie : {
                params : {
                    name : 'Galerie',
                    module : 'galerie',
                    icone : 'th-large',
                    bloc_url : 'app_admin/views/elements/cms/bloc-galerie.html'
                },
                content : {
                    title : '',
                    files : []
                }
            },
            link : {
                params : {
                    name : 'Bloc de liens',
                    module : 'link',
                    icone : 'list',
                    bloc_url : 'app_admin/views/elements/cms/bloc-liens.html'
                },
                content : {
                    title : '',
                    list : []
                }
            }
        };

        if(type == 'all') {

            return data;

        }else{

            var m = {ContentModule : data[type]};

            m.ContentModule.module = type;

            return m;
        }


    };


    var service = {
        Instance:function(){ return new ModuleProvider(); }
    };

    return service;

});

//Singleton Services
App.factory('Module',['ModuleProvider',function(ModuleProvider){

    return ModuleProvider.Instance();

}]);

App.filter('orderModule', function() {

    return function(items, field, reverse) {

        var filtered = [];

        angular.forEach(items, function(item) {
            filtered.push(item);
        });

        filtered.sort(function (a, b) {
            //return (a[field] > b[field] ? 1 : -1);
            return (a.ContentModule.content[field] > b.ContentModule.content[field] ? 1 : -1);
        });

        if(reverse) filtered.reverse();

        return filtered;
    };
});


App.controller('ModuleCtrl', ['$scope', 'Module', 'toaster', '$timeout', '$window', function ($scope, Module, toaster, $timeout, $window) {

    'use strict';

    //$scope.Module   = Module.types;
    $scope.Modules  = Module.getInstance('all');

    $scope.addBloc = function (type) {

        $scope.data.Contents.push(Module.getInstance(type, 0));

        //Ajouter 1 à la position
        $scope.data.Contents[$scope.data.Contents.length - 1].ContentModule.content.position = $scope.data.Contents.length;


    };

    $scope.deleteBloc = function (index) {

        if (confirm('Etes vous sûr(e) de vouloir supprimer ce bloc ? ')) {

            $scope.data.Contents.splice(index, 1);

            toaster.pop('success', 'Opération réussie', 'Le bloc sera supprimé à l’enregistrement');

        }
    }


    $scope.LiensNewBloc = function ($index) {

        $scope.data.Contents[$index].ContentModule.content.files.push();

    }

    $scope.galDelete = function (indexModule, indexFile) {

        $scope.data.Contents[indexModule].ContentModule.content.files.splice(indexFile, 1);

    }


    /**
     * Ordre
     */

    $scope.moduleSortableOptions = {
        handle: '.sortable-handler'
    };
}]);