<?php
$this->data['header'] = $this->t('{login:user_pass_header}');

if (strlen($this->data['username']) > 0) {
	$this->data['autofocus'] = 'password';
} else {
	$this->data['autofocus'] = 'username';
}
$this->includeAtTemplateBase('includes/header-boostrap.php');

$visible = false;

if ($this->data['errorcode'] !== null) {

    if ($this->data['errorcode'] == 'WRONGUSERPASS') {

        $visible = true;
    }
}
?>


<div class="block-center mt-xl wd-xl">
    <!-- START panel-->
    <div class="panel panel-dark panel-flat">
        <div class="panel-heading text-center">
            <a href="#">
                <img src="/app_membre/assets/img/logo.svg" alt="Image" class="block-center img-rounded" />
            </a>
        </div>
        <div class="panel-body">
            <p class="text-center pv">Espace membre</p>
            <form action="?" method="post" name="f" class="smart-form client-form" id="#login-form">
                <div class="form-group has-feedback">
                    <?php
                    if ($this->data['forceUsername']) {

                        echo '<strong style="font-size: medium">' . htmlspecialchars($this->data['username']) . '</strong>';

                    } else {

                        echo '<input type="text" id="username" tabindex="1" name="username" value="' . htmlspecialchars($this->data['username']) . '" placeholder="Email" required="required" class="form-control" />';
                        echo '<span class="fa fa-envelope form-control-feedback text-muted"></span>';
                    }
                    ?>
                </div>
                <div class="form-group has-feedback">
                    <input id="exampleInputPassword1" type="password" name="password" placeholder="Mot de passe" required="required" class="form-control" />
                    <span class="fa fa-lock form-control-feedback text-muted"></span>
                </div>

                <?php
                foreach ($this->data['stateparams'] as $name => $value) {

                    echo('<input type="hidden" name="' . htmlspecialchars($name) . '" value="' . htmlspecialchars($value) . '" />');
                }
                ?>

                <!--
                <div class="clearfix">
                    <div class="checkbox c-checkbox pull-left mt0">
                        <label>
                            <input type="checkbox" value="" name="remember" ng-model="account.remember" />
                            <span class="fa fa-check"></span>Se souvenir de moi</label>
                    </div>
                    <div class="pull-right"><a ui-sref="public.recover" class="text-muted">Mot de passe perdu</a>
                    </div>
                </div>
                -->
                <button type="submit" class="btn btn-block btn-primary mt-lg">S'identifier</button>
            </form>

            <?php if ($visible) : ?>
                <div class="alert alert-danger text-center">
                   Le nom d&#39;utilisateur ou le mot de passe est incorrect
                </div>
            <?php endif; ?>

            <!--<p class="pt-lg text-center">Pas encore inscrit ?</p><a ui-sref="public.register" class="btn btn-block btn-default">S'inscrire</a>-->

        </div>
    </div>
    <!-- END panel-->

</div>


<?php $this->includeAtTemplateBase('includes/footer-bootstrap.php');?>