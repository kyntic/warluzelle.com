$(document).ready(function() {


    /*
     * Fixed table height
     */
    /*
     tableHeightSize()

     $(window).resize(function() {
     tableHeightSize()
     })

     function tableHeightSize() {
     var tableHeight = $(window).height() - 312;
     $('.table-wrap').css('height', tableHeight + 'px');
     }
     */

    $.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
        _title : function(title) {
            if (!this.options.title) {
                title.html("&#160;");
            } else {
                title.html(this.options.title);
            }
        }
    }));


// Dialog click
    $('.dialog').click(function() {

        var width = ($(this).data('dialog-width')) ? $(this).data('dialog-width') : 600;
        var title = ($(this).attr('title')) ? $(this).attr('title') : 'Ma boite de dialogue';
        var icone = ($(this).attr('dialog-icone')) ? $(this).attr('dialog-icone') : 'fa-question-circle';

        $('#dialog_simple').dialog({
            autoOpen 	: false,
            width 		: width,
            resizable 	: true,
            modal 		: true,
            title : "<div class='widget-header'><h4><span class='txt-color-orangeDark'><i class='fa " + icone + "'></i> <strong>" + title + "</strong></span></h4></div>",
            buttons : [{
                html 	: "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
                "class" : "btn btn-danger",
                click : function() {
                    $(this).dialog("close");
                }
            }, {
                html : "<i class='fa fa-times'></i>&nbsp; Cancel",
                "class" : "btn btn-default",
                click : function() {
                    $(this).dialog("close");
                }
            }]
        });

        $('#dialog_simple').dialog('open');
        return false;

    });

    pageSetUp();

    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });


});


$("a[data-alert]").click(function(e) {

    var el = $(this);

    $.smartMessageBox({
        title : "<span class='MsgTitle'><i class='fa fa-warning txt-color-orangeDark'></i> Message <span class='txt-color-orangeDark'><strong>Alerte! </strong></span></span>",
        content : $(this).data('alert'),
        buttons : '[Non][Oui]'
    }, function(ButtonPressed) {
        if (ButtonPressed === "Oui") {

            window.location = $(el).attr('href');
        }

    });
    e.preventDefault();
});



var smartAlert = function (message, type) {

    switch(type) {

        case 'error' :

            $.bigBox({
                title : "Une erreur est survenue",
                content : message,
                color : "#C46A69",
//timeout: 6000,
                icon : "fa fa-warning shake animated",
                timeout : 6000
            });


            break;


        case 'success' :

            $.bigBox({
                title : "Opération réussie",
                content : message,
                color : "#739E73",
                timeout: 8000,
                icon : "fa fa-check"

            });

            break;

        default :

            $.bigBox({
                title : "Info",
                content : message,
                color : "#3276B1",
                timeout: 8000,
                icon : "fa fa-bell swing animated"

            });

    }


}

var whActiveTab = function (id) {

    var hash = window.location.hash.substring(1);
    if(!hash) var hash = $('#' + id + ' .nav-tabs li a:first-child').attr('href').substring(1);

    $('#'+ id +' a[href=#' + hash + ']').parent().addClass('active');
    $('#' + hash).addClass('active');
    $('#' + hash).addClass('in');

    $('#'+ id +' .nav-tabs a').click(function() {

        window.location.hash = $(this).attr('href');


    });

}
